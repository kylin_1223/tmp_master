import { FieldURISchema, FolderName } from 'coremail/src/main/ets/protocols/exchange/xml/xml_attribute';

import {
  GetCalendarRequest
} from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_calendar_request'
import { GetCalendarMessage } from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_calendar_message'
import { GetCalendarTask } from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_calendar_task'

import {
  GetCalendarItemMessage,
  CalendarView
} from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_calendar_item_message'
import {
  GetCalendarItemRequest
} from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_calendar_item_request'
import {
  GetCalendarItemTask
} from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_calendar_item_task'

import { GetItemBatchMessage } from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_item_batch_message'
import { GetItemBatchRequest } from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_item_batch_request'
import { GetItemBatchTask } from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_item_batch_task'

import {
  GetRelatedRecurrenceItemsMessage
} from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_related_recurrence_items_message'
import {
  GetRelatedRecurrenceItemsRequest
} from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_related_recurrence_items_request'
import {
  GetRelatedRecurrenceItemsTask
} from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_related_recurrence_items_task'

import { GetRoomListsMessage } from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_room_lists_message'
import { GetRoomListRequest } from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_room_list_request'
import { GetRoomListsTask } from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_room_lists_task'

import { GetRoomsMessage } from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_rooms_message'
import { GetRoomsRequest } from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_rooms_request'
import { GetRoomsTask } from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_rooms_task'

import {
  GetUserAvailabilityMessage
} from 'coremail/src/main/ets/protocols/exchange/model/calendar/get_user_availability_message'
import {
  GetUserAvailabilityRequest
} from 'coremail/src/main/ets/protocols/exchange/request/calendar/get_user_availability_request'
import {
  GetUserAvailabilityTask
} from 'coremail/src/main/ets/protocols/exchange/task/calendar/get_user_availability_task'

import { Properties } from 'coremail/src/main/ets/api';
import {
  Attendee,
  CalendarFolder,
  EndDateRecurrence,
  Mailbox,
  Recurrence,
  WeeklyRecurrence
} from 'coremail/src/main/ets/protocols/exchange/model/calendar/calendar_response_types';
import { ExchangeError } from 'coremail/src/main/ets/protocols/exchange/error/exchange_error';
import {
  ChangeItem,
  ConflictResolution,
  ItemField,
  ItemOperateType,
  UpdateItem
} from 'coremail/src/main/ets/protocols/exchange/model/update_item';
import { Calendar } from 'coremail/src/main/ets/protocols/exchange/model/calendar/item_calendar';
import { UpdateItemTask } from 'coremail/src/main/ets/protocols/exchange/task/update_item_task';
import { UpdateItemRequest } from 'coremail/src/main/ets/protocols/exchange/request/update_item_request';
import { getAuthUser } from 'coremail/src/main/ets/protocols/exchange/utils/calendar_util';
import { FolderIds, DistinguishedFolderId } from 'coremail/src/main/ets/protocols/exchange/model/folder/folder';
import {
  MessageDisposition,
  PropertySet,
  SendMeetingInvitations,
  TraversalType
} from 'coremail/src/main/ets/protocols/exchange/utils/common_enum';
import { SoapXmlWriter } from 'coremail/src/main/ets/protocols/exchange/xml';
import { UniteId } from 'coremail/src/main/ets/protocols/exchange/model/mode_base';
import { MailEngine } from 'coremail/Index';

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: getAuthUser().address,
    password: getAuthUser().pwd,
  }
}

let folders: CalendarFolder[];
let calendarItems: Calendar[];

@Entry
@Component
struct Index {
  private exchangeEngine: MailEngine | null = null;
  @State message: string = '';

  aboutToAppear(): void {
    this.exchangeEngine = new MailEngine();
    try {
      this.exchangeEngine.setProperties(properties);
    } catch (e) {
      console.log(e)
    }
  }

  build() {
    Row() {
      Column() {
        Scroll() {
          Column() {
            Text('点击1 -> 点击2 -> 然后3/4/5/6').fontSize(16)
            Button('1.获取约会（GetFolder）')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetAppointment();
              })
            Button('2.获取约会（FindItem）')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetAppointmentItem();
              })
            Button('3.批量获取日历项')
              .fontSize(16)
              .onClick(() => {
                this.calendarBatchGetItem();
              })
            Button('4.更新约会')
              .fontSize(16)
              .onClick(() => {
                this.calendarUpdateAppointment();
              })
            Button('5.更新文件夹')
              .fontSize(16)
              .onClick(() => {
                this.calendarUpdateFolder();
              })
            Button('6.批量更新日历项')
              .fontSize(16)
              .onClick(() => {
                this.calendarBatchUpdateItem();
              })

            Button('7.访问定期系列')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetRelatedRecurrenceItems();
              })
            Button('8.更新定期系列')
              .fontSize(16)
              .onClick(() => {
                this.calendarUpdateRecurringSeries();
              })
            Button('9.修改定期系列中的约会')
              .fontSize(16)
              .onClick(() => {
                this.calendarModifyARecurringSeries();
              })

            Button('10.获取所有会议室列表')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetRoomList();
              })
            Button('11.获取会议室列表中的所有会议室')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetRooms();
              })

            Button('12.获取忙/闲信息')
              .fontSize(16)
              .onClick(() => {
                this.calendarGetSuggestedMeetingTimesAndFreeBusyInfo();
              })
          }
        }.height('50%')

        Scroll() {
          Text(this.message).fontSize(14);
        }.height('50%')
      }.width('100%')
    }
    .height('100%')
  }

  changeRequestState() {
    this.message = '正在请求...'
  }

  /**
   * 获取约会和会议（使用 EWS 托管 API 获取约会）GetFolder
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","Folders":[{"FolderId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgAuAAADp5+FtyHI4kKA/XTbWXuvdwEAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAA","ChangeKey":"AgAAABYAAACxBbuuvod1RYou+/5iFMFrAAACFAvl"},"ParentFolderId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgAuAAADp5+FtyHI4kKA/XTbWXuvdwEAsQW7rr6HdUWKLvv+YhTBawAAAgEIAAAA","ChangeKey":"AQAAAA=="},"FolderClass":"IPF.Appointment","DisplayName":"日历","TotalCount":"5","ChildFolderCount":"4","EffectiveRights":{"CreateAssociated":"true","CreateContents":"true","CreateHierarchy":"true","Delete":"true","Modify":"true","Read":"true","ViewPrivateItems":"true"},"PermissionSet":{"CalendarPermissions":{"CalendarPermission":[{"UserId":{"DistinguishedUser":"Default"},"CanCreateItems":"false","CanCreateSubFolders":"false","IsFolderOwner":"false","IsFolderVisible":"false","IsFolderContact":"false","EditItems":"None","DeleteItems":"None","ReadItems":"TimeOnly","CalendarPermissionLevel":"FreeBusyTimeOnly"},{"UserId":{"DistinguishedUser":"Anonymous"},"CanCreateItems":"false","CanCreateSubFolders":"false","IsFolderOwner":"false","IsFolderVisible":"false","IsFolderContact":"false","EditItems":"None","DeleteItems":"None","ReadItems":"None","CalendarPermissionLevel":"None"}]}}}]}]}}
   */
  calendarGetAppointment() {
    this.changeRequestState();
    let requestMessage: GetCalendarMessage = new GetCalendarMessage();
    requestMessage.baseShape = PropertySet.AllProperties;
    requestMessage.folderId = FolderName.calendar;
    let anyTask: GetCalendarTask = new GetCalendarTask(new GetCalendarRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetAppointment----->" + this.message);
      folders = data.getData().responseMessages[0].folders;
    });
  }

  /**
   * 获取约会和会议（使用 EWS 托管 API 获取约会）FindItem
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","RootFolder":{"TotalItemsInView":"5","IncludesLastItemInRange":"true"},"Items":[{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAPNS8UAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP1rW"},"Subject":"Tennis Lesson moved one hour later and to the day after Wednesday!","Start":"2024-03-21T04:00:00Z","End":"2024-03-21T04:30:00Z"},{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAZfMiIAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP00L"},"Subject":"修改 呦呵呦呵 === Company headquarters are moving down the street to 1234 Contoso Drive!","Start":"2024-03-25T02:00:00Z","End":"2024-03-25T02:30:00Z"},{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAZfMiMAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAGXTCY"},"Subject":"阿萨啊啊啊","Start":"2024-03-28T15:00:00Z","End":"2024-03-28T15:30:00Z"},{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgFRAAgIANxPgy1awABGAAACp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAdBulUAAAAQ","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHPyJF"},"Subject":"范儿官方深V下次","Start":"2024-03-29T07:00:00Z","End":"2024-03-29T07:30:00Z"},{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAZfMiQAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAGXTCb"},"Subject":"冲冲冲出","Start":"2024-03-31T15:00:00Z","End":"2024-03-31T15:30:00Z"}]},{"ResponseClass":"Success","ResponseCode":"NoError","RootFolder":{"TotalItemsInView":"1","IncludesLastItemInRange":"true"},"Items":[{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAAZfGqsAAACxBbuuvod1RYou+/5iFMFrAAAAB0GqrgAAAA==","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP00P"},"Subject":"修改 那边MV那份 === Company headquarters are moving down the street to 1234 Contoso Drive!: Code Blast","Start":"2024-03-28T15:00:00Z","End":"2024-03-28T15:30:00Z"}]},{"ResponseClass":"Success","ResponseCode":"NoError","RootFolder":{"TotalItemsInView":"1","IncludesLastItemInRange":"true"},"Items":[{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAAZfGqwAAACxBbuuvod1RYou+/5iFMFrAAAAB0GygAAAAA==","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHPyIs"},"Subject":"，你们慢慢，","Start":"2024-03-28T15:00:00Z","End":"2024-03-28T15:30:00Z"}]}]}}
   */
  calendarGetAppointmentItem() {
    this.changeRequestState();
    let requestMessage: GetCalendarItemMessage = new GetCalendarItemMessage();
    requestMessage.traversal = TraversalType.Shallow;
    requestMessage.baseShape = PropertySet.AllProperties;
    requestMessage.fieldUris = [FieldURISchema.Subject, FieldURISchema.Start, FieldURISchema.End];

    let calendarView: CalendarView = {
      maxEntriesReturned: '30',
      startDate: '2024-04-01T09:00:00Z',
      endDate: '2024-04-30T23:59:59Z',
    }
    requestMessage.calendarView = calendarView;

    let parentFolderId: UniteId[] = [];
    if (!folders || folders.length <= 0) {
      this.message = '先调用 GetFolder';
      return;
    }
    folders.forEach(item => {
      if (item.folderId) {
        parentFolderId.push(item.folderId);
      }
    });

    //支持多个？
    requestMessage.parentFolderIds = parentFolderId;

    let anyTask: GetCalendarItemTask = new GetCalendarItemTask(new GetCalendarItemRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetAppointmentItem----->" + this.message);
      calendarItems = data.getData().responseMessages[0].items;
    });
  }

  /**
   * 批量处理日历项目（使用 EWS 托管 API 批量获取日历项）
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","responseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAPNS8UAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP1rW"},"ParentFolderId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgAuAAADp5+FtyHI4kKA/XTbWXuvdwEAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAA","ChangeKey":"AQAAAA=="},"ItemClass":"IPM.Appointment","Subject":"Tennis Lesson moved one hour later and to the day after Wednesday!","Sensitivity":"Normal","Body":{"BodyType":"HTML","IsTruncated":"false"},"DateTimeReceived":"2024-03-21T03:10:49Z","Size":"8650","Importance":"Normal","IsSubmitted":"false","IsDraft":"false","IsFromMe":"false","IsResend":"false","IsUnmodified":"false","DateTimeSent":"2024-03-21T03:10:49Z","DateTimeCreated":"2024-03-21T03:10:48Z","ReminderDueBy":"2024-03-21T04:00:00Z","ReminderIsSet":"true","ReminderNextTime":"2024-03-21T03:45:00Z","ReminderMinutesBeforeStart":"15","DisplayTo":"liwangwuhan; liwangchengmai; xiongwenshuai1@outlook.com","HasAttachments":"false","Culture":"zh-CN","LastModifiedName":"wuhanliwang@outlook.com","LastModifiedTime":"2024-03-27T11:56:08Z","IsAssociated":"false","WebClientReadFormQueryString":"https://outlook.live.com/owa/?itemid=AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5%2BFtyHI4kKA%2FXTbWXuvdwcAsQW7rr6HdUWKLvv%2BYhTBawAAAgENAAAAsQW7rr6HdUWKLvv%2BYhTBawAAAAPNS8UAAAA%3D&exvsurl=1&path=/calendar/item","ConversationId":{"Id":"AQQkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgAQABPa5Ee1BAtNlQamaKAJko4="},"InstanceKey":"AQAAAAAAAQ0BAAAAA81LxQAAAAA=","UID":"040000008200E00074C5B7101A82E008000000001CEF975F3D7BDA01000000000000000010000000BDDC91419CA4434DA405039EC1449196","DateTimeStamp":"2024-03-27T11:55:24Z","Start":"2024-03-21T04:00:00Z","End":"2024-03-21T04:30:00Z","IsAllDayEvent":"false","LegacyFreeBusyStatus":"Busy","Location":"4567 Contoso Way, Redmond, OH 33333, USA","IsMeeting":"true","IsCancelled":"false","IsRecurring":"false","MeetingRequestWasSent":"true","IsResponseRequested":"true","CalendarItemType":"Single","MyResponseType":"Organizer","Duration":"PT30M","TimeZone":"(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi","AppointmentSequenceNumber":"6","AppointmentState":"1","IsOnlineMeeting":"false","StartWallClock":"2024-03-21T12:00:00+08:00","EndWallClock":"2024-03-21T12:30:00+08:00","StartTimeZoneId":"China Standard Time","EndTimeZoneId":"China Standard Time","IntendedFreeBusyStatus":"NoData","IsOrganizer":"true"}},{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAAZfGqwAAACxBbuuvod1RYou+/5iFMFrAAAAB0GygAAAAA==","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHPyIs"},"ParentFolderId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgAuAAADp5+FtyHI4kKA/XTbWXuvdwEAsQW7rr6HdUWKLvv+YhTBawAAAAZfGqwAAAA=","ChangeKey":"AQAAAA=="},"ItemClass":"IPM.Appointment","Subject":"，你们慢慢，","Sensitivity":"Normal","Body":{"BodyType":"HTML","IsTruncated":"false"},"DateTimeReceived":"2024-03-26T09:42:28Z","Size":"5937","Importance":"Normal","IsSubmitted":"false","IsDraft":"false","IsFromMe":"false","IsResend":"false","IsUnmodified":"false","DateTimeSent":"2024-03-26T09:42:28Z","DateTimeCreated":"2024-03-26T09:42:28Z","ReminderDueBy":"2024-03-28T15:00:00Z","ReminderIsSet":"true","ReminderNextTime":"2024-03-28T14:45:00Z","ReminderMinutesBeforeStart":"15","HasAttachments":"false","Culture":"zh-CN","LastModifiedName":"wuhanliwang@outlook.com","LastModifiedTime":"2024-03-26T09:42:28Z","IsAssociated":"false","WebClientReadFormQueryString":"https://outlook.live.com/owa/?itemid=AQMkADAwATM3ZmYAZS
   */
  calendarBatchGetItem() {
    this.changeRequestState();
    let requestMessage: GetItemBatchMessage = new GetItemBatchMessage();
    // requestMessage.setTimeZoneDefinitionId('Pacific Standard Time');
    requestMessage.baseShape = PropertySet.AllProperties;
    // requestMessage.setFieldURIs([FieldURISchema.Subject, FieldURISchema.Body, FieldURISchema.ReminderMinutesBeforeStart,
    //   FieldURISchema.Start, FieldURISchema.End, FieldURISchema.CalendarItemType, FieldURISchema.Location,
    //   FieldURISchema.RequiredAttendees])
    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }
    let itemIds: UniteId[] = [];
    calendarItems.forEach(item => {
      if (item.itemId) {
        itemIds.push(item.itemId);
      }
    });
    requestMessage.itemIds = itemIds;
    let anyTask: GetItemBatchTask = new GetItemBatchTask(new GetItemBatchRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarBatchGetItem----->" + JSON.stringify(data));
    });
  }

  /**
   * 更新约会和会议（使用 EWS 托管 API 更新约会）
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAPNS8UAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP1rW"}},"ConflictResults":{"Count":0}}]}}
   */
  calendarUpdateAppointment() {
    this.changeRequestState();
    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }
    let updateItems = new UpdateItem();
    let itemChanges: ChangeItem[] = [];
    let items = new ChangeItem();
    let itemFields: ItemField[] = [];

    let item_Subject = new Calendar();
    item_Subject.fieldUri = FieldURISchema.Subject;
    item_Subject.subject = '更新内容为calendarUpdateAppointment'
    let itemField_item_Subject: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: item_Subject.fieldUri.valueOf(),
      item: item_Subject,
    }
    itemFields.push(itemField_item_Subject);
    items.itemId = { id: calendarItems[0].itemId.id, changeKey: calendarItems[0].itemId.changeKey };
    items.itemFields = itemFields;

    itemChanges.push(items);
    updateItems.itemChanges = itemChanges;

    updateItems.messageDisposition = MessageDisposition.SaveOnly;
    updateItems.conflictResolution = ConflictResolution.AlwaysOverwrite;
    updateItems.sendMeetingInvitationsOrCancellations = SendMeetingInvitations.SendToNone;

    this.updateCalendar(updateItems);
  }

  /**
   * 更新约会和会议（使用 EWS 托管 API 更新文件夹）
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAPNS8UAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP1q8"}},"ConflictResults":{"Count":0}}]}}
   */
  calendarUpdateFolder() {
    this.changeRequestState();
    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }

    let updateItems = new UpdateItem();
    let itemChanges: ChangeItem[] = [];
    let items = new ChangeItem();
    let itemFields: ItemField[] = [];

    let item_Subject = new Calendar();
    item_Subject.fieldUri = FieldURISchema.Subject;
    item_Subject.subject = '更新内容为calendarUpdateFolder'
    let itemField_item_Subject: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: item_Subject.fieldUri.valueOf(),
      item: item_Subject,
    }
    itemFields.push(itemField_item_Subject);

    let calendar_Location = new Calendar();
    calendar_Location.fieldUri = FieldURISchema.Location;
    calendar_Location.location = '4567 Contoso Way, Redmond, OH 33333, USA';
    let itemField_calendar_Location: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_Location.fieldUri.valueOf(),
      item: calendar_Location,
    }
    itemFields.push(itemField_calendar_Location);

    let calendar_RequiredAttendees = new Calendar();
    calendar_RequiredAttendees.fieldUri = FieldURISchema.RequiredAttendees;
    let att = new Attendee();
    att.mailbox = new Mailbox();
    att.mailbox.name = 'liwangwuhan';
    att.mailbox.emailAddress = 'liwangwuhan@outlook.com';
    att.mailbox.routingType = 'SMTP';

    let att1 = new Attendee();
    att1.mailbox = new Mailbox();
    att1.mailbox.name = 'liwangchengmai';
    att1.mailbox.emailAddress = 'liwang-chengmai@outlook.com';
    att1.mailbox.routingType = 'SMTP';

    let att2 = new Attendee();
    att2.mailbox = new Mailbox();
    att2.mailbox.emailAddress = 'xiongwenshuai1@outlook.com';

    calendar_RequiredAttendees.requiredAttendees = [att, att1, att2];
    let itemField_calendar_RequiredAttendees: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_RequiredAttendees.fieldUri.valueOf(),
      item: calendar_RequiredAttendees,
    }
    itemFields.push(itemField_calendar_RequiredAttendees);
    items.itemId = { id: calendarItems[0].itemId.id, changeKey: calendarItems[0].itemId.changeKey };
    items.itemFields = itemFields;
    itemChanges.push(items);
    updateItems.itemChanges = itemChanges;

    updateItems.messageDisposition = MessageDisposition.SaveOnly;
    updateItems.conflictResolution = ConflictResolution.AlwaysOverwrite;
    updateItems.sendMeetingInvitationsOrCancellations = SendMeetingInvitations.SendToAllAndSaveCopy;
    updateItems.timeZoneDefinitionId = 'Pacific Standard Time';

    this.updateCalendar(updateItems);
  }

  /**
   * 批量处理日历项（使用 EWS 批量更新日历项）
   * 每次更新的时候，要重新查一遍，changKey会变化
   * pass
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAPNS8UAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP3Ih"}},"ConflictResults":{"Count":1}},{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAgENAAAAsQW7rr6HdUWKLvv+YhTBawAAAAZfMiIAAAA=","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP3Ij"}},"ConflictResults":{"Count":1}},{"ResponseClass":"Success","ResponseCode":"NoError","CalendarItem":{"ItemId":{"Id":"AQMkADAwATM3ZmYAZS03OTViLTRhMjYtMDACLTAwCgBGAAADp5+FtyHI4kKA/XTbWXuvdwcAsQW7rr6HdUWKLvv+YhTBawAAAAZfGqwAAACxBbuuvod1RYou+/5iFMFrAAAAB0GygAAAAA==","ChangeKey":"DwAAABYAAACxBbuuvod1RYou+/5iFMFrAAAHP3Il"}},"ConflictResults":{"Count":1}}]}}
   */
  calendarBatchUpdateItem() {
    this.changeRequestState();
    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }
    let updateItems = new UpdateItem();
    let itemChanges: ChangeItem[] = [];


    // updateItems.timeZoneDefinitionId = 'Pacific Standard Time';
    // let savedItemFolderId = new FolderIds();
    // let distinguishedFolderId = new DistinguishedFolderId();
    // distinguishedFolderId._id = FolderName.calendar;
    // savedItemFolderId.distinguishedFolderId = distinguishedFolderId;
    // updateItems.savedItemFolderId = savedItemFolderId;
    //
    // calendarItems.forEach(item => {
    //   //只修改普通事件的内容
    //   if (item.calendarItemType === 'Single') {
    //     let itemFields: ItemField[] = [];
    //     let item_Subject = new Calendar();
    //     item_Subject.fieldUri = FieldURISchema.Subject;
    //     item_Subject.subject = '批量更新内容为calendarBatchUpdateItem'
    //     let itemField_item_Subject: ItemField = {
    //       operateType: ItemOperateType.SetItemField,
    //       fieldURI: item_Subject.fieldUri.valueOf(),
    //       item: item_Subject,
    //     }
    //     itemFields.push(itemField_item_Subject);
    //
    //     let items = new ChangeItem();
    //     items.itemId = { id: item.itemId.id, changeKey: item.itemId.changeKey };
    //     items.itemFields = itemFields;
    //     itemChanges.push(items);
    //   }
    // });
    // updateItems.itemChanges = itemChanges;
    // updateItems.messageDisposition = MessageDisposition.SendAndSaveCopy;
    // updateItems.conflictResolution = ConflictResolution.AutoResolve;
    // updateItems.sendMeetingInvitationsOrCancellations = SendMeetingInvitations.SendToChangedAndSaveCopy;
    //
    // if (itemChanges.length > 0) {
    //   this.updateCalendar(updateItems);
    // }
  }

  /**
   * 访问定期系列（使用 EWS 访问定期系列中的日历项目）
   * error 设置的参数有问题？
   */
  calendarGetRelatedRecurrenceItems() {
    this.changeRequestState();
    let requestMessage: GetRelatedRecurrenceItemsMessage = new GetRelatedRecurrenceItemsMessage();
    requestMessage.setBaseShape(PropertySet.AllProperties);
    requestMessage.setFieldURIs([FieldURISchema.CalendarItemType, FieldURISchema.Start]);
    //recurringMasterId 要通过什么获取？
    requestMessage.setOccurrenceItemId({
      recurringMasterId: 'AQMkADAwATNiZmYAZC03NjUwLWI0MjktMDACLTAwCgFRAAgIANxPgy1awABGAAAC38y5GdJT/UGkr+wVtuRh1wcAk3rSMcKTfkq/Sv1SYitckQAAAgENAAAAk3rSMcKTfkq/Sv1SYitckQAAAAYbcDMAAAAQ',
      instanceIndex: 2
    })
    let anyTask: GetRelatedRecurrenceItemsTask = new GetRelatedRecurrenceItemsTask(new GetRelatedRecurrenceItemsRequest(requestMessage), properties);
    anyTask.execute().then((data) => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetRoomRoomList----->" + this.message);
    }, (err: ExchangeError) => {
      this.errorInfo('calendarGetRelatedRecurrenceItems', err);
    });
  }

  /**
   * 更新定期系列（使用 EWS 托管 API 修改序列中的所有匹配项）
   * error 设置的参数有问题？
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":[{"ResponseClass":"Error","ResponseCode":"ErrorInvalidPropertySet"}]}}
   */
  calendarUpdateRecurringSeries() {
    this.changeRequestState();

    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }
    let updateCalendarItem: Calendar | undefined = undefined;

    calendarItems.forEach(element => {
      if (element.calendarItemType === 'Occurrence') {
        updateCalendarItem = element;
      }
    });

    if (!updateCalendarItem) {
      this.message = '无定期事件';
      return;
    }

    let updateItems = new UpdateItem();
    let itemChanges: ChangeItem[] = [];
    let items = new ChangeItem();
    let itemFields: ItemField[] = [];

    let calendar_Location = new Calendar();
    calendar_Location.fieldUri = FieldURISchema.Location;
    calendar_Location.location = 'Conference Room 2';
    let itemField_calendar_Location: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_Location.fieldUri.valueOf(),
      item: calendar_Location,
    }
    itemFields.push(itemField_calendar_Location);

    let calendar_RequiredAttendees = new Calendar();
    calendar_RequiredAttendees.fieldUri = FieldURISchema.RequiredAttendees;
    let att = new Attendee();
    att.mailbox = new Mailbox();
    att.mailbox.name = 'liwangwuhan';
    att.mailbox.emailAddress = 'liwangwuhan@outlook.com';
    att.mailbox.routingType = 'SMTP';

    let att1 = new Attendee();
    att1.mailbox = new Mailbox();
    att1.mailbox.name = 'liwangchengmai';
    att1.mailbox.emailAddress = 'liwang-chengmai@outlook.com';
    att1.mailbox.routingType = 'SMTP';

    calendar_RequiredAttendees.requiredAttendees = [att, att1];
    let itemField_calendar_RequiredAttendees: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_RequiredAttendees.fieldUri.valueOf(),
      item: calendar_RequiredAttendees,
    }
    itemFields.push(itemField_calendar_RequiredAttendees);

    let calendar_Recurrence = new Calendar();
    calendar_Recurrence.fieldUri = FieldURISchema.Recurrence;
    calendar_Recurrence.recurrence = new Recurrence();
    calendar_Recurrence.recurrence.weeklyRecurrence = new WeeklyRecurrence();
    calendar_Recurrence.recurrence.weeklyRecurrence.interval = 1;
    calendar_Recurrence.recurrence.weeklyRecurrence.daysOfWeek = 'Monday';
    calendar_Recurrence.recurrence.endDateRecurrence = new EndDateRecurrence();
    calendar_Recurrence.recurrence.endDateRecurrence.startDate = '2024-05-06';
    calendar_Recurrence.recurrence.endDateRecurrence.endDate = '2024-06-22';
    let itemField_calendar_Recurrence: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_Recurrence.fieldUri.valueOf(),
      item: calendar_Recurrence,
    }
    itemFields.push(itemField_calendar_Recurrence);

    let calendar_MeetingTimeZone = new Calendar();
    calendar_MeetingTimeZone.fieldUri = FieldURISchema.MeetingTimeZone;
    calendar_MeetingTimeZone.timeZoneName = 'Pacific Standard Time';
    let itemField_calendar_MeetingTimeZone: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_MeetingTimeZone.fieldUri.valueOf(),
      item: calendar_MeetingTimeZone,
    }
    itemFields.push(itemField_calendar_MeetingTimeZone);
    items.itemId = {
      id: (updateCalendarItem as Calendar).itemId.id,
      changeKey: (updateCalendarItem as Calendar).itemId.changeKey
    };
    items.itemFields = itemFields;
    itemChanges.push(items);
    updateItems.itemChanges = itemChanges;

    updateItems.messageDisposition = MessageDisposition.SaveOnly;
    updateItems.conflictResolution = ConflictResolution.AlwaysOverwrite;
    updateItems.sendMeetingInvitationsOrCancellations = SendMeetingInvitations.SendToAllAndSaveCopy;
    updateItems.timeZoneDefinitionId = 'Pacific Standard Time';
    this.updateCalendar(updateItems);
  }

  /**
   * 修改定期系列中的约会（使用 EWS 托管 API 修改序列中的单个事件）
   * pass
   */
  calendarModifyARecurringSeries() {
    this.changeRequestState();

    if (!calendarItems || calendarItems.length <= 0) {
      this.message = '先调用 FindItem';
      return;
    }
    let updateCalendarItem: Calendar | undefined = undefined;

    calendarItems.forEach(element => {
      if (element.calendarItemType === 'Occurrence') {
        updateCalendarItem = element;
      }
    });

    if (!updateCalendarItem) {
      this.message = '无定期事件';
      return;
    }

    let updateItems = new UpdateItem();
    let itemChanges: ChangeItem[] = [];
    let items = new ChangeItem();
    let itemFields: ItemField[] = [];

    let item_Subject = new Calendar();
    item_Subject.fieldUri = FieldURISchema.Subject;
    item_Subject.subject = 'Weekly Update Meeting:Mandatory';
    let itemField_item_Subject: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: item_Subject.fieldUri.valueOf(),
      item: item_Subject,
    }
    itemFields.push(itemField_item_Subject);

    let calendar_Location = new Calendar();
    calendar_Location.fieldUri = FieldURISchema.Location;
    calendar_Location.location = 'Helipad of Contoso Bldg 1';
    let itemField_calendar_Location: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_Location.fieldUri.valueOf(),
      item: calendar_Location,
    }
    itemFields.push(itemField_calendar_Location);

    let calendar_Start = new Calendar();
    calendar_Start.fieldUri = FieldURISchema.Start;
    calendar_Start.start = '2024-04-27T19:33:00.000-07:00';
    let itemField_calendar_Start: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_Start.fieldUri.valueOf(),
      item: calendar_Start,
    }
    itemFields.push(itemField_calendar_Start);

    let calendar_End = new Calendar();
    calendar_End.fieldUri = FieldURISchema.End;
    calendar_End.end = '2024-04-27T20:33:00.000-07:00';
    let itemField_calendar_End: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_End.fieldUri.valueOf(),
      item: calendar_End,
    }
    itemFields.push(itemField_calendar_End);

    let calendar_RequiredAttendees = new Calendar();
    calendar_RequiredAttendees.fieldUri = FieldURISchema.RequiredAttendees;
    let att = new Attendee();
    att.mailbox = new Mailbox();
    att.mailbox.name = 'liwangwuhan';
    att.mailbox.emailAddress = 'liwangwuhan@outlook.com';
    att.mailbox.routingType = 'SMTP';

    let att1 = new Attendee();
    att1.mailbox = new Mailbox();
    att1.mailbox.name = 'liwangchengmai';
    att1.mailbox.emailAddress = 'liwang-chengmai@outlook.com';
    att1.mailbox.routingType = 'SMTP';

    calendar_RequiredAttendees.requiredAttendees = [att, att1];
    let itemField_calendar_RequiredAttendees: ItemField = {
      operateType: ItemOperateType.SetItemField,
      fieldURI: calendar_RequiredAttendees.fieldUri.valueOf(),
      item: calendar_RequiredAttendees,
    }
    itemFields.push(itemField_calendar_RequiredAttendees);

    items.itemId = {
      id: (updateCalendarItem as Calendar).itemId.id,
      changeKey: (updateCalendarItem as Calendar).itemId.changeKey
    };
    items.itemFields = itemFields;
    itemChanges.push(items);
    updateItems.itemChanges = itemChanges;
    updateItems.conflictResolution = ConflictResolution.AlwaysOverwrite;
    updateItems.sendMeetingInvitationsOrCancellations = SendMeetingInvitations.SendToAllAndSaveCopy;
    // updateItems.setTimeZoneDefinitionId('Pacific Standard Time')

    this.updateCalendar(updateItems);
  }

  /**
   * 获取会议室列表（使用 EWS 托管 API 获取所有会议室列表）
   * error
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":{"ResponseClass":"Error","ResponseCode":"ErrorADOperation"}}}
   */
  calendarGetRoomList() {
    this.changeRequestState();
    let requestMessage: GetRoomListsMessage = new GetRoomListsMessage();
    let anyTask: GetRoomListsTask = new GetRoomListsTask(new GetRoomListRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetRoomList----->" + this.message);
    }).catch((error: ExchangeError) => {
      this.errorInfo('calendarGetRoomList', error);
    });
  }

  /**
   * 获取会议室列表（使用 EWS 托管 API 获取会议室列表中的所有会议室）
   * error
   * {"data":{"requestServiceVersion":"Exchange2013","ResponseMessages":{"ResponseClass":"Error","ResponseCode":"ErrorADOperation"}}}
   */
  calendarGetRooms() {
    this.changeRequestState();
    let requestMessage: GetRoomsMessage = new GetRoomsMessage();
    requestMessage.setEmailAddress('wuhanliwang@outlook.com')
    let anyTask: GetRoomsTask = new GetRoomsTask(new GetRoomsRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetRooms----->" + this.message);
    }).catch((error: ExchangeError) => {
      this.errorInfo('calendarGetRooms', error);
    });
  }

  /**
   * 获取忙/闲信息（使用 EWS 托管 API 获取建议的会议时间和忙/闲信息）
   * error
   * 500
   */
  calendarGetSuggestedMeetingTimesAndFreeBusyInfo() {
    this.changeRequestState();
    let requestMessage: GetUserAvailabilityMessage = new GetUserAvailabilityMessage();
    //header
    requestMessage.setTimeZoneDefinition({
      name: '(UTC-08:00) Pacific Time (US &amp;amp; Canada)',
      id: 'Pacific Standard Time'
    });
    requestMessage.setPeriods([
      { bias: 'P0DT8H0M0.0S', name: 'Standard', id: 'Std' },
      { bias: 'P0DT7H0M0.0S', name: 'Daylight', id: 'Dlt/1' },
      { bias: 'P0DT7H0M0.0S', name: 'Daylight', id: 'Dlt/2007' },
    ]);
    requestMessage.setTransitionsGroups([
      {
        id: '0', recurringDayTransitions: [
        {
          kind: 'Period',
          to: 'Dlt/1',
          timeOffset: 'P0DT2H0M0.0S',
          month: 4,
          dayOfWeek: 'Sunday',
          occurrence: 1,
        },
        {
          kind: 'Period',
          to: 'Dlt/1',
          timeOffset: 'P0DT2H0M0.0S',
          month: 10,
          dayOfWeek: 'Sunday',
          occurrence: -1,
        },
      ]
      },
      {
        id: '1', recurringDayTransitions: [
        {
          kind: 'Period',
          to: 'Dlt/2007',
          timeOffset: 'P0DT2H0M0.0S',
          month: 3,
          dayOfWeek: 'Sunday',
          occurrence: 2,
        },
        {
          kind: 'Period',
          to: 'std',
          timeOffset: 'P0DT2H0M0.0S',
          month: 11,
          dayOfWeek: 'Sunday',
          occurrence: 1,
        },
      ]
      },
    ]);
    requestMessage.setTransitions({
      transition: { kind: 'Group', to: 0 },
      absoluteDateTransition: { kind: 'Group', to: 1, dateTime: '2007-01-01T08:00:00.000Z' }
    });

    //body
    requestMessage.setMailboxDataArray(
      [
        { address: 'mack@contoso.com', attendeeType: 'Organizer', excludeConflicts: false },
        { address: 'sadie@contoso.com', attendeeType: 'Required', excludeConflicts: false },
      ]
    );
    requestMessage.setFreeBusyViewOption({
      startTime: '2024-04-13T00:00:00',
      endTime: '2024-04-14T00:00:00',
      mergedFreeBusyIntervalInMinutes: 30,
      requestedView: 'FreeBusy',
    });
    requestMessage.setSuggestionsViewOption({
      goodThreshold: 49,
      maximumResultsByDay: 2,
      maximumNonWorkHourResultsByDay: 0,
      meetingDurationInMinutes: 60,
      minimumSuggestionQuality: 'Good',
      startTime: '2024-04-13T00:00:00',
      endTime: '2024-04-14T00:00:00'
    });

    let anyTask: GetUserAvailabilityTask = new GetUserAvailabilityTask(new GetUserAvailabilityRequest(requestMessage), properties);
    anyTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang-----calendarGetRoomRoomList----->" + this.message);
    }).catch((error: ExchangeError) => {
      this.errorInfo('calendarGetSuggestedMeetingTimesAndFreeBusyInfo', error);
    });
  }

  /**
   * update_item 方式更新
   * @param updateItems
   */
  updateCalendar(updateItems: UpdateItem) {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    let test = new UpdateItemRequest(updateItems);
    test.writeToXml(writer)
    let str = test.xmlToString(writer);
    console.log('liwang-------->update=' + str)

    let updateItemTask: UpdateItemTask = new UpdateItemTask(new UpdateItemRequest(updateItems), properties);
    updateItemTask.execute().then(data => {
      this.message = JSON.stringify(data);
      console.log("liwang------->data=" + this.message);
    }).catch((error: ExchangeError) => {
      this.errorInfo('updateCalendar', error);
    });
  }

  private errorInfo(api: string, err: ExchangeError) {
    this.message = JSON.stringify(err);
    console.error("liwang-----" + api + "----->error=" + this.message);
  }
}