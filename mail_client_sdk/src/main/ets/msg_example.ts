/* Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// import { open, openSync, readSync, statSync } from "fs";
// import {
//   MsgReader,
//   CFBReader,
//   DirectoryObjectType,
// } from "./format/msg";
// import type { DirectoryEntryTreeItem } from "./format/msg";
// import { getLogger } from "./utils/log";
//
// function readFile(fileName: string): Uint8Array {
//   // open file and read all data
//   const stat = statSync(fileName);
//   const fd = openSync(fileName, "r");
//   const buffer = Buffer.alloc(stat.size);
//   readSync(fd, buffer, 0, stat.size, 0);
//   return buffer;
// }
// const file = ["Re.msg", "test.msg", "testtest.msg", "fd.msg"][0];
//
// https://learn.microsoft.com/en-us/openspecs/exchange_server_protocols/ms-oxmsg/b046868c-9fbf-41ae-9ffb-8de2bd4eec82
// type Struct = {
//   name: string;
//   children: Struct[];
// };
// function getStructure(root: DirectoryEntryTreeItem): Struct {
//   return {
//     name: root.name,
//     children: root.children.map(child => getStructure(child)),
//   };
// }
// function printStructure(root: DirectoryEntryTreeItem): void {
//   _printStructure(root, "");
// }
// function _printStructure(root: DirectoryEntryTreeItem, prefix: string): void {
//   console.log(prefix + root.name);
//   if (root.children.length > 0) {
//     root.children.forEach(child => _printStructure(child, prefix + "  "));
//   }
// }
// function test(): void {
//   const buff = readFile(file);
//   const msg = new MsgReader(buff.buffer);
//   const cfbRoot = msg.parse();
// }
//
// test();
