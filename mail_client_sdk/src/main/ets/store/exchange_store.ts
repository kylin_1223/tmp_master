/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  Filter,
  FolderData,
  IBuffer,
  IBufferCreator,
  IStore,
  MailAttribute,
  MailBasic,
  Order,
  Properties,
  Query,
  StoreFeature,
} from '../api';
import { autoDiscoverUrl } from '../protocols/exchange/task/autodiscover_task';
import { AuthType, BasicCredentials, CredentialsHeaderStore } from '../protocols/exchange/utils/exchange_credentials';
import { getLogger, Logger } from '../utils/log';

const logger: Logger = getLogger("exchangeStore");

export class ExchangeStore implements IStore {
  private properties: Properties | null = null;
  private domainList: string[] = [
    'outlook.com',
    'hotmail.com',
    'gmail.cn',
    '163.com',
    '126.com',
    'qq.com',
    '139.com',
    '189.com',
    'wo.cn',
    'tom.com',
    'aliyun.com',
    'foxmail.com',
    'vip.163.com',
    'vip.126.com',
    'yeah.net',
    '188.com',
    'sina.com',
    'sina.cn',
    'sohu.com',
    'yahoo.com',
    'live.com',
    'live.cn',
  ];

  /**
   * 登录
   *
   * @param type
   */
  login(type?:AuthType): Promise<void> {
    if(!this.properties) {
      logger.error('properties is null');
      return;
    }
    if(!this.properties.userInfo.username || !this.properties.userInfo.password) {
      logger.error('username or password is null');
      return;
    }
    if (type === AuthType.OAUTH) {
      logger.info('auth2');
    } else if (type === AuthType.NTLM) {
      logger.info('ntlm');
    } else {
      CredentialsHeaderStore.getInstance().credentialHeader = new BasicCredentials
      (this.properties.userInfo.username, this.properties.userInfo.password);
    }
    return;
  }

  /**
   * 设置配置信息
   *
   * @param properties
   * @throws { ExchangeError } USERNAME_OR_PASSWORD_IS_NULL userName or passWord is null.
   */
  setProperties(properties: Properties) {
    this.properties = properties;
    if (!CredentialsHeaderStore.getInstance().credentialHeader) {
      this.login();
    }
  }

  public async autoDiscoverEwsUrl(callback?: (progress: number) => void): Promise<string> {
    if (!this.properties) {
      logger.error('autoDiscoverEwsUrl, properties is null');
      return '';
    }
    return autoDiscoverUrl(this.properties, callback);
  }

  /**
   * 获取联想邮箱地址
   *
   * @returns domainList
   */
  getDomainList(): string[] {
    return this.domainList;
  }

  check(): Promise<void> {
    return;
  }

  supportedFeatures(): StoreFeature[] {
    return [];
  }

  hasFeature(feature: StoreFeature): boolean {
    return false;
  }

  sync?() {
    return;
  }

  on(event: 'new-mail', handler: (folderFullName: string, mailIdList: string[]) => void);

  on(event: string, handler: Function);

  on(event: unknown, handler: unknown): void {
  }

  getMail(folderFullName: string, mailId: string): Promise<IBuffer> {
    return;
  }

  getMailPartContent(folderFullName: string, mailId: string, partId: string): Promise<IBuffer> {
    return;
  }

  getMailBasic(folderFullName: string, mailId: string): Promise<MailBasic> {
    return;
  }

  getMailIndex(folderFullName: string, mailId: string, order: Order): Promise<number> {
    return;
  }

  setMailAttributes(folderFullName: string, mailId: string, attributes: MailAttribute[], modifyType: '' | '+' | '-'): Promise<MailAttribute[]> {
    return;
  }

  createSubFolder(folderName: string, parent: string): Promise<void> {
    return;
  }

  renameFolder(folderName: string, newName: string): Promise<void> {
    return;
  }

  deleteFolder(folderName: string): Promise<void> {
    return;
  }

  getAllFolderList(): Promise<FolderData[]> {
    return;
  }

  getFolderList(parent: string): Promise<FolderData[]> {
    return;
  }

  getFolderInfo(folderName: string): Promise<FolderData> {
    return;
  }

  openFolder(folderName: string): Promise<FolderData> {
    return;
  }

  closeFolder(folderName: string): Promise<void> {
    return;
  }

  isFolderOpen(folderName: string): boolean {
    return;
  }

  getFolderMailIdList(folderName: string, start: number, size: number, order: Order): Promise<string[]> {
    return;
  }

  moveMail(fromFolderFullName: string, mailId: string, toFolderFullName: string): Promise<string> {
    return;
  }

  copyMail(fromFolderFullName: string, mailId: string, toFolderFullName: string): Promise<string> {
    return;
  }

  addMail(mail: string, folderName: string): Promise<string> {
    return;
  }

  deleteMail(fromFolder: string, mailId: string): Promise<void> {
    return;
  }

  filterMail(filter: Filter): Promise<Map<string, string[]>> {
    return;
  }

  searchMail(query: Query): Promise<string[]> {
    return;
  }

  release?() {
  }

  setBufferCreator(creator: IBufferCreator) {
  }
}