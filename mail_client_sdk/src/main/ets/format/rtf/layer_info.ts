/* Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class LayerInfo {
  private _ucValue:number;

  get UcValue():number {
    return this._ucValue;
  }
  set UcValue(value:number) {
    this._ucValue = value;
    this.UcValueCount = 0;
  }

  UcValueCount:number = 0;




public  checkUcValueCount():boolean {
  this.UcValueCount--;
  return this.UcValueCount < 0;
}

  /**
   * clone
   * @returns
   */
public  clone():LayerInfo {
  let info = new LayerInfo();
  info.UcValueCount = this.UcValueCount;
  info._ucValue = this._ucValue;
  return info;
}

}