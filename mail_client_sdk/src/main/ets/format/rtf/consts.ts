/* Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Consts {
   static  FromHtml = "fromhtml";
  static  HtmlRtf = "htmlrtf";
  static  HtmlTag = "htmltag";
  static  Tab = "tab";
  static  Lquote = "lquote";
  static  Rquote = "rquote";
  static  LdblQuote = "ldblquote";
  static  RdblQuote = "rdblquote";
  static  Bullet = "bullet";
  static  Endash = "endash";
  static  Emdash = "emdash";
  static  Tilde = "~";
  static  Apostrophe = "'";
  static  Underscore = "_";
  static  Af = "af";
  static  Rtf = "rtf";
  static  Fonttbl = "fonttbl";
  static  Info = "info";
  static  Author = "author";
  static  Creatim = "creatim";
  static  Version = "version";
  static  Ansi = "ansi";
  static  Ansicpg = "ansicpg";
  static  Deff = "deff";
  static  DefLang = "deflang";
  static  Fnil = "fnil";
  static  Fcharset = "fcharset";
  static  Generator = "generator";
  static  Pntext = "pntext";
  static  Pntxtb = "pntxtb";
  static  Par = "par";
  static  Plain = "plain";
  static  F = "f";
  static  U = "u";
  static  Uc = "uc";
  static  Lang = "lang";
}