/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { base64Encode } from '../../../utils/encodings';
import { ClientErrorCode, ErrorType, ExchangeError } from '../error/exchange_error';

/**
 * 登录方式枚举
 *
 * @since 2024-03-19
 */
export enum AuthType {
  /**
   * Basic登录
   */
  BASIC = 'Basic',

  /**
   * Ntlm登录
   */
  NTLM = 'Ntlm',

  /**
   * OAuth登录
   */
  OAUTH = 'OAuth',
}

/**
 * 导出接口ExchangeCredentials
 *
 * @since 2024-03-19
 */
export interface ExchangeCredentialsHeader {
  /**
   * 获取header
   *
   * @returns header对象
   */
  getCredentialsHeader(): object;
}

/**
 * BasicCredentials类,basic的登录方式
 *
 * @since 2024-03-19
 */
export class BasicCredentials implements ExchangeCredentialsHeader {
  /**
   * basic登录构造器
   *
   * @param userName 用户名
   * @param password 用户密码
   */
  public constructor(private userName: string, private password: string) {
    if (!userName || !password) {
      throw new ExchangeError(ErrorType.CLIENT_ERROR, 'userName or passWord is null.',
        ClientErrorCode[ClientErrorCode.USERNAME_OR_PASSWORD_IS_NULL]);
    }
  }

  /**
   * 获取basic凭证
   *
   * @returns basic凭证
   */
  public getCredentialsHeader(): object {
    return {
      'Content-Type': 'text/xml',
      'Authorization': `Basic ${base64Encode(`${this.userName}:${this.password}`)}`,
    };
  }
}

/**
 * OAuthCredentials类,OAuth的登录方式
 *
 * @since 2024-03-19
 */
export class OAuthCredentials implements ExchangeCredentialsHeader {
  /**
   * OAuth登录构造器
   *
   * @param token 请求token
   */
  public constructor(private token: string) {
  }

  /**
   * 获取OAuth凭证
   *
   * @returns OAuth凭证
   */
  public getCredentialsHeader(): object {
    return {
      'Content-Type': 'text/xml',
      'Authorization': `Bearer ${this.token}`,
    };
  }
}

/**
 * NtlmCredentials类,Ntlm的登录方式
 *
 * @since 2024-03-19
 */
export class NtlmCredentials implements ExchangeCredentialsHeader {
  /**
   * ntlm登录构造器
   *
   * @param token 请求token
   */
  public constructor(private token: string) {
  }

  /**
   * 获取ntlm凭证
   *
   * @returns ntlm凭证
   */
  public getCredentialsHeader(): object {
    return {
      'Content-Type': 'text/xml',
      'Authorization': `Ntlm ${this.token}`,
      'Connection': 'Close',
    };
  }
}

/**
 * header储存
 *
 * @since 2024-03-28
 */
export class CredentialsHeaderStore {
  private static INSTANCE: CredentialsHeaderStore;
  private _credentialHeader: ExchangeCredentialsHeader;

  /**
   * 单例实现私有化构造器
   */
  private constructor() {
  }

  /**
   * 生成实例
   *
   * @returns CredentialsHeaderStore实例
   */
  public static getInstance(): CredentialsHeaderStore {
    if (!CredentialsHeaderStore.INSTANCE) {
      CredentialsHeaderStore.INSTANCE = new CredentialsHeaderStore();
    }
    return CredentialsHeaderStore.INSTANCE;
  }

  /**
   * 设置凭据
   *
   * @param credentialHeader
   */
  public set credentialHeader(credentialHeader: ExchangeCredentialsHeader){
    this._credentialHeader = credentialHeader;
  }

  /**
   * 获取header
   *
   * @returns _credentialHeader
   */
  public get credentialHeader(): ExchangeCredentialsHeader {
    return this._credentialHeader;
  }
}
