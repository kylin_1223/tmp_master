/**
 * 格式化字符串
 *
 * @param formatter字符串模板
 * @param args参数列表
 * @since 2024-04-1
 */
export function formatString(str: string, ...args: any[]): string {
  return str.replace(/\{(\d+)\}/g, (match, index) => {
    return typeof args[index] !== "undefined" ? args[index] : match;
  });
}