/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 消息操作描述
 */
export enum MessageDisposition {
  SaveOnly = 1,
  SendOnly = 2,
  SendAndSaveCopy = 3,
}

export enum SendMeetingInvitations {
  SendToNone = 1,
  SendOnlyToAll = 2,
  SendOnlyToChanged = 3,
  SendToAllAndSaveCopy = 4,
  SendToChangedAndSaveCopy = 5,
}

/**
 * 协议版本号
 */
export enum ExchangeVersion {
  Exchange2007_SP1 = 0,
  Exchange2010 = 1,
  Exchange2010_SP1 = 2,
  Exchange2010_SP2 = 3,
  Exchange2013 = 4,
  Exchange2013_SP1 = 5,
}

/**
 * 返回内容范围
 */
export enum PropertySet {
  /**
   * 仅返回ID
   */
  IdOnly = 'IdOnly',

  /**
   * 返回定义为默认值的一组属性
   */
  Default = 'Default',

  /**
   * 返回 Exchange 业务逻辑层用于构造文件夹的所有属性
   */
  AllProperties = 'AllProperties',
}

/**
 * 同步项目作用域
 */
export enum SyncFolderItemsScope {

  /**
   * 普通项
   */
  NormalItems = 'NormalItems',

  /**
   * 普通项和关联项
   */
  NormalAndAssociatedItems = 'NormalAndAssociatedItems',
}

/**
 * 便利类型
 */
export enum TraversalType {
  Shallow = 'Shallow',
  SoftDeleted = 'SoftDeleted',
  Associated = 'Associated'
}

/**
 * ExtendedFieldURI -> DistinguishedPropertySetId 属性
 */
export enum DistinguishedPropertySetId {
  /**
   * 按 地址属性集ID 标识
   */
  Address = 1,

  /**
   * 按 约会属性集ID 标识
   */
  Appointment,

  /**
   * 按 日历助手属性集ID 标识
   */
  CalendarAssistant,

  /**
   * 按 公共属性集ID 标识
   */
  Common,

  /**
   * 按 Internet 标头属性集ID 标识
   */
  InternetHeaders,

  /**
   * 按 会议属性集ID 标识
   */
  Meeting,

  /**
   * 按 公用ID 标识
   */
  Sharing,

  /**
   * 按 公共字符串属性集ID 标识
   */
  PublicStrings,

  /**
   * 按 任务属性集ID 标识
   */
  Task,

  /**
   * 按 统一消息属性集ID 标识
   */
  UnifiedMessaging,
}

/**
 * ExtendedFieldURI -> PropertyType 属性
 */
export enum PropertyType {
  /**
   * 日期和时间的双精度值。 整数部分是日期，分数部分是时间
   */
  ApplicationTime = 1,

  /**
   * 日期和时间的双精度值的数组
   */
  ApplicationTimeArray,

  /**
   * Base64 编码的二进制值
   */
  Binary,

  /**
   * Base64 编码的二进制值的数组
   */
  BinaryArray,

  /**
   * 布尔值 true 或 false
   */
  Boolean,

  /**
   * GUID 字符串
   */
  CLSID,

  /**
   * GUID 字符串的数组
   */
  CLSIDArray,

  /**
   * 美分数的 64 位整数
   */
  Currency,

  /**
   * 一个由 64 位整数构成的数组，这些整数被解释为美分数
   */
  CurrencyArray,

  /**
   * 64 位浮点值
   */
  Double,

  /**
   * 包含 64 位浮点值的数组
   */
  DoubleArray,

  /**
   * SCODE 值;32 位无符号整数。不用于限制或获取/设置值。 这仅适用于报告。
   */
  Error,

  /**
   * 32 位浮点值
   */
  Float,

  /**
   * 32 位浮点值的数组
   */
  FloatArray,

  /**
   * 有符号 32 位 (Int32) 整数
   */
  Integer,

  /**
   * 有符号的 32 位 (Int32) 整数的数组
   */
  IntegerArray,

  /**
   * 有符号或无符号 64 位 (int64) 整数
   */
  Long,

  /**
   * 有符号或无符号 64 位 (int64) 整数的数组
   */
  LongArray,

  /**
   * 指示无属性值
   */
  Null,

  /**
   * 指向实现 IUnknown 接口的 对象的指针
   */
  Object,

  /**
   * 指向实现 IUnknown 接口的 对象的指针数组
   */
  ObjectArray,

  /**
   * 有符号 16 位整数
   */
  Short,

  /**
   * 带符号 16 位整数的数组
   */
  ShortArray,

  /**
   * FILETIME 结构形式的 64 位整数数据和时间值
   */
  SystemTime,

  /**
   * 由 FILETIME 结构形式的 64 位整数数据和时间值组成的数组
   */
  SystemTimeArray,

  /**
   * Unicode 字符串
   */
  String,

  /**
   * Unicode 字符串的数组
   */
  StringArray,
}

/**
 * 邮箱用户的邮箱类型
 */
export enum MailboxType {
  /**
   * 启用邮件的 Active Directory 对象
   */
  Mailbox = 1,

  /**
   * 公共分发列表
   */
  PublicDL,

  /**
   * 用户邮箱中的私人通讯组列表
   */
  PrivateDL,

  /**
   * 用户邮箱中的联系人
   */
  Contact,

  /**
   * 公用文件夹
   */
  PublicFolder,

  /**
   * 未知类型的邮箱
   */
  Unknown,

  /**
   * 个人通讯组列表中的一次性成员
   */
  OneOff,

  /**
   * 组邮箱
   */
  GroupMailbox,
}

/**
 * 清空或删除模式
 */
export enum DeleteType {
  /**
   * 从存储区中永久删除项目
   */
  HardDelete = 1,

  /**
   * 如果启用了垃圾站，项目将移动到垃圾站
   */
  SoftDelete = 2,

  /**
   * 文件被移至“已删除项目”文件夹
   */
  MoveToDeletedItems = 3
}
