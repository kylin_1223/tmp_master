
const fillZero = (d: number) => {
  return d.toString().padStart(2, '0');
}
export const formatTimeToUtcString = (t: Date) => {
  const year = t.getUTCFullYear();
  const month = fillZero(t.getUTCMonth() + 1);
  const date = fillZero(t.getUTCDate());
  const hour = fillZero(t.getUTCHours());
  const minutes = fillZero(t.getUTCMinutes());
  const seconds = fillZero(t.getUTCSeconds());
  return `${year}-${month}-${date}T${hour}:${minutes}:${seconds}`;
}

export const booleanToString = (b: boolean) => {
  return b ? 'true' : 'false';
}