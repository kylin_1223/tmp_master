/* Copyright © 2024 Coremail论客. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { JsObjectElement } from '../xml/soap_xml';
import { util } from '@kit.ArkTS';
import { XmlElement } from '../xml';

export function getAuthUser() {
  // return { address: 'wuhanliwang@outlook.com', pwd: 'liwang2023@137' };
  return { address: 'liwang-personal@outlook.com', pwd: 'liwang2024@137' };
}

export function getEncodeBase64():string{
  return encodeBase64(getAuthUser().address, getAuthUser().pwd);
}

export function encodeBase64(account:string, pwd: string):string{
  let encodedCredentials = `${account}:${pwd}`
  let strToAscii: number[] = [];
  for (let i = 0;i < encodedCredentials.length; i++) {
    strToAscii.push(encodedCredentials.charCodeAt(i));
  }
  let that = new util.Base64Helper();
  let encodeNum = new Uint8Array(strToAscii);
  return that.encodeToStringSync(encodeNum)
}

export function fillObjectFields(element?: JsObjectElement, object?: any){
  element._elements.forEach(fieldElement => {
    let fieldName = fieldElement._name.charAt(0).toLowerCase() + fieldElement._name.substring(1);
    if (fieldElement._attributes) {
      switch (fieldElement._name) {
        case XmlElement.NAME_FOLDER_ID:
        case XmlElement.NAME_PARENT_FOLDER_ID:
        case XmlElement.NAME_ITEM_ID:
        case XmlElement.NAME_CONVERSATION_ID:
          object[fieldName] = {
            id: fieldElement._attributes.Id,
            changeKey: fieldElement._attributes.ChangeKey
          };
          break;
        case XmlElement.NAME_BODY:
          object[fieldName] = {
            bodyType: fieldElement._attributes.BodyType,
            isTruncated: fieldElement._attributes.IsTruncated,
            content: fieldElement._attributes.Content
          };
          break;
        default:
          console.log('liwang-----待补全----->name='+ fieldElement._name + '=' + JSON.stringify(fieldElement));
          break;
      }
    }else if (fieldElement._elements && fieldElement._elements.length > 0) {
      object[fieldName] = fieldElement._elements[0]._text;
    }else {
      //节点下无值
      // console.log('liwang----->name='+ fieldElement._name +' 无法赋值=' + JSON.stringify(fieldElement))
    }
  });
}