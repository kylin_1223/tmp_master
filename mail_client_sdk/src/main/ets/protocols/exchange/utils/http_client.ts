/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from "@ohos.net.http";
import { ErrorType, ExchangeError } from '../error/exchange_error';
import { CredentialsHeaderStore } from './exchange_credentials';

interface Listeners {
  onDataReceive?: Function,
  onDataReceiveProgress?: Function,
}


/**
 * Http请求工具类
 *
 * @since 2024-03-19
 */
export abstract class HttpRequestBase {
  protected url: string;
  protected currentRequest?: http.HttpRequest;
  protected options: http.HttpRequestOptions;
  protected header: object;

  /**
   * header构造器
   */
  public constructor() {
    this.header = CredentialsHeaderStore.getInstance().credentialHeader?.getCredentialsHeader() ?? {};
  }

  /**
   * 设置请求路径
   *
   * @param url 请求路径
   * @returns Http请求工具类
   */
  public setUrl(url: string): HttpRequestBase {
    this.url = url;
    return this;
  }

  /**
   * 设置ca证书路径
   *
   * @param caPath ca证书路径
   * @returns Http请求工具类
   */
  public setCaPath(caPath: string): HttpRequestBase {
    this.options.caPath = caPath;
    return this;
  }

  /**
   * 设置请求头
   *
   * @param header 请求头
   * @returns Http请求工具类
   */
  public setHeader(header: Object): HttpRequestBase {
    this.options.header = header;
    return this;
  }

  /**
   * Http销毁
   */
  public destroy(): void {
    this.currentRequest?.destroy();
  }

  /**
   * 普通请求
   *
   * @param option 可选参数 自配置请求选项
   * @returns promise 响应对象
   */
  public abstract request(option?: http.HttpRequestOptions): Promise<http.HttpResponse>;

  /**
   * 流式请求
   *
   * @param option 可选参数 自配置请求选项
   * @param onDataReceive 回调函数获取数据
   * @returns promise 响应数据
   */
  public abstract requestInStream(option?: http.HttpRequestOptions, listeners?: Listeners): Promise<number>;
}

/**
 * Http请求工具类
 *
 * @since 2024-03-19
 */
export class HttpRequest extends HttpRequestBase {
  /**
   * HttpRequest构造器
   */
  public constructor() {
    super();
    this.options = {
      method: http.RequestMethod.POST,
      header: this.header,
      expectDataType: http.HttpDataType.STRING,
      usingCache: true,
      priority: 1,
      readTimeout: 60000,
      connectTimeout: 60000,
      usingProtocol: http.HttpProtocol.HTTP1_1,
    };
  }

  /**
   * 普通请求
   *
   * @param option 可选参数 自配置请求选项
   * @returns promise 响应对象
   */
  public async request(option?: http.HttpRequestOptions): Promise<http.HttpResponse> {
    let dstOptions: http.HttpRequestOptions = {
      ...this.options,
      ...option,
    };
    let httpRequest: http.HttpRequest = http.createHttp();
    this.currentRequest = httpRequest;
    let httpResp: http.HttpResponse = await httpRequest.request(this.url, dstOptions);
    httpRequest.destroy();
    return httpResp;
  }

  public requestInStream(option?: http.HttpRequestOptions, listeners?: Listeners): Promise<number> {
    throw new ExchangeError(ErrorType.HTTP_ERROR, 'HttpRequest method is not implemented.', ErrorType.SYS_COMMON_ERROR);
  }
}

/**
 * Http流式请求工具类
 *
 * @since 2024-03-19
 */
export class HttpStreamRequest extends HttpRequestBase {
  /**
   * HttpStreamRequest构造器
   */
  public constructor() {
    super();
    this.options = {
      method: http.RequestMethod.POST,
      header: this.header,
      expectDataType: http.HttpDataType.STRING,
      usingCache: true,
      priority: 1,
      readTimeout: 60 * 1000 * 30,
      connectTimeout: 6000,
      usingProtocol: http.HttpProtocol.HTTP1_1,
    };
  }

  /**
   * 流式请求
   * @param option 可选参数 自配置请求选项
   * @param onDataReceive 回调函数获取数据
   * @returns promise 响应数据
   */
  async requestInStream(option?: http.HttpRequestOptions, listeners?: Listeners): Promise<number> {
    let dstOptions: http.HttpRequestOptions = {
      ...this.options,
      ...option,
    };
    let httpRequest: http.HttpRequest = http.createHttp();
    this.currentRequest = httpRequest;
    httpRequest.on('dataReceive', (data: ArrayBuffer) => {
      listeners.onDataReceive(data);
    });
    httpRequest.on('dataReceiveProgress', (data: http.DataReceiveProgressInfo) => {
      console.log('2323',data);
      listeners.onDataReceiveProgress(data);
    });
    let httpResp: number = await httpRequest.requestInStream(this.url, dstOptions);
    httpRequest.off('dataReceive');
    httpRequest.destroy();
    return httpResp;
  }

  public request(option?: http.HttpRequestOptions): Promise<http.HttpResponse> {
    throw new ExchangeError(ErrorType.HTTP_ERROR, 'HttpStreamRequest method is not implemented.', ErrorType.SYS_COMMON_ERROR);
  }
}