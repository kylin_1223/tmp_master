/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderField, FolderOperateType } from '../../model/folder/folder_change';
import { UpdateFolder } from '../../model/folder/update_folder';
import { FolderChange } from '../../model/folder/folder_change';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 更新文件夹请求
 *
 * @since 2024-03-19
 */
export class UpdateFolderRequest extends RequestBase<UpdateFolder> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_UPDATE_FOLDER);
    let folderChanges = this.rawData.folderChanges;
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_CHANGES);
    folderChanges.forEach((folderChange: FolderChange) => {
      this.writeFolderChange(soapXmlWriter, folderChange);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 根据传入值写入folderChange标签
   *
   * @param soapXmlWriter xml写入器
   * @param folderChange 更新文件夹的信息
   */
  protected writeFolderChange(soapXmlWriter: SoapXmlWriter, folderChange: FolderChange): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_CHANGE);
    const folderArr: Map<string, string> = new Map();
    if (folderChange?.distinguishedFolderId?._id) {
      folderArr.set(XmlAttribute.NAME_ID, folderChange.distinguishedFolderId._id);
      folderArr.set(XmlAttribute.NAME_CHANGE_KEY, folderChange.distinguishedFolderId._changeKey ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID, folderArr);
    }
    if (folderChange?.folderId?.id) {
      folderArr.set(XmlElement.NAME_ID, folderChange.folderId.id);
      folderArr.set(XmlElement.NAME_CHANGE_KEY, folderChange.folderId.changeKey ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID, folderArr);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_UPDATES);
    const folderFields = folderChange.folderFields;
    folderFields.forEach((folderField: FolderField) => {
      this.writeFolderField(soapXmlWriter, folderField);
    })
    soapXmlWriter.writeEndElement();
  }

  /**
   * 根据传入值写入对应的folderField
   *
   * @param soapXmlWriter xml写入器
   * @param folderField 新增、删除或者修改类型及相关信息
   */
  protected writeFolderField(soapXmlWriter: SoapXmlWriter, folderField: FolderField): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, FolderOperateType[folderField.operateType]);
    if (folderField.fieldURI) {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I, new Map([[XmlAttribute.NAME_FIELD_URI, folderField.fieldURI]]));
    }
    if (folderField.indexedFieldURI) {
      let indexedFieldURIMap = new Map();
      indexedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.indexedFieldURI?.fieldURI ?? '');
      indexedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.indexedFieldURI?.fieldIndex ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_INDEXED_FIELD_URL, indexedFieldURIMap);
    }
    if (folderField.extendedFieldURI) {
      let extendedFieldURIMap = new Map();
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.distinguishedPropertySetId ?? '');
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.propertySetId ?? '');
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.propertyTag ?? '');
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.propertyName ?? '');
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.propertyId ?? '');
      extendedFieldURIMap.set(XmlElement.NAME_FIELD_UR_I, folderField.extendedFieldURI?.propertyType ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL, extendedFieldURIMap);
    }
    if (!(folderField.operateType === FolderOperateType.DeleteFolderField)) {
      folderField.folder?.writeToXml(soapXmlWriter);
      folderField.contactsFolder?.writeToXml(soapXmlWriter);
      folderField.searchFolder?.writeToXml(soapXmlWriter);
      folderField.calenderFolder?.writeToXml(soapXmlWriter);
      folderField.tasksFolder?.writeToXml(soapXmlWriter);
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}