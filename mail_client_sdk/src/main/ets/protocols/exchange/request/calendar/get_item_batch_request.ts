/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetItemBatchMessage } from '../../model/calendar/get_item_batch_message';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 批量获取日历项报文
 */
export class GetItemBatchRequest extends RequestBase<GetItemBatchMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_HEADER);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_REQUEST_SERVER_VERSION,
      new Map([[XmlAttribute.NAME_VERSION, this.rawData.getVersionString()]]));
    if (this.rawData.timeZoneDefinitionId) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_CONTEXT)
        .writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_DEFINITION, new Map([
          [XmlAttribute.NAME_ID, this.rawData.timeZoneDefinitionId]
        ])).writeEndElement();
    }
    soapXmlWriter.writeEndElement();
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ITEM);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, new Map(), this.rawData.baseShape.valueOf());
    if (this.rawData.fieldURIs && this.rawData.fieldURIs.length > 0) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
      this.rawData.fieldURIs.forEach(element => {
        soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI, new Map([
          [XmlAttribute.NAME_FIELD_URI, element.valueOf()]
        ]));
      });
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_IDS);
    this.rawData.itemIds.forEach(element => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ID_ITEM, new Map([
        [XmlAttribute.NAME_ID, element.id],
        [XmlAttribute.NAME_CHANGE_KEY, element.changeKey]
      ]));
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}