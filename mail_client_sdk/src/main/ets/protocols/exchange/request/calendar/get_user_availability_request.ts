/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetUserAvailabilityMessage } from '../../model/calendar/get_user_availability_message';
import { ExchangeVersion } from '../../utils/common_enum';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 获取忙/闲信息（使用 EWS 托管 API 获取建议的会议时间和忙/闲信息）
 */
export class GetUserAvailabilityRequest extends RequestBase<GetUserAvailabilityMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_HEADER);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_REQUEST_SERVER_VERSION,
      new Map([[XmlAttribute.NAME_VERSION, ExchangeVersion[this.rawData.version]]]));

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_CONTEXT);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TIME_ZONE_DEFINITION)
      .writeAttribute(XmlAttribute.NAME_ID, this.rawData.getTimeZoneDefinition().id)
      .writeAttribute(XmlAttribute.NAME_NAME, this.rawData.getTimeZoneDefinition().name);

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_PERIODS)
    this.rawData.getPeriods().forEach(element => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_PERIOD, new Map([
        [XmlAttribute.NAME_BIAS, element.bias],
        [XmlAttribute.NAME_NAME, element.name],
        [XmlAttribute.NAME_ID, element.id],
      ]));
    });
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TRANSITIONS_GROUPS)
    this.rawData.getTransitionsGroups().forEach(element => {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TRANSITIONS_GROUP)
        .writeAttribute(XmlAttribute.NAME_ID, element.id);
      element.recurringDayTransitions.forEach(dayElement => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_RECURRING_DAY_TRANSITION);
        soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_TO, new Map([
          [XmlAttribute.NAME_KIND, dayElement.kind],
        ]), dayElement.to);
        soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_TIME_OFFSET, new Map(), dayElement.timeOffset);
        soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MONTH, new Map(), String(dayElement.month));
        soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_DAY_OF_WEEK, new Map(), String(dayElement.dayOfWeek));
        soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_OCCURRENCE, new Map(), String(dayElement.occurrence));
        soapXmlWriter.writeEndElement();
      });
      soapXmlWriter.writeEndElement();
    });
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TRANSITIONS)
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TRANSITION)
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_TO, new Map([
      [XmlAttribute.NAME_KIND, this.rawData.getTransitions().transition.kind],
    ]), String(this.rawData.getTransitions().transition.to));
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ABSOLUTE_DATE_TRANSITION)
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_TO, new Map([
      [XmlAttribute.NAME_KIND, this.rawData.getTransitions().absoluteDateTransition.kind],
    ]), String(this.rawData.getTransitions().absoluteDateTransition.to));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_DATE_TIME, new Map(), this.rawData.getTransitions().absoluteDateTransition.dateTime);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();


    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_USER_AVAILABILITY_REQUEST);

    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_MAILBOX_DATA_ARRAY);
    this.rawData.getMailboxDataArray().forEach(element => {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_MAILBOX_DATA);

      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EMAIL);
      soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_ADDRESS, new Map(), element.address);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_ATTENDEE_TYPE, new Map(), element.attendeeType);
      soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_EXCLUDE_CONFLICTS, new Map(), String(element.excludeConflicts));

      soapXmlWriter.writeEndElement();
    });
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FREE_BUSY_VIEW_OPTIONS);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_TIME_WINDOW);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_START_TIME, new Map(), this.rawData.getFreeBusyViewOption().startTime);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_END_TIME, new Map(), this.rawData.getFreeBusyViewOption().endTime);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MERGED_FREE_BUSY_INTERVAL_IN_MINUTES, new Map(), String(this.rawData.getFreeBusyViewOption().mergedFreeBusyIntervalInMinutes));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_REQUESTED_VIEW, new Map(), this.rawData.getFreeBusyViewOption().requestedView);
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_SUGGESTIONS_VIEW_OPTIONS);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_GOOD_THRESHOLD, new Map(), String(this.rawData.getSuggestionsViewOption().goodThreshold));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MAXIMUM_RESULTS_BY_DAY, new Map(), String(this.rawData.getSuggestionsViewOption().maximumResultsByDay));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MAXIMUM_NON_WORK_HOUR_RESULTS_BY_DAY, new Map(), String(this.rawData.getSuggestionsViewOption().maximumNonWorkHourResultsByDay));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MEETING_DURATION_IN_MINUTES, new Map(), String(this.rawData.getSuggestionsViewOption().meetingDurationInMinutes));
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_MINIMUM_SUGGESTION_QUALITY, new Map(), this.rawData.getSuggestionsViewOption().minimumSuggestionQuality);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DETAILED_SUGGESTIONS_WINDOW);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_START_TIME, new Map(), this.rawData.getFreeBusyViewOption().startTime);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE,  XmlElement.NAME_END_TIME, new Map(), this.rawData.getFreeBusyViewOption().endTime);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}