/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateAppointmentMessage } from '../../model/calendar/create_appointment_message';
import { XmlNamespace } from '../../xml/xml_namespace';
import { SoapXmlWriter, XmlElement } from '../../xml/index';
import { CreateItemRequest } from '../create_item_request';
import { BodyType, Sensitivity, Importance } from '../../model/mail/item_message';

export enum FreeBusyStatus {
  Free, Tentative, Busy, OOF, WorkingElsewhere, NoData
}

export class CreateAppointmentItemRequest extends CreateItemRequest<CreateAppointmentMessage> {
  protected writeItemsChildren(soapXmlWriter: SoapXmlWriter): void {
    for (const item of this.rawData.items) {

      soapXmlWriter.writeElementWithAttribute(
        XmlNamespace.TYPE,
        XmlElement.NAME_CALENDAR_ITEM,
        new Map([[XmlNamespace.XMLNS, XmlNamespace.SCHEMA_TYPE]]),
        false
      );
      // Subject
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_SUBJECT)
        .writeElementValue(item.subject)
        .writeEndElement();
      // Body
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_BODY)
        .writeAttribute('BodyType', BodyType[item.bodyType])
        .writeElementValue(item.bodyText)
        .writeEndElement();
      // ReminderIsSet
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_REMINDER_IS_SET)
        .writeElementValueFromBoolean(item.reminderIsSet)
        .writeEndElement();
      // ReminderIsSet
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_REMINDER_IS_SET)
        .writeElementValueFromBoolean(item.reminderIsSet)
        .writeEndElement();
      // ReminderMinutesBeforeStart
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_REMINDER_MINUTES_BEFORE_START)
        .writeElementValueFromNumber(item.reminderMinutesBeforeStart)
        .writeEndElement();
      // Start
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_START)
        .writeElementValueFromDate(item.start)
        .writeEndElement();
      // End
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_END)
        .writeElementValueFromDate(item.end)
        .writeEndElement();
      // IsAllDayEvent
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_IS_ALL_DAY_EVENT)
        .writeElementValueFromBoolean(item.isAllDayEvent)
        .writeEndElement();
      // LegacyFreeBusyStatus
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_LEGACY_FREE_BUSY_STATUS)
        .writeElementValue(FreeBusyStatus[item.legacyFreeBusyStatus])
        .writeEndElement();
      // Location
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_LOCATION)
        .writeElementValue(item.location)
        .writeEndElement();
      // sensitivity
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_SENSITIVITY)
        .writeElementValue(Sensitivity[item.sensitivity])
        .writeEndElement();
      // importance
      soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_IMPORTANCE)
        .writeElementValue(Importance[item.importance])
        .writeEndElement();

      if (item.requiredAttendees) {
        writeAttendees(soapXmlWriter, item.requiredAttendees);
      }
      if (item.optionalAttendees) {
        writeAttendees(soapXmlWriter, item.optionalAttendees, XmlElement.NAME_OPTIONAL_ATTENDEES);
      }
      soapXmlWriter.writeEndElement();
    }
  }
}

function writeAttendees(soapXmlWriter: SoapXmlWriter, attendees: string[], attendeeCategory?: string) {
  soapXmlWriter.writeStartElementWithoutNamespace(attendeeCategory ?? XmlElement.NAME_REQUIRED_ATTENDEES)

  for (const attendee of attendees) {
    soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_ATTENDEE)
    soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_MAILBOX)

    soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_EMAIL_ADDRESS)
    soapXmlWriter.writeElementValue(attendee)
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  soapXmlWriter.writeEndElement();
}
