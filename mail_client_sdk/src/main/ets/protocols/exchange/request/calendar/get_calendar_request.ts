/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetCalendarMessage } from '../../model/calendar/get_calendar_message';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { FolderName, XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 获取日历文件夹报文
 */
export class GetCalendarRequest extends RequestBase<GetCalendarMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_FOLDER);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_SHAPE);
    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, new Map(), this.rawData.baseShape.valueOf())
      .writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FOLDER_IDS)
      .writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID,
      new Map([[XmlAttribute.NAME_ID, FolderName[this.rawData.folderId].toLowerCase()]]))
      .writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}