/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetRoomsMessage } from '../../model/calendar/get_rooms_message';
import { SoapXmlWriter } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base'

/**
 * 获取会议室列表（使用 EWS 托管 API 获取会议室列表中的所有会议室）
 */
export class GetRoomsRequest extends RequestBase<GetRoomsMessage> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_GET_ROOMS);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ROOM_LIST);

    soapXmlWriter.writeIntactElement(XmlNamespace.TYPE, XmlElement.NAME_EMAIL_ADDRESS, new Map(), this.rawData.getEmailAddress());

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}