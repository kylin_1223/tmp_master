/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateCalendarsMessage } from '../../model/calendar/create_calendar_message';
import { FolderName, XmlAttribute } from '../../xml/xml_attribute';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';
import { SoapXmlWriter, XmlElement } from '../../xml';


export class CreateCalendarRequest extends RequestBase<CreateCalendarsMessage> {

  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  private writeFolders(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_FOLDERS)
    for (const folderName of this.rawData.folderDisplayNames) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, 'CalendarFolder')
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISPLAY_NAME)
        .writeElementValue(folderName)
        .writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
  }

  public writeBodyToXml(soapXmlWriter: SoapXmlWriter) {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY)
    soapXmlWriter.writeElementWithAttribute('',
      XmlElement.NAME_CREATE_FOLDER,
      new Map([[XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE]]),
      false
    )
    soapXmlWriter.writeStartElementWithoutNamespace(XmlElement.NAME_PARENT_FOLDER_ID)
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID)
      .writeAttribute(XmlAttribute.NAME_ID, FolderName[FolderName.calendar])
      .writeEndElement();
    soapXmlWriter.writeEndElement();
    this.writeFolders(soapXmlWriter)
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}