/* Copyright © 2024 Coremail论客.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SoapXmlWriter } from '../../xml/soap_xml';
import { FolderName, XmlAttribute } from '../../xml/xml_attribute';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';
import { Filters, FindItems } from '../../model/search/mail_search_findItems';
import {
  ContainmentMode,
  ComparisonMode,
  ExcludesBitmask,
  ContainsSubstring,
  Exists,
  IsEqualTo,
  IsNotEqualTo,
  IsGreaterTan,
  IsGreaterTanOrEqual,
  IsLessThan,
  IsLessThanOrEqual,
  Not,
  Composite,
} from '../../model/search/mail_search_filter';
import { Folder } from '../../model/search/mail_search_folder';
import { BasePoint, BaseShape, ItemTraversal, PropertyType, SortOrder } from '../../model/search/mail_search_view';

export class SearchMailRequest extends RequestBase<FindItems<Filters>> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter) {
    this.writeCommonHeader(soapXmlWriter);
  }

  // body
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    const resView = this.rawData.view; // 获取页面参数
    const resFolder = this.rawData.folder; // 获取要搜索的文件夹
    const resQueryString = this.rawData.queryString; // 获取查询字符串
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_FIND_ITEM)
      .writeAttribute(XmlAttribute.NAME_TRAVERSAL,ItemTraversal[resView.itemTraversal]);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    const baseShape = resView.baseShape;
    if (baseShape) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE, BaseShape[resView.baseShape]);
    }
    const fieldURI = resView.fieldURIArr;
    if (fieldURI) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ADDITIONAL_PROPERTIES);
      fieldURI.forEach(item => {
        if (typeof item === 'string') {
          soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
            .writeAttribute(XmlAttribute.NAME_FIELD_URI, item)
            .writeEndElement();
        } else {
          soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
            .writeAttribute(XmlAttribute.NAME_PROPERTY_TAG, item.PropertyTag)
            .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, item.propertySetId)
            .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, item.propertyName)
            .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[item.propertyType])
            .writeEndElement();
        }
      })
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();

    }
    //设置分页属性
    const itemView = resView.itemView
    if (itemView && itemView.maxEntriesReturned) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_INDEXED_PAGE_ITEM_VIEW)
        .writeAttribute(XmlAttribute.NAME_MAX_ENTRIES_RETURNED, itemView.maxEntriesReturned)
        .writeAttribute(XmlAttribute.NAME_OFFSET, itemView.offset)
        .writeAttribute(XmlAttribute.NAME_BASE_POINT, BasePoint[itemView.basePoint])
        .writeEndElement();
    }else{
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_INDEXED_PAGE_ITEM_VIEW)
        .writeAttribute(XmlAttribute.NAME_OFFSET, itemView.offset)
        .writeAttribute(XmlAttribute.NAME_BASE_POINT, BasePoint[itemView.basePoint])
        .writeEndElement();
    }
    // 设置搜索筛选器
    const resFilter = this.rawData.filter;
    if (resFilter) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_RESTRICTION);
      this.setSearchFilters(soapXmlWriter, resFilter);
      soapXmlWriter.writeEndElement();
    }


    //设置排序方式
    const orderBy = resView.orderBy;
    if (orderBy) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SORT_ORDER);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_ORDER)
        .writeAttribute(XmlAttribute.NAME_ORDER,SortOrder[orderBy.orderBy]);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, orderBy.FieldURI)
        .writeEndElement();
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    // 设置要搜索的文件夹
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_PARENT_FOLDER_IDS);
    if (resFolder instanceof Folder) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, resFolder.id)
        .writeAttribute(XmlAttribute.NAME_CHANGE_KEY, resFolder.changeKey);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID)
        .writeAttribute(XmlAttribute.NAME_ID, FolderName[resFolder])
        .writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    // 设置要搜索的查询字符串
    if (resQueryString) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_QUERY_STRING, resQueryString);
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  protected setSearchFilters(soapXmlWriter: SoapXmlWriter, resFilter: Filters): void {
    // 设置搜索筛选器
    if (resFilter instanceof ContainsSubstring) {
      this.ContainsSubstringFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof ExcludesBitmask) {
      this.ExcludesBitmaskFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof Exists) {
      this.ExistsFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof IsEqualTo || resFilter instanceof IsNotEqualTo) {
      this.IsEqualToOrIsNotEqualToFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof IsGreaterTan
      || resFilter instanceof IsGreaterTanOrEqual
      || resFilter instanceof IsLessThan
      || resFilter instanceof IsLessThanOrEqual) {
      this.RelationshipFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof Not) {
      this.NotFilter(soapXmlWriter, resFilter);
    } else if (resFilter instanceof Composite) {
      this.CompositeFilter(soapXmlWriter, resFilter);
    }
  }

  /**
   * 包含搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 包含搜索筛选器
   */
  protected ContainsSubstringFilter(soapXmlWriter: SoapXmlWriter, resFilter: ContainsSubstring): void {
    if (resFilter.containmentMode || resFilter.comparisonMode) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTAINS)
        .writeAttribute(XmlAttribute.NAME_CONTAINMENT_MODE, ContainmentMode[resFilter.containmentMode])
        .writeAttribute(XmlAttribute.NAME_CONTAINMENT_COMPARISON, ComparisonMode[resFilter.comparisonMode]);
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTAINS);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
      .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      .writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value)
      .writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 位掩码搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 位掩码搜索筛选器
   */
  protected ExcludesBitmaskFilter(soapXmlWriter: SoapXmlWriter, resFilter: ExcludesBitmask): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXCLUDES);
    if(typeof resFilter.fieldURIString === 'string'){
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement();
    }else{
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TAG, resFilter.propertySet.PropertyTag)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, resFilter.propertySet.propertySetId)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, resFilter.propertySet.propertyName)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[resFilter.propertySet.propertyType])
        .writeEndElement();
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BITMASK)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.bitmask)
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 存在搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 存在搜索筛选器
   */
  protected ExistsFilter(soapXmlWriter: SoapXmlWriter, resFilter: Exists): void {
    const fieldURIString = resFilter.fieldURIString;
    if(typeof fieldURIString === 'string'){
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement()
    }else{
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXISTS);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, resFilter.propertySet.propertySetId)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, resFilter.propertySet.propertyName)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[resFilter.propertySet.propertyType])
        .writeEndElement();
      soapXmlWriter.writeEndElement();
    }

  }
  /**
   * 相等和不相等搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 相等和不相等搜索筛选器
   */
  protected IsEqualToOrIsNotEqualToFilter(soapXmlWriter: SoapXmlWriter, resFilter: IsEqualTo | IsNotEqualTo): void {
    if (resFilter instanceof IsEqualTo) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_EQUAL_TO);
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_NOT_EQUAL_TO);
    }

    const fieldURIString = resFilter.fieldURIString
    if (typeof fieldURIString === 'string') {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.fieldURIString)
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
        .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    } else {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, resFilter.propertySet.propertySetId)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, resFilter.propertySet.propertyName)
        .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[resFilter.propertySet.propertyType])
        .writeEndElement();
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I)
        .writeAttribute(XmlAttribute.NAME_FIELD_URI, resFilter.value)
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 关系测试搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 关系测试搜索筛选器
   */
  protected RelationshipFilter(soapXmlWriter: SoapXmlWriter,
                               resFilter:IsGreaterTan | IsGreaterTanOrEqual | IsLessThan | IsLessThanOrEqual): void {
    if (resFilter instanceof IsGreaterTan) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_GREATER_THAN);
    } else if (resFilter instanceof IsGreaterTanOrEqual) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_GREATER_THAN_OR_EQUAL_TO);
    } else if (resFilter instanceof IsLessThan) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_LESS_THAN);
    } else if (resFilter instanceof IsLessThanOrEqual) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_IS_LESS_THAN_OR_EQUAL_TO);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL)
      .writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, resFilter.propertySet.propertySetId)
      .writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, resFilter.propertySet.propertyName)
      .writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[resFilter.propertySet.propertyType])
      .writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FIELD_URI_OR_CONSTANT);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONSTANT)
      .writeAttribute(XmlAttribute.NAME_VALUE, resFilter.value);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 否等搜索筛选器
   */
  protected NotFilter(soapXmlWriter: SoapXmlWriter, resFilter: Not):void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_NOT);
    this.setSearchFilters(soapXmlWriter, resFilter.searchFilter);
    soapXmlWriter.writeEndElement();
  }

  /**
   * 复合搜索筛选器
   * @param {soapXmlWriter} xml写入器
   * @param {resFilter} 复合搜索筛选器
   */
  protected CompositeFilter(soapXmlWriter: SoapXmlWriter, resFilter: Composite):void  {
    const logicalOperator = resFilter.logicalOperator;
    if (!logicalOperator) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_AND);
      resFilter.searchFilters.forEach((item) => {
        this.setSearchFilters(soapXmlWriter, item);
      })
      soapXmlWriter.writeEndElement();
    }else{
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE,XmlElement.NAME_OR);
      resFilter.searchFilters.forEach((item)=>{
        this.setSearchFilters(soapXmlWriter,item);
      })
      soapXmlWriter.writeEndElement();
    }
  }
}