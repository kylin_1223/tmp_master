/* Copyright © 2024 Coremail论客. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderItems } from '../../model/sync/folder_items';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { XmlElement } from '../../xml/xml_element';
import { XmlNamespace } from '../../xml/xml_namespace';
import { RequestBase } from '../request_base';

/**
 * 获取指定文件夹下需要同步的项目
 *
 * @since 2024-03-24
 */
export class SyncFolderItemsRequest extends RequestBase<FolderItems> {
  /**
   * 将协议头写入
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 根据参数构建ews协议报文体
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    let data = this.rawData;
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_FOLDER_ITEMS);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ITEM_SHAPE);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_BASE_SHAPE);
    soapXmlWriter.writeElementValue(data.propertySet);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    if (data.syncFolderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_FOLDER_ID);
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID);
      soapXmlWriter.writeAttribute(XmlElement.NAME_ID, data.syncFolderId.id);
      soapXmlWriter.writeAttribute(XmlElement.NAME_CHANGE_KEY, data.syncFolderId.changeKey);
      soapXmlWriter.writeEndElement();
      soapXmlWriter.writeEndElement();
    }
    if (data.syncState) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_STATE, data.syncState);
    }
    if (data.ignoredItemIds && data.ignoredItemIds.length > 0) {
      let ignoredItemIds = data.ignoredItemIds;
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_IGNORE);
      ignoredItemIds.forEach(ignoredItemId => {
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ID_ITEM);
        soapXmlWriter.writeAttribute(XmlElement.NAME_ID, ignoredItemId.id);
        soapXmlWriter.writeAttribute(XmlElement.NAME_CHANGE_KEY, ignoredItemId.changeKey);
        soapXmlWriter.writeEndElement();
      })
      soapXmlWriter.writeEndElement();
    }
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_MAX_CHANGES_RETURNED);
    soapXmlWriter.writeElementValue(data.maxChangesReturned.toString());
    soapXmlWriter.writeEndElement();
    if (data.syncScope) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.MESSAGE, XmlElement.NAME_SYNC_SCOPE, data.syncScope)
    }
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}