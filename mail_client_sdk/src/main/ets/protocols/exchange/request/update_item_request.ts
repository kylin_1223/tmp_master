/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { FolderId } from '../model/folder/folder';
import { ChangeItem, ConflictResolution, ItemField, ItemOperateType, UpdateItem } from '../model/update_item';
import { MessageDisposition, SendMeetingInvitations } from '../utils/common_enum';
import { XmlAttribute } from '../xml';
import { SoapXmlWriter } from '../xml/soap_xml';
import { XmlElement } from '../xml/xml_element';
import { XmlNamespace } from '../xml/xml_namespace';
import { RequestBase } from './request_base';

/**
 * 同步方法，xml请求报文
 *
 * @since 2024-03-28
 */
export class UpdateItemRequest extends RequestBase<UpdateItem> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.writeParameters(soapXmlWriter);

    // 写入savedItemFolderId标签
    if (this.rawData.savedItemFolderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SAVED_ITEM_FOLDER_ID);
      let savedItemFolderId: FolderId = this.rawData.savedItemFolderId;
      savedItemFolderId.writeToXml(soapXmlWriter);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_CHANGE_ITEMS);
    const itemChanges: ChangeItem[] = [...this.rawData.itemChanges];
    itemChanges.forEach((itemChange: ChangeItem) => {
      // 写入ItemChange标签
      this.writeUpdateElement(soapXmlWriter, itemChange);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 根据传入值写入UpdateItems属性
   *
   * @param soapXmlWriter xml写入器
   */
  private writeParameters(soapXmlWriter: SoapXmlWriter): void {
    const updateAttributeMap: Map<string, string> = new Map();
    if (this.rawData.conflictResolution) {
      updateAttributeMap.set(XmlAttribute.NAME_CONFLICT_RESOLUTION,
        ConflictResolution[this.rawData.conflictResolution]);
    }
    if (this.rawData.messageDisposition) {
      updateAttributeMap.set(XmlAttribute.NAME_MESSAGE_DISPOSITION,
        MessageDisposition[this.rawData.messageDisposition]);
    }
    if (this.rawData.sendMeetingInvitationsOrCancellations) {
      updateAttributeMap.set(XmlAttribute.NAME_SEND_MEETING_INVITATIONS_OR_CANCELLATIONS,
        SendMeetingInvitations[this.rawData.sendMeetingInvitationsOrCancellations]);
    }
    if (this.rawData.suppressReadReceipts) {
      updateAttributeMap.set(XmlAttribute.NAME_SUPPRESS_READ_RECEIPTS, this.rawData.suppressReadReceipts);
    }
    updateAttributeMap.set(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE);
    soapXmlWriter.writeElementWithAttribute(XmlNamespace.MESSAGE, XmlElement.NAME_UPDATE_ITEM,
      updateAttributeMap, false);
  }

  /**
   * 根据传入值写入ItemChange标签
   *
   * @param soapXmlWriter xml写入器
   * @param itemChange 单个更新Item的信息
   */
  private writeUpdateElement(soapXmlWriter, itemChange): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CHANGE_ITEM);

    // ItemID、OccurrenceItemId和RecurringMasterItemId有且只有一个存在
    if (itemChange.itemId) {
      const itemArr: Map<string, string> = new Map();
      itemArr.set(XmlElement.NAME_ID, itemChange.itemId.id);
      itemArr.set(XmlElement.NAME_CHANGE_KEY, itemChange.itemId.changeKey);
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ID_ITEM, itemArr);
    }
    if (itemChange.occurrenceItemId) {
      const itemArr: Map<string, string> = new Map();
      itemArr.set(XmlAttribute.NAME_ID, itemChange.occurrenceItemId?.recurringMasterId ?? '');
      itemArr.set(XmlAttribute.NAME_CHANGE_KEY, itemChange.occurrenceItemId?.changeKey ?? '');
      itemArr.set(XmlAttribute.NAME_INSTANCE_INDEX, itemChange.occurrenceItemId?.instanceIndex ?? '');

      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_OCCURRENCE_ITEM_ID, itemArr);
    }
    if (itemChange.recurringMasterItemId) {
      const itemArr: Map<string, string> = new Map();
      itemArr.set(XmlElement.NAME_ID, itemChange.recurringMasterItemId?.occurrenceId ?? '');
      itemArr.set(XmlElement.NAME_CHANGE_KEY, itemChange.recurringMasterItemId?.changeKey ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_RECURRING_MASTER_ITEM_ID, itemArr);
    }
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_UPDATES);
    const itemFields: ItemField[] = [...itemChange.itemFields];
    itemFields.forEach((itemField: ItemField) => {
      this.writeItemField(soapXmlWriter, itemField);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }

  /**
   * 根据传入值写入对应的ItemField
   *
   * @param soapXmlWriter xml写入器
   * @param itemField 新增、删除或者修改其中的一个类型及其相关信息
   */
  private writeItemField(soapXmlWriter, itemField): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, ItemOperateType[itemField.operateType]);
    if (itemField.fieldURI) {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_FIELD_UR_I,
        new Map([[XmlElement.NAME_FIELD_UR_I, itemField.fieldURI]]));
    }
    if (itemField.indexedFieldURI) {
      let indexedFieldURIMap: Map<string, string> = new Map();
      indexedFieldURIMap.set(XmlAttribute.NAME_FIELD_URI, itemField.indexedFieldURI?.fieldURI ?? '');
      indexedFieldURIMap.set(XmlAttribute.NAME_FIELD_INDEX, itemField.indexedFieldURI?.fieldIndex ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_INDEXED_FIELD_URL,
        indexedFieldURIMap);
    }
    if (itemField.extendedFieldURI) {
      let extendedFieldURIMap: Map<string, string> = new Map();
      extendedFieldURIMap.set(XmlAttribute.NAME_DISTINGUISHED_PROPERTY_SET_ID,
        itemField.extendedFieldURI?.distinguishedPropertySetId ?? '');
      extendedFieldURIMap.set(XmlAttribute.NAME_PROPERTY_SET_ID, itemField.extendedFieldURI?.propertySetId ?? '');
      extendedFieldURIMap.set(XmlAttribute.NAME_PROPERTY_TAG, itemField.extendedFieldURI?.propertyTag ?? '');
      extendedFieldURIMap.set(XmlAttribute.NAME_PROPERTY_NAME, itemField.extendedFieldURI?.propertyName ?? '');
      extendedFieldURIMap.set(XmlAttribute.NAME_PROPERTY_ID, itemField.extendedFieldURI?.propertyId ?? '');
      extendedFieldURIMap.set(XmlAttribute.NAME_PROPERTY_TYPE, itemField.extendedFieldURI?.propertyType ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE,
        XmlElement.NAME_EXTENDED_FIELD_URL, extendedFieldURIMap);
    }
    if (itemField.operateType !== ItemOperateType.DeleteItemField) {
      itemField.item?.writeToXml(soapXmlWriter);
    }
    soapXmlWriter.writeEndElement();
  }
}
