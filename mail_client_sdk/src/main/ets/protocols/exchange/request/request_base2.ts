import { ModelBase } from '../model/mode_base';
import { ExchangeVersion } from '../utils/common_enum';
import { XmlTag, SoapXmlTag, createSoapEnvelope, createSoapHeader,
  HeaderOption,
  SoapEnvelope,
} from '../xml/xmlElement';


export class RequestBase2<T extends ModelBase | ModelBase[]> {

  constructor(protected rawData: T) {
  }

  private rootXml: SoapEnvelope;

  public writeToXml(): void {
    this.rootXml = createSoapEnvelope()
      .setNamespaceAttrXsi()
      .setNamespaceAttrXsd()
      .setNamespaceAttrType()
      .setNamespaceAttrMessage()
    const headerXml = this.writeHeaderToXml();
    const bodyXml = this.writeBodyToXml();

    let rootChildren = [];
    if (headerXml) {
      rootChildren.push(headerXml);
    };
    if (bodyXml) {
      rootChildren.push(bodyXml);
    }
    this.rootXml.appendChildren(rootChildren);
  }

  protected writeHeaderToXml(): XmlTag | null {
    return null;
  }

  protected writeBodyToXml(): XmlTag | null {
    return null;
  }

  public xmlToString(): string {
    return this.rootXml.toString();
  }

  private _headerOption: Partial<HeaderOption> = {
    requestedServerVersion: ExchangeVersion.Exchange2007_SP1
  };

  public set xmlHeaderOption(option: Partial<HeaderOption>) {
    this._headerOption = Object.assign(this._headerOption, option);
  }

  protected writeCommonHeader() {
    return createSoapHeader(this._headerOption)
  }
}