/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { StreamingSubscription, EventTypes } from '../model/streaming_subscription';
import { SoapXmlWriter } from '../xml/soap_xml';
import { FolderName, XmlAttribute } from '../xml/xml_attribute';
import { XmlElement } from '../xml/xml_element';
import { XmlNamespace } from '../xml/xml_namespace';
import { RequestBase } from './request_base';

/**
 * 订阅请求协议
 *
 * @since 2024-03-19
 */
export class StreamingSubscriptionRequest extends RequestBase<StreamingSubscription> {
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_SUBSCRIBE);
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_STREAMING_SUBSCRIPTION_REQUEST);
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_IDS);
    this.rawData.folderIds.forEach(folderId => {
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID,
        new Map([[XmlAttribute.NAME_ID, FolderName[folderId].toLowerCase()]]));
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EVENT_TYPES);
    let types: EventTypes[] = this.rawData.eventTypes;
    types.forEach(item => {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_EVENT_TYPE, EventTypes[item]);
    });
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}