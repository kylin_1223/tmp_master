/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MailBoxRouteType, OofExternalAudience, OofSettings, OofState } from '../../model/mail/oof_settings';
import { SoapXmlWriter, XmlElement, XmlNamespace } from '../../xml/index';
import { RequestBase } from '../request_base';

/**
 * 获取自动回复请求
 *
 * @since 2024-03-16
 */
export class GetOofRequest extends RequestBase<OofSettings> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    // 没有Header
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement('', XmlElement.NAME_GET_USER_OOF_SETTINGS_REQUEST)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE);

    soapXmlWriter.writeStartElement('', XmlElement.NAME_MAILBOX)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_TYPE);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_ADDRESS, this.rawData.address);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_ROUTING_TYPE,
      this.rawData.routeType || MailBoxRouteType.SMTP);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_NAME, this.rawData.name);
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 设置自动回复请求
 *
 * @since 2024-03-16
 */
export class SetOofRequest extends RequestBase<OofSettings> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    // 没有Header
  }

  /**
   * 写入xml的Envelope的属性
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeEnvelopeAttr(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.XSI, XmlNamespace.SCHEMA_XSI)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.XSD, XmlNamespace.SCHEMA_XSD)
      .writeAttrWithNamespace(XmlNamespace.XMLNS, XmlNamespace.SOAP, XmlNamespace.SCHEMA_SOAP);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    const oofData: OofSettings = this.rawData;
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    soapXmlWriter.writeStartElement('', XmlElement.NAME_SET_USER_OOF_SETTINGS_REQUEST)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_MESSAGE);

    // 写 mailbox
    soapXmlWriter.writeStartElement('', XmlElement.NAME_MAILBOX)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_TYPE)
      .writeElementWithValue('', XmlElement.NAME_ADDRESS, oofData.address)
      .writeElementWithValue('', XmlElement.NAME_ROUTING_TYPE, oofData.routeType || MailBoxRouteType.SMTP);
    if (oofData.name) {
      soapXmlWriter.writeElementWithValue('', XmlElement.NAME_NAME, oofData.name);
    }
    soapXmlWriter.writeEndElement();

    // 写 UserOofSettings
    soapXmlWriter.writeStartElement('', XmlElement.NAME_USER_OOF_SETTINGS)
      .writeAttribute(XmlNamespace.XMLNS, XmlNamespace.SCHEMA_TYPE);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_OOF_STATE, OofState[oofData.state]);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_EXTERNAL_AUDIENCE, OofExternalAudience[oofData.externalAudience]);
    soapXmlWriter.writeStartElement('', XmlElement.NAME_DURATION);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_START_TIME, oofData.startTime?.toISOString() ?? '');
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_END_TIME, oofData.endTime?.toISOString() ?? '');
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement('', XmlElement.NAME_INTERNAL_REPLY);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_MESSAGE, oofData.internalReply);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeStartElement('', XmlElement.NAME_EXTERNAL_REPLY);
    soapXmlWriter.writeElementWithValue('', XmlElement.NAME_MESSAGE, oofData.externalReply);
    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();

    soapXmlWriter.writeEndElement();
    soapXmlWriter.writeEndElement();
  }
}