/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CreateMessage } from '../../model/mail/create_message';
import { SendItem } from '../../model/mail/send_item';
import { XmlElement, XmlNamespace, SoapXmlWriter } from '../../xml';
import { CreateItemRequest } from '../create_item_request';
import { RequestBase } from '../request_base';

/**
 * 发送邮件请求
 *
 * @since 2024-03-16
 */
export class SendMailRequest extends CreateItemRequest<CreateMessage> {
  /**
   * 写入xml中Items的子节点
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeItemsChildren(soapXmlWriter: SoapXmlWriter): void {
    this.rawData?.getMessage()?.writeToXml(soapXmlWriter);
  }
}

/**
 * 发送草稿箱的电子邮件请求
 *
 * @since 2024-03-16
 */
export class SendItemRequest extends RequestBase<SendItem> {
  /**
   * 写入xml的Header
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeHeaderToXml(soapXmlWriter: SoapXmlWriter): void {
    this.writeCommonHeader(soapXmlWriter);
  }

  /**
   * 写入xml的Body
   *
   * @param soapXmlWriter xml写入器
   */
  protected writeBodyToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.SOAP, XmlElement.NAME_BODY);
    this.rawData?.writeToXml(soapXmlWriter);
    soapXmlWriter.writeEndElement();
  }
}