/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CMError, ErrorCode } from '../../../api';

/**
 * Exchange异常类型。
 */
export enum ErrorType {
  /**
   * EWS错误
   */
  EWS_ERROR = 0,
  /**
   * HTTP请求错误
   */
  HTTP_ERROR = 1,
  /**
   * HTTP模块通用错误
   */
  SYS_COMMON_ERROR = 2,
  /**
   * 客户端错误
   */
  CLIENT_ERROR = 3,
}

/**
 * Exchange异常。
 *
 * @param T data数据类型
 */
export class ExchangeError<T = void> extends Error {
  /**
   * 构造器
   *
   * @param type { ErrorType } 异常类型
   * @param message 错误信息
   * @param code 错误码
   * @param data 数据
   */
  public constructor(public type: ErrorType, message: string, public code: number | string, public data?: T) {
    super(message);
  }
}

/**
 * SDK client异常错误码
 */
export enum ClientErrorCode {
  XML_DECODED_ERROR,
  XML_ENCODED_ERROR,
  USERNAME_OR_PASSWORD_IS_NULL,
  REQUEST_VALIDATE_FAILED,
  AUTO_DISCOVER_ERROR,
}
