/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { getLogger, Logger } from '../../../utils/log';
import { GetStreamingEventsResp } from '../model/streaming_subscription';
import { JsObjectElement } from '../xml';
import { ResponseBase } from './response_base';

const logger: Logger = getLogger('StreamingEventsResponse');

/**
 * 流式处理通知响应器
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsResponse extends ResponseBase<GetStreamingEventsResp> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    let responseMessage: JsObjectElement[] = this.ParseXml2Json(bodyElement);
    logger.info('GetStreamingEventsResp', responseMessage);
    const code: JsObjectElement | undefined = responseMessage.find(s => s._name === 'ResponseCode');
    this.respCode = code?._elements[0]._text;
    if (!this.isSuccessful()) {
      return;
    } else {
      const connectionInfo: JsObjectElement | undefined = responseMessage.find(c => c._name === 'ConnectionStatus');
      if (!connectionInfo) {
        this.data.responseType = 'eventResponse';
        let notificationsElement: JsObjectElement | undefined = responseMessage.find(c => c._name === 'Notifications');
        this.data.readFromXml(notificationsElement?._elements);
      } else {
        this.data.connectionStatus = connectionInfo._elements[0]._text;
        this.data.responseType = 'connectionResponse';
      }
    }
  }

  /**
   * 解析Xml字符串为json
   *
   * @param bodyElement xml字符串
   * @returns responseMessage 解析后的json
   */
  private ParseXml2Json(bodyElement: JsObjectElement): JsObjectElement[] {
    let elementsName: string | undefined = bodyElement._name;
    let jsonElement: JsObjectElement = bodyElement._elements[0];
    let responseMessage: JsObjectElement[] = [];

    while (elementsName !== 'GetStreamingEventsResponseMessage') {
      jsonElement = jsonElement._elements[0];
      elementsName = jsonElement._elements[0]._name;
    }
    responseMessage = jsonElement._elements[0]._elements;
    return responseMessage;
  }
}