/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CreateItemResult } from '../model/calendar/create_appointment_message';
import { findElements } from '../utils/parse_jsobj_for_text';
import { XmlAttribute, XmlElement } from '../xml';
import type { JsObjectElement } from '../xml/soap_xml'
import { parseErrorMessage } from './calendar/create_appointment_response';
import { ResponseBase, ResponseBatchModelBase, ResponseFailedModelBase, ResponseSuccessModelBase, type ResponseModelBase } from './response_base';

/*
 * Parse the public base class of the result returned by create item
 * */
class CreateItemResponseBase<
T extends ResponseModelBase<CreateItemResult> |
ResponseBatchModelBase<CreateItemResult>
> extends ResponseBase<T> {

  responseMessages: JsObjectElement | undefined;

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    this.responseMessages = findElements(bodyElement,
      XmlElement.NAME_RESPONSE_MESSAGES
    )[0];
    if (!this.responseMessages) {
      this.handleXmlDecodedError('xml element ResponseMessages not found!');
    };
  }

  public readResponseMessage(responseMessage: JsObjectElement): ResponseModelBase<CreateItemResult> {
    const flag = responseMessage._attributes['ResponseClass'];
    let result: ResponseModelBase<CreateItemResult>;
    if (flag === 'Error') {
      result = new ResponseFailedModelBase();
      result.errorResult = parseErrorMessage(responseMessage);
    } else if (flag === 'Success') {
      result = new ResponseSuccessModelBase<CreateItemResult>();
      const itemId = findElements(responseMessage, XmlElement.NAME_ITEM_ID)[0];
      if (!itemId) {
        this.handleXmlDecodedError('xml element ItemId not found!');
      };
      const successRes = new CreateItemResult();
      successRes.id = itemId._attributes[XmlAttribute.NAME_ID];
      successRes.changeKey = itemId._attributes[XmlAttribute.NAME_CHANGE_KEY];
      result.successResult = successRes;
    }
    return result;
  }
}


export class CreateItemResponse extends CreateItemResponseBase<ResponseModelBase<CreateItemResult>> {

  public readBodyFromXml(bodyElement: JsObjectElement): void {
    super.readBodyFromXml(bodyElement);
    const responseMessage = this.responseMessages._elements[0];
    this.data = this.readResponseMessage(responseMessage);
  }
}


export class CreateItemsResponse extends CreateItemResponseBase<ResponseBatchModelBase<CreateItemResult>> {

  public readBodyFromXml(bodyElement: JsObjectElement): void {
    super.readBodyFromXml(bodyElement);
    const result = new ResponseBatchModelBase<CreateItemResult>();
    this.responseMessages._elements.forEach((element) => {
      const res = this.readResponseMessage(element);
      if (res.status === 'success') {
        result.successList.push(res.successResult);
      } else {
        result.errorList.push(res.errorResult);
      }
    });
    this.data = result;
  }
}

