/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ClientErrorCode, ErrorType, ExchangeError } from '../error/exchange_error';
import { ModelBase } from '../model/mode_base';
import { SoapXmlReader, JsObjectElement } from '../xml/soap_xml';
import { XmlElement } from '../xml/xml_element';
import { XmlNamespace } from '../xml/xml_namespace';

/**
 * 服务端请求响应基类，业务请求服务端响应应该继承此类
 *
 * @since 2024-03-19
 */
export abstract class ResponseBase<T extends ModelBase> {
  protected respCode: string;
  protected data: T | undefined;

  /**
   * 构造器
   *
   * @param respData 模型数据
   */
  public constructor(respData?: T) {
    this.data = respData;
  }

  /**
   * 获取响应数据，此数据类型为一个model类
   *
   * @returns 响应数据
   */
  public getData(): T {
    return this.data;
  }

  /**
   * 服务端是否正常返回，服务端返回码为NoError时，表示服务端正常处理业务。
   *
   * @returns true：服务端正常
   */
  public isSuccessful(): boolean {
    return this.respCode === 'NoError';
  }

  /**
   * 获取服务端返回码，对应xml报文<m:ResponseCode>字段
   *
   * @returns 服务端返回码
   */
  public getRespCode(): string {
    return this.respCode;
  }

  /**
   * 将xml协议数据转换成响应对象，并封装响应数据为指定Model类。
   *
   * @param soapXmlReader xml读取器
   * @param needError 出错时是否中断
   * @returns 响应结果
   */
  public readFromXml(soapXmlReader: SoapXmlReader, needError = true): ResponseBase<T> {
    const envelope = soapXmlReader.readEnvelopeElement();
    if (!envelope || !envelope._elements?.length) {
      if (needError) {
        this.handleXmlDecodedError('decoded xml envelope error.');
      } else {
        return this;
      }
    }
    let headerElement: JsObjectElement = soapXmlReader.readElement(XmlElement.NAME_HEADER, envelope._elements);
    let bodyElement: JsObjectElement = soapXmlReader.readElement(XmlElement.NAME_BODY, envelope._elements);
    this.readHeaderFromXml(headerElement);
    this.readBodyFromXml(bodyElement);
    return this;
  };

  /**
   * 从xml文件读取消息头
   *
   * @param headerElement xml转换后对象的
   */
  protected readHeaderFromXml(headerElement: JsObjectElement): void {
    if (!headerElement) {
      this.handleXmlDecodedError('decoded xml header error.');
    }
  }

  /**
   * 抛出一个xml解析异常
   *
   * @param msg 异常信息
   */
  protected handleXmlDecodedError(msg: string): void {
    throw new ExchangeError(ErrorType.CLIENT_ERROR, msg, ClientErrorCode[ClientErrorCode.XML_DECODED_ERROR]);
  }

  /**
   * 从xml解析出body信息
   *
   * @param bodyElement body元素对象
   */
  protected abstract readBodyFromXml(bodyElement: JsObjectElement): void;
}

// 返回类型格式优化
export interface ErrorMsgInfo {
  errorMessage: string;
  errorCode: string;
}

export class ResponseSuccessModelBase<S> extends ModelBase {
  readonly status: 'success' = 'success';
  successResult: S;
}

export class ResponseFailedModelBase extends ModelBase {
  readonly status: 'failed' = 'failed';
  errorResult: ErrorMsgInfo;
}

// 批量操作返回成功和失败的数组
export class ResponseBatchModelBase<S> extends ModelBase {
  successList: S[] = [];
  errorList: ErrorMsgInfo[] = [];
}

/**
 * 返回ResponseMessage封装
 */
export class ResponseMessage<T> extends ModelBase {
  private _responseCode: string; // 错误代码，用于标识请求遇到的特定错误。
  private _responseClass: string; // 响应的状态 -成功 -警告 -错误
  private _messageText: string; // 提供响应状态的文本说明
  private _descriptiveLinkKey: string; // 预留字段；当前未使用，保留供将来使用。 它包含值 0。
  private _items: T[]; // 包含返回的项的数组

  /**
   * 设置错误代码
   *
   * @param value 错误代码
   */
  public set responseCode(value: string) {
    this._responseCode = value;
  }

  /**
   * 获取错误代码
   * @returns 错误代码
   */
  public get responseCode(): string {
    return this._responseCode;
  }

  /**
   * 设置响应的状态
   *
   * @param value 响应的状态
   */
  public set responseClass(value: string) {
    this._responseClass = value;
  }

  /**
   * 获取响应的状态
   *
   * @returns 响应的状态
   */
  public get responseClass(): string {
    return this._responseClass;
  }

  /**
   * 设置提供响应状态的文本说明
   *
   * @param value 提供响应状态的文本说明
   */
  public set messageText(value: string) {
    this._messageText = value;
  }

  /**
   * 获取提供响应状态的文本说明
   *
   * @returns string 提供响应状态的文本说明
   */
  public get messageText(): string {
    return this._messageText;
  }

  /**
   * 设置预留字段
   *
   * @param value预留字段
   */
  public set descriptiveLinkKey(value: string) {
    this._descriptiveLinkKey = value;
  }

  /**
   * 获取预留字段
   * @returns string 预留字段
   */
  public get descriptiveLinkKey(): string {
    return this._descriptiveLinkKey;
  }

  /**
   * 设置包含返回的项的数组
   *
   * @param value 包含返回的项的数组
   */
  public set items(value: T[]) {
    this._items = value;
  }

  /**
   * 获取包含返回的项的数组
   *
   * @returns T[]包含返回的项的数组
   */
  public get items(): T[] {
    return this._items;
  }
}

export type ResponseModelBase<S> = ResponseSuccessModelBase<S> | ResponseFailedModelBase;