/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getLogger, Logger } from '../../../../utils/log';
import { ResponseBase } from '../response_base';
import { GetFolderResponseResp } from '../../model/folder/get_folder';
import { JsObjectElement, XmlElement } from '../../xml/index';
import { Folder, FolderResponse } from '../../model/folder/folder';
import { ContactsFolder } from '../../model/folder/contacts_folder';
import { CalendarFolder } from '../../model/folder/calendar_folder';
import { SearchFolder } from '../../model/folder/search_folder';
import { TasksFolder } from '../../model/folder/tasks_folder';

const logger: Logger = getLogger('GetFolderResponse');

/**
 * 获取文件夹响应
 *
 * @since 2024-03-19
 */
export class GetFolderResponse extends ResponseBase<GetFolderResponseResp> {
  public constructor() {
    super();
    this.data = new GetFolderResponseResp();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    // decode body
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.respCode = 'NoError';
    const responseMessages = bodyElement._elements[0]._elements[0]._elements;
    let responses: FolderResponse[] = [];
    responseMessages.forEach(item => {
      let response: FolderResponse = new FolderResponse();
      let responseMessage = item._elements;
      response.responseClass = item._attributes.ResponseClass;
      let length = responseMessage.length;
      for (let i = 0; i < length; i++) {
        let elementsName = responseMessage[i]._name;
        switch (elementsName) {
          case XmlElement.NAME_RESPONSE_CODE:
            response.responseCode = responseMessage[i]._elements[0]._text;
            break;
          case XmlElement.NAME_MESSAGE_TEXT:
            response.messageText = responseMessage[i]._elements[0]._text;
            break;
          case XmlElement.NAME_FOLDERS:
            if (responseMessage[i]._elements && response.responseClass === 'Success') {
              let folderType = responseMessage[i]._elements[0]._name;
              switch (folderType) {
                case XmlElement.NAME_FOLDER:
                  const folder = new Folder();
                  folder.readFromXml(responseMessage[i]._elements);
                  response.folder = folder;
                  logger.info('folder', JSON.stringify(folder));
                  break;
                case XmlElement.NAME_CONTACTS_FOLDER:
                  const contactsFolder = new ContactsFolder();
                  contactsFolder.readFromXml(responseMessage[i]._elements);
                  response.contactsFolder = contactsFolder;
                  logger.info('contactsFolder', JSON.stringify(contactsFolder));
                  break;
                case XmlElement.NAME_CALENDAR_FOLDER:
                  const calendarFolder = new CalendarFolder();
                  calendarFolder.readFromXml(responseMessage[i]._elements);
                  response.calendarFolder = calendarFolder;
                  logger.info('calenderFolder', JSON.stringify(calendarFolder));
                  break;
                case XmlElement.NAME_SEARCH_FOLDER:
                  const searchFolder = new SearchFolder();
                  searchFolder.readFromXml(responseMessage[i]._elements);
                  logger.info('searchFolder', JSON.stringify(searchFolder));
                  break;
                case XmlElement.NAME_TASKS_FOLDER:
                  const tasksFolder = new TasksFolder();
                  tasksFolder.readFromXml(responseMessage[i]._elements);
                  response.tasksFolder = tasksFolder;
                  logger.info('tasksFolder', JSON.stringify(tasksFolder));
                  break;
                default:
                  break;
              }
            }
            break;
          default:
            break;
        }
      }
      responses.push(response);
    })
    logger.info(responses);
    this.data.responseMessages = responses;
  }
}