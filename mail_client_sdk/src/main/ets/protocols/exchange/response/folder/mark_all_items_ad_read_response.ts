/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { parseResponseMassage } from '../../model/folder/folder';
import { MarkAllItemsAsReadResp } from '../../model/folder/mark_all_items_as_read';
import { JsObjectElement } from '../../xml/soap_xml'
import { ResponseBase } from '../response_base'

/**
 * 标记文件夹已读响应
 *
 * @since 2024-03-19
 */
export class MarkAllItemsAsReadResponse extends ResponseBase<MarkAllItemsAsReadResp> {
  public constructor() {
    super();
    this.data = new MarkAllItemsAsReadResp();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    // decode body
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.respCode = 'NoError';
    const responseMessages = bodyElement._elements[0]._elements[0]._elements;
    const responses = parseResponseMassage(responseMessages);
    this.data.responseMessages = responses;
  }
}