/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { EmptyFolderResp } from '../../model/folder/empty_folder';
import { parseResponseMassage } from '../../model/folder/folder';
import { JsObjectElement } from '../../xml/soap_xml'
import { ResponseBase } from '../response_base'

/**
 * 清空文件夹响应
 *
 * @since 2024-03-19
 */
export class EmptyFolderResponse extends ResponseBase<EmptyFolderResp> {
  public constructor() {
    super();
    this.data = new EmptyFolderResp();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    // decode body
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.respCode = 'NoError';
    const responseMessages = bodyElement._elements[0]._elements[0]._elements;
    const responses = parseResponseMassage(responseMessages);
    this.data.responseMessages = responses;
  }
}