import { DeleteItemResp, DeleteItemResponseMessage } from '../model/delete_item';
import { XmlElement } from '../xml';
import { JsObjectElement } from '../xml/soap_xml';
import { ResponseBase } from './response_base';

export class DeleteItemResponse extends ResponseBase<DeleteItemResp> {

  /**
   * 解析返回报文
   *
   * @param bodyElement
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    // decode body
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.respCode = 'NoError';
    const responseMessages = bodyElement._elements[0]._elements[0]._elements;
    let responses: DeleteItemResponseMessage[] = [];
    responseMessages.forEach(item => {
      let response: DeleteItemResponseMessage = new DeleteItemResponseMessage();
      let responseMessage = item?._elements;
      response.responseClass = item?._attributes?.ResponseClass;
      let length = responseMessage.length;
      for (let i = 0; i < length; i++) {
        let elementsName = responseMessage[i]?._name;
        let elementsValue = responseMessage[i]?._elements[0]?._text;
        switch (elementsName) {
          case XmlElement.NAME_RESPONSE_CODE:
            response.responseCode = elementsValue;
            break;
          case XmlElement.NAME_MESSAGE_TEXT:
            response.messageText = elementsValue;
            break;
          case XmlElement.NAME_DESCRIPTIVE_LINK_KEY:
            response.descriptiveLinkKey = elementsValue;
            break;
          default:
            break;
        }
      }
      responses.push(response);
    });
    this.data.responseMessages = responses;
  }
}