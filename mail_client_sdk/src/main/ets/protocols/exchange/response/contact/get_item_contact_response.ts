/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CompleteName,
  EmailAddress,
  GetContact,
  GetItemContactResponseModel,
  ImAddress,
  ImAddressesKey,
  PhoneNumber,
  PhoneNumbersKey,
  PhysicalAddressElementContent,
  PhysicalAddress
} from '../../model/contact/get_contact';
import type { EffectiveRights } from '../../model/contact/get_contact';
import { JsObjectElement } from '../../xml';
import { ResponseBase } from '../response_base';
import { lowercaseFirstLetter } from '../../utils/sync_util';

/**
 * 查找联系人返回XML解析类
 *
 * @since 2024-04-09
 */
export class GetItemContactResponse extends ResponseBase<GetItemContactResponseModel> {
  /**
   * 查询联系人返回XML报文解析函数
   *
   * @param bodyElement Xml协议转换成JS对象的数据结构 此处为返回报文主体标签节点对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }

    if (!bodyElement._elements[0]._elements[0]._elements) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,get ResponseMessage failed');
    }

    this.respCode = 'NoError';

    const responseMessage: JsObjectElement[] = bodyElement._elements[0]._elements[0]._elements;
    const getItemResponseContactArr: GetContact[] = [];

    responseMessage.forEach(responseContactElement => {
      const responseContact = responseContactElement?._elements;
      const getItemResponseContact = new GetContact();
      responseContact[1]._elements[0]._elements.forEach(contactItem => {
        if (responseContactElement._attributes.ResponseClass === 'Error') {
          getItemResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          getItemResponseContact.responseCode = responseContact[0]._elements[1]._text;
          getItemResponseContact.messageText = responseContact[0]._elements[0]._text;
        } else if(responseContactElement._attributes.ResponseClass === 'Success') {
          getItemResponseContact.responseClass = responseContactElement._attributes.ResponseClass;
          getItemResponseContact.responseCode = responseContact[0]._elements[0]._text;
          switch (contactItem._name) {
            case 'ItemId':
              getItemResponseContact.itemId = {
                id: contactItem._attributes.Id,
                changeKey: contactItem._attributes.ChangeKey
              };
              break;
            case 'ParentFolderId':
              getItemResponseContact.parentFolderId = {
                id: contactItem._attributes.Id,
                changeKey: contactItem._attributes.ChangeKey
              };
              break;
            case 'Body':
              getItemResponseContact.body = {
                bodyType: contactItem._attributes.BodyType,
                isTruncated: contactItem._attributes.IsTruncated
              };
              break;
            case 'EffectiveRights':
              let _effectiveRight: EffectiveRights = {
                createAssociated: '',
                createContents: '',
                createHierarchy: '',
                delete: '',
                modify: '',
                read: '',
                viewPrivateItems: ''
              };
              contactItem._elements.forEach(effectiveRightItem => {
                _effectiveRight[lowercaseFirstLetter(effectiveRightItem._name)] =
                  effectiveRightItem._elements[0]._text;
              });
              getItemResponseContact.effectiveRights = _effectiveRight;
              break;
            case 'ConversationId':
              getItemResponseContact.conversationId = {
                id: contactItem._attributes?.Id,
              };
              break;
            case 'Flag':
              getItemResponseContact.flag = {
                flagStatus: contactItem._elements[0]._elements[0]._text
              };
              break;
            case 'CompleteName':
              let _completeName: CompleteName = {
                title: '',
                firstName: '',
                middleName: '',
                lastname: '',
                suffix: '',
                initials: '',
                fullName: '',
                nickname: '',
                yomiFirstName: '',
                yomiLastName: ''
              };
              contactItem._elements.forEach(completeNameItem => {
                _completeName[lowercaseFirstLetter(completeNameItem._name)] =
                  completeNameItem._elements[0]._text;
              });
              getItemResponseContact.completeName = _completeName;
              break;
            case 'EmailAddress':
              let _emailAddresses: EmailAddress[] = [];
              contactItem._elements.forEach(_emailAddress => {
                _emailAddresses.push({
                  key: _emailAddress._attributes.Key,
                  name: _emailAddress._attributes.Name,
                  routingType: _emailAddress._attributes.RoutingType,
                  mailboxType: _emailAddress._attributes.MailboxType,
                  elementContent: _emailAddress._elements[0]._text
                });
              });
              getItemResponseContact.emailAddresses = _emailAddresses;
              break;
            case 'PhysicalAddress':
              let _physicalAddresses: PhysicalAddress[] = [];
              contactItem._elements.forEach(_physicalAddress => {
                let _physicalAddressElementContent: PhysicalAddressElementContent = {};
                _physicalAddress._elements.forEach(_physicalAddressEntry => {
                  _physicalAddressElementContent[lowercaseFirstLetter(_physicalAddressEntry._name)] =
                    _physicalAddressEntry._elements[0]?._text;
                });
                _physicalAddresses.push({
                  key: _physicalAddress._attributes.Key,
                  elementContent: _physicalAddressElementContent
                });
              });
              getItemResponseContact.physicalAddresses = _physicalAddresses;
              break;
            case 'PhoneNumber':
              let _phoneNumbers: PhoneNumber[] = [];
              contactItem._elements.forEach(_phoneNumber => {
                if (_phoneNumber._elements) {
                  _phoneNumbers.push({
                    key: _phoneNumber._attributes.Key as PhoneNumbersKey,
                    elementContent: _phoneNumber._elements[0]?._text
                  });
                }
              });
              getItemResponseContact.phoneNumbers = _phoneNumbers;
              break;
            case 'ImAddress':
              let _imAddresses: ImAddress[] = [];
              contactItem._elements.forEach(_imAddress => {
                if (_imAddress._elements) {
                  _imAddresses.push({
                    key: _imAddress._attributes.Key as ImAddressesKey,
                    elementContent: _imAddress._elements[0]?._text
                  })
                }
              })
              getItemResponseContact.imAddresses = _imAddresses;
              break;
            default:
              if (contactItem._elements) {
                getItemResponseContact[lowercaseFirstLetter(contactItem._name)] =
                  contactItem._elements[0]._text
              }
              break;
          }
        }
      })
      getItemResponseContactArr.push(getItemResponseContact);
    })
    this.data.contacts = getItemResponseContactArr;
  }
}