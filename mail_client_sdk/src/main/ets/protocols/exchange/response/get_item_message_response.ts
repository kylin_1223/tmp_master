/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getLogger, Logger } from '../../../utils/log';
import { Message } from '../model/mail/item_message';
import { JsObjectElement } from '../xml/soap_xml';
import { ResponseBase } from './response_base';
import { GetItemMessageResponseModel } from '../model/get_item';
import { ResponseMessage } from '../response/response_base';
import { XmlElement } from '../xml';

const logger: Logger = getLogger("GetMessageItemResponse");

/**
 * 获取item响应类
 *
 * @since 2024-04-08
 */
export class GetItemMessageResponse extends ResponseBase<GetItemMessageResponseModel> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    logger.info('bodyElement' + JSON.stringify(bodyElement));
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }

    // 获取需要解析的ResponseMessages节点数据
    const responseMessagesNode: JsObjectElement[] = bodyElement._elements?.[0]?._elements?.[0]?._elements;
    if (!responseMessagesNode) {
      this.handleXmlDecodedError('decoded xml body error,get ResponseMessage failed');
    }

    // 解析后的responseMessage数组
    const responseMessagesObject: ResponseMessage<Message>[] = responseMessagesNode.map(responseMessageElement => {
      return this.generateGetItemResponseMessage(responseMessageElement);
    });

    // 判断整体是否成功
    const hasError: number = responseMessagesObject.findIndex((responseMessage) => {
      return responseMessage.responseCode === 'Error';
    });

    // 子元素有任意一个失败，则整体失败
    this.respCode = hasError !== -1 ? 'Error' : 'NoError';
    this.data.responseMessages = responseMessagesObject;
  }

  /**
   * 根据节点数据生成ResponseMessage对象
   *
   * @param responseMessageElement 解析后的responseMessage节点数据
   * @returns ResponseMessage<Message> 生成的responseMessage对象
   */
  private generateGetItemResponseMessage(responseMessageElement: JsObjectElement): ResponseMessage<Message> {
    let getItemResponseMessage: ResponseMessage<Message> = new ResponseMessage<Message>();
    getItemResponseMessage.responseClass = responseMessageElement?._attributes?.ResponseClass;
    responseMessageElement?._elements?.forEach(resMsgElement => {
      this.processGetItemResponseMessage(resMsgElement, getItemResponseMessage);
    });
    return getItemResponseMessage;
  }

  /**
   * 处理 getItemResponseMessage 的具体属性
   *
   * @param resMsgElement 解析后的responseMessage节点数据
   * @param getItemResponseMessage 具体的responseMessage对象
   */
  private processGetItemResponseMessage(resMsgElement: JsObjectElement,
                                        getItemResponseMessage: ResponseMessage<Message>): void {
    switch (resMsgElement._name) {
      case XmlElement.NAME_RESPONSE_CODE:
        getItemResponseMessage.responseCode = resMsgElement._elements[0]?._text;
        break;
      case XmlElement.NAME_MESSAGE_TEXT:
        getItemResponseMessage.messageText = resMsgElement._elements[0]?._text;
        break;
      case XmlElement.NAME_DESCRIPTIVE_LINK_KEY:
        getItemResponseMessage.descriptiveLinkKey = resMsgElement._elements[0]?._text;
        break;
      case XmlElement.NAME_ITEMS:
        getItemResponseMessage.items = this.generateItems(resMsgElement._elements);
        break;
      default:
        break;
    }
  }

  /**
   * 解析xml，生成邮件对象数组
   *
   * @param itemsJsObjectElementArr
   * @returns Message[] 解析后的message数组
   */
  private generateItems(itemsJsObjectElementArr: JsObjectElement[]): Message[] {
    if (!itemsJsObjectElementArr || itemsJsObjectElementArr.length < 1) {
      return;
    }
    let messages: Message[] = [];
    itemsJsObjectElementArr.forEach(itemsJsObjectElement => {
      let messageItem: Message = new Message();
      messageItem.readFromXml(itemsJsObjectElement._elements);
      messages.push(messageItem);
    });
    return messages;
  }
}