/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { UniteId } from '../model/mode_base';
import { ChangeItem, UpdateItemResp, UpdateResponse } from '../model/update_item';
import { XmlElement } from '../xml';
import { JsObjectElement } from '../xml/soap_xml';
import { ResponseBase } from './response_base';

export class UpdateItemResponse extends ResponseBase<UpdateItemResp> {
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.respCode = 'Error';
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    this.respCode = 'NoError';
    let responseMessages: JsObjectElement[] = bodyElement?._elements[0]?._elements[0]?._elements;
    let responses: UpdateResponse[] = [];
    responseMessages?.forEach(item => {
      let itemResponse: UpdateResponse = new UpdateResponse();
      let responseMessage: JsObjectElement[] = item._elements;
      itemResponse.responseClass = item._attributes.ResponseClass;
      responseMessage.forEach(response => {
        this.responseMessageWriteToJson(response, itemResponse);
      });
      responses.push(itemResponse);
    });
    this.data.response = responses;
  }

  /**
   * 将responseMessage的xml响应报文封装成Json
   *
   * @param response 单个responseMessage的xml响应报文
   * @param itemResponse 响应格式类
   */
  private responseMessageWriteToJson(response: JsObjectElement, itemResponse: UpdateResponse): void {
    let elementsName = response._name;
    switch (elementsName) {
      case XmlElement.NAME_RESPONSE_CODE:
        itemResponse.code = response._elements[0]._text;
        break;
      case XmlElement.NAME_MESSAGE_TEXT:
        itemResponse.messageText = response._elements[0]._text;
        break;
      case XmlElement.NAME_ITEMS:
        let items: ChangeItem[] = [];
        let changeElements: JsObjectElement[] = response._elements;

      // Items为失败请求末尾空标签则需要排除
        if (changeElements && changeElements.length) {
          changeElements.forEach(changeElement => {
            let item: ChangeItem = new ChangeItem();
            let itemId: UniteId = {
              id: changeElement._elements[0]._attributes.Id,
              changeKey: changeElement._elements[0]._attributes.ChangeKey,
            };
            item.itemId = itemId;
            items.push(item);
          })
          itemResponse.items = items;
        }
        break;
      default:
        break;
    }
  }
}
