/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { OofState, OofExternalAudience, OofSettings } from '../../model/mail/oof_settings';
import { parseResponse } from '../../utils/parse_jsobj_for_text';
import { XmlElement, JsObjectElement } from '../../xml';
import { ResponseBase } from '../response_base';

/**
 * 获取自动回复设置响应
 *
 * @since 2024-03-16
 */
export class GetOofResponse extends ResponseBase<OofSettings> {
  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);
    this.data.state = OofState[responseMap.get(XmlElement.NAME_OOF_STATE)];
    this.data.externalAudience = OofExternalAudience[responseMap.get(XmlElement.NAME_EXTERNAL_AUDIENCE)];
    this.data.startTime = new Date(responseMap.get(XmlElement.NAME_START_TIME));
    this.data.endTime = new Date(responseMap.get(XmlElement.NAME_END_TIME));

    if (this.isSuccessful()) {
      const oofSettings: JsObjectElement = bodyElement._elements?.[0]?._elements?.[1];
      this.data.internalReply = oofSettings?._elements?.[3]?._elements?.[0]?._elements?.[0]?._text;
      this.data.externalReply = oofSettings?._elements?.[4]?._elements?.[0]?._elements?.[0]?._text;
    }
  }
}

/**
 * 设置自动回复响应
 *
 * @since 2024-03-16
 */
export class SetOofResponse extends ResponseBase<OofSettings> {
  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMap: Map<string, string> = parseResponse(bodyElement);
    this.respCode = responseMap.get(XmlElement.NAME_RESPONSE_CODE);
  }
}