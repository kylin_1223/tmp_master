/*
 * Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CreateAttachmentResp,
  DeleteAttachmentResp,
  FileAttachment,
  GetAttachmentResp,
  ItemAttachment,
  GetAttachmentResponseMessage,
  DownLoadAttachmentResp,
  CreateAttachmentResponseMessage,
  DeleteAttachmentResponseMessage
} from '../../model/mail/attachments';
import { JsObjectElement } from '../../xml/soap_xml'
import { ResponseBase } from '../response_base'
import { getLogger } from '../../../../utils/log';
import { constant } from '@kit.ConnectivityKit';

// 要加类型 名字
const logger = getLogger('AttachmentResp----')

/**
 * 创建附件响应
 *
 * @since
 */
export class CreateAttachmentResponse extends ResponseBase<CreateAttachmentResp> {
  public constructor() {
    super();
    this.data = new CreateAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }

    const responseMessage = bodyElement._elements[0]._elements[0]._elements;

    const createAttachmentResponseMessageArr: CreateAttachmentResponseMessage[] = [];
    responseMessage.forEach(attachmentResponseMessage => {
      const createAttachmentResponseMessage = new CreateAttachmentResponseMessage();
      const elements = attachmentResponseMessage._elements;
      if (attachmentResponseMessage._attributes.ResponseClass == 'Success') {
        elements.forEach(item =>{
          if (item._name === 'ResponseCode') {
            createAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }else if(item._name === 'Attachments'){
            const attachments = item._elements;
            attachments.forEach(attachment => {
              if (attachment._name === 'FileAttachment') {
                const fileAttachments = attachment._elements;
                const fileAttachment = new FileAttachment();
                fileAttachment.readFromXml(fileAttachments);
                createAttachmentResponseMessage.attachmentList = [fileAttachment];
              } else if(attachment._name === 'ItemAttachment'){
                const itemAttachments = attachment._elements;
                const itemAttachment = new ItemAttachment();
                itemAttachment.readFromXml(itemAttachments);
                createAttachmentResponseMessage.attachmentList = [itemAttachment];
              }
            })
          }
        })
      } else if(attachmentResponseMessage._attributes.ResponseClass == 'Error') {
        elements.forEach(item =>{
          if(item._name === 'MessageText') {
            createAttachmentResponseMessage.messageText = item._elements[0]._text;
          } else if(item._name === 'ResponseCode') {
            createAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }
        })
      } else {
        // ResponseClass为警告
        // attachmentResponseMessage._attributes.ResponseClass == 'Warning'
      }
      createAttachmentResponseMessageArr.push(createAttachmentResponseMessage);
    })
    this.data.responseMessages = createAttachmentResponseMessageArr;
  }
}

/**
 * 删除附件响应
 */
export class DeleteAttachmentResponse extends ResponseBase<DeleteAttachmentResp> {
  public constructor() {
    super();
    this.data = new DeleteAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }

    const responseMessage = bodyElement._elements[0]._elements[0]._elements;

    const deleteAttachmentResponseMessageArr: DeleteAttachmentResponseMessage[] = [];
    responseMessage.forEach(attachmentResponseMessage => {
      const deleteAttachmentResponseMessage = new DeleteAttachmentResponseMessage();
      const elements = attachmentResponseMessage._elements;
      if (attachmentResponseMessage._attributes.ResponseClass == 'Success') {
        elements.forEach(item =>{
          if (item._name === 'ResponseCode') {
            deleteAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }
        })
      } else if(attachmentResponseMessage._attributes.ResponseClass == 'Error') {
        elements.forEach(item =>{
          if(item._name === 'MessageText'){
            deleteAttachmentResponseMessage.messageText = item._elements[0]._text;
          } else if(item._name === 'ResponseCode'){
            deleteAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }
        })
      } else {
        // ResponseClass为警告
        // attachmentResponseMessage._attributes.ResponseClass == 'Warning'
      }
      deleteAttachmentResponseMessageArr.push(deleteAttachmentResponseMessage);
    })
    this.data.responseMessages = deleteAttachmentResponseMessageArr;
  }
}

/**
 * 获取附件响应
 */
export class GetAttachmentResponse extends ResponseBase<GetAttachmentResp> {
  public constructor() {
    super();
    this.data = new GetAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    // logger.info('bodyElement',JSON.stringify(bodyElement))
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
    const responseMessage = bodyElement._elements[0]._elements[0]._elements;
    logger.info('responseMessage',responseMessage);

    const getAttachmentResponseMessageArr: GetAttachmentResponseMessage[] = [];

    responseMessage.forEach(attachmentResponseMessage => {
      const getAttachmentResponseMessage = new GetAttachmentResponseMessage();
      const elements = attachmentResponseMessage._elements;
      if (attachmentResponseMessage._attributes.ResponseClass == 'Success') {
        elements.forEach(item =>{
          if (item._name === 'ResponseCode') {
            getAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }else if(item._name === 'Attachments'){
            const attachments = item._elements;
            attachments.forEach(attachment => {
              if (attachment._name === 'FileAttachment') {
                const fileAttachments = attachment._elements;
                const fileAttachment = new FileAttachment();
                fileAttachment.readFromXml(fileAttachments);
                getAttachmentResponseMessage.attachmentList = [fileAttachment];
              }else if(attachment._name === 'ItemAttachment'){
                const itemAttachments = attachment._elements;
                const itemAttachment = new ItemAttachment();
                itemAttachment.readFromXml(itemAttachments);
                getAttachmentResponseMessage.attachmentList = [itemAttachment];
              }
            })
          }
        })
      } else if(attachmentResponseMessage._attributes.ResponseClass == 'Error') {
        elements.forEach(item =>{
          if(item._name === 'MessageText'){
            getAttachmentResponseMessage.messageText = item._elements[0]._text;
          } else if(item._name === 'ResponseCode'){
            getAttachmentResponseMessage.responseCode = item._elements[0]._text;
          }
        })
      } else {
        // ResponseClass为警告
        // attachmentResponseMessage._attributes.ResponseClass == 'Warning'
      }
      getAttachmentResponseMessageArr.push(getAttachmentResponseMessage);
    })
    this.data.responseMessages = getAttachmentResponseMessageArr;
  }
}

/**
 * 下载附件响应
 */
export class DownLoadAttachmentResponse extends ResponseBase<DownLoadAttachmentResp> {

  public downLoadPath:string;

  public constructor() {
    super();
    this.data = new DownLoadAttachmentResp();
  }

  /**
   * 从xml->Body解析结果
   *
   * @param bodyElement xml->Body的对象
   */
  protected readBodyFromXml(bodyElement: JsObjectElement) {
    if (!bodyElement) {
      this.handleXmlDecodedError('decoded xml body error,body element is null');
    }
  }
}
