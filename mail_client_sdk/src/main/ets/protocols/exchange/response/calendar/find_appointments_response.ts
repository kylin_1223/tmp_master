/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AppointmentItem } from '../../model/calendar/find_appointments';
import { FindItemsResults } from '../../model/calendar/find_appointments';
import { JsObjectElement } from '../../xml/soap_xml'
import { ResponseBase } from '../response_base'

export class FindAppointmentsResponse extends ResponseBase<FindItemsResults> {
  public constructor() {
    super();
    this.data = new FindItemsResults();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement) {
    // decode body
    bodyElement && this.traverseJsObjectElement(bodyElement);
  }

  traverseJsObjectElement(element: JsObjectElement) {
    if (element._name == 'ResponseCode') {
      super.respCode = element._elements[0]._text;
    }

    if (element._name == 'Items') {
      let responseItems: JsObjectElement[] = element._elements;
      for (let i = 0; i < responseItems.length; i++) {
        const tempAppointment = new AppointmentItem();
        let id = responseItems[i]._elements[0]._attributes['Id'];
        let changeKey = responseItems[i]._elements[0]._attributes['ChangeKey'];
        let subject = responseItems[i]._elements[1]._elements[0]._text;
        tempAppointment.AppointmentItemId= {
          id: id,
          changeKey: changeKey,
        };
        tempAppointment.AppointmentSubject = subject;
        this.data.addItem(tempAppointment);
      }
    }

    if (element._elements) {
      for (const subElement of element._elements) {
        this.traverseJsObjectElement(subElement);
      }
    }
  }
}