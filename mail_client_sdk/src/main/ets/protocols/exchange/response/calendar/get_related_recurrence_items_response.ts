/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetRelatedRecurrenceItemsResponseMessage } from '../../model/calendar/get_related_recurrence_items_message';
import { ItemResponseMessage } from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'
import { Calendar } from '../../model/calendar/item_calendar';

export class GetRelatedRecurrenceItemsResponse extends ResponseBase<GetRelatedRecurrenceItemsResponseMessage> {

  public constructor() {
    super();
    this.data = new GetRelatedRecurrenceItemsResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    console.log('liwang---->' + JSON.stringify(bodyElement));
    this.printEle(bodyElement._elements);
  }

  printEle(elements?: JsObjectElement[], getItem?: ItemResponseMessage): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_RESPONSE_MESSAGES:
          this.data.setResponseMessages([]);
          break;
        case XmlElement.NAME_GET_ITEM_RESPONSE_MESSAGE:
          getItem = new ItemResponseMessage();
          getItem.responseClass = element._attributes.ResponseClass;
          this.data.getResponseMessages().push(getItem);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          getItem[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_CALENDAR_ITEM:
          getItem.calendar = new Calendar();
          fillObjectFields(element, getItem.calendar);
          break
      }
      this.printEle(element._elements, getItem);
    });
  }
}