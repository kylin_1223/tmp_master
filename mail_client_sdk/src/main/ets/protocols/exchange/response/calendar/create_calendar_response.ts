/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderResult } from '../../model/calendar/create_calendar_message';
import { findElements } from '../../utils/parse_jsobj_for_text';
import { JsObjectElement, XmlAttribute, XmlElement } from '../../xml';
import {
  ResponseBase,
  ResponseBatchModelBase,
  ResponseFailedModelBase,
  ResponseModelBase, ResponseSuccessModelBase } from '../response_base';
import { parseErrorMessage } from './create_appointment_response';


export class CreateCalendarsResponse extends ResponseBase<ResponseBatchModelBase<FolderResult>> {

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    const responseMessages = findElements(bodyElement,
      XmlElement.NAME_RESPONSE_MESSAGES
    )[0];
    if (!responseMessages) {
      this.handleXmlDecodedError('xml element ResponseMessage not found!');
    };
    const result = new ResponseBatchModelBase<FolderResult>();
    responseMessages._elements.forEach((element) => {
      const flag = element._attributes['ResponseClass'];
      if (flag === 'Error') {
        result.errorList.push(parseErrorMessage(element))
      } else if (flag === 'Success') {
        const itemId = findElements(element, XmlElement.NAME_ITEM_ID)[0];
        if (!itemId) {
          this.handleXmlDecodedError('xml element ItemId not found!');
        }
        result.successList.push({
          id: itemId._attributes[XmlAttribute.NAME_ID],
          changeKey: itemId._attributes[XmlAttribute.NAME_CHANGE_KEY]
        });
      }
    });
    this.data = result;
  }
}

export class CreateCalendarResponse extends ResponseBase<ResponseModelBase<FolderResult>> {

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    const createFolderResponseMsg = findElements(bodyElement,
      XmlElement.NAME_CREATE_FOLDER_PATH_RESPONSE_MESSAGE,
    )[0];
    if (!createFolderResponseMsg) {
      this.handleXmlDecodedError('xml element CreateFolderResponseMessage not found!')
    }
    const flag = createFolderResponseMsg._attributes['ResponseClass'];
    let result: ResponseModelBase<FolderResult>;
    if (flag === 'Error') {
      result = new ResponseFailedModelBase();
      result.errorResult = parseErrorMessage(createFolderResponseMsg);
    } else if (flag === 'Success') {
      result = new ResponseSuccessModelBase<FolderResult>();
      const itemId = findElements(createFolderResponseMsg, XmlElement.NAME_ITEM_ID)[0];
      if (!itemId) {
        this.handleXmlDecodedError('xml element ItemId not found!');
      };
      result.successResult = {
        id: itemId._attributes[XmlAttribute.NAME_ID],
        changeKey: itemId._attributes[XmlAttribute.NAME_CHANGE_KEY]
      }
    }
    this.data = result;
  }
}