/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  CreateItemResult,
} from '../../model/calendar/create_appointment_message';
import { findElements } from '../../utils/parse_jsobj_for_text';
import { JsObjectElement, XmlAttribute, XmlElement } from '../../xml';
import {
  ErrorMsgInfo,
  ResponseBase,
  ResponseBatchModelBase,
  ResponseFailedModelBase, ResponseModelBase, ResponseSuccessModelBase } from '../response_base';


export class CreateAppointmentResponse extends ResponseBase<ResponseModelBase<CreateItemResult>> {

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    const createItemResponseMsg = findElements(bodyElement,
      XmlElement.NAME_CREATE_ITEM_RESPONSE_MESSAGE,
    )[0];
    if (!createItemResponseMsg) {
      this.handleXmlDecodedError('xml element CreateItemResponseMessage not found!')
    }
    const flag = createItemResponseMsg._attributes['ResponseClass'];
    let result: ResponseModelBase<CreateItemResult>;
    if (flag === 'Error') {
      result = new ResponseFailedModelBase();
      result.errorResult = parseErrorMessage(createItemResponseMsg);
    } else if (flag === 'Success') {
      result = new ResponseSuccessModelBase<CreateItemResult>();
      result.successResult = parseSuccessMessage(createItemResponseMsg);
    }
    this.data = result;
  }
}

export class CreateBatchAppointmentResponse extends ResponseBase<ResponseBatchModelBase<CreateItemResult>> {

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    const msgTags = findElements(bodyElement,
      XmlElement.NAME_CREATE_ITEM_RESPONSE_MESSAGE,
    );
    const result = new ResponseBatchModelBase<CreateItemResult>();
    msgTags.forEach(element => {
      const flag = element._attributes['ResponseClass']
      if (flag === 'Error') {
        result.errorList.push(parseErrorMessage(element))
      } else if (flag === 'Success') {
        result.successList.push(parseSuccessMessage(element))
      }
    });
    this.data = result;
  }
}

// 解析成功创建报文
export function parseErrorMessage(tag: JsObjectElement): ErrorMsgInfo {
  const errorMessage = tag._elements[0]._elements[0]._text;
  const errorCode = tag._elements[1]._elements[0]._text;
  return {
    errorMessage,
    errorCode
  };
}

// 解析创建失败报文
export function parseSuccessMessage(tag: JsObjectElement): CreateItemResult {
  const result = new CreateItemResult();
  const itemIdEl = findElements(tag, XmlElement.NAME_ITEM_ID)[0]
  if (itemIdEl) {
    result.id = itemIdEl._attributes[XmlAttribute.NAME_ID];
    result.changeKey = itemIdEl._attributes[XmlAttribute.NAME_CHANGE_KEY]
  }
  return result;
}
