/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetUserAvailabilityResponseMessage } from '../../model/calendar/get_user_availability_message';
import {
  CalendarEvent,
  FreeBusyResponseMessage,
  UserAvailabilityResponseMessage,
  Time,
  Suggestion,
  SuggestionDayResult,
  SuggestionsResponseMessage,
  TimeZone,
  WorkingHours,
  WorkingPeriod
} from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'

export class GetUserAvailabilityResponse extends ResponseBase<GetUserAvailabilityResponseMessage> {
  public constructor() {
    super();
    this.data = new GetUserAvailabilityResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    console.log('liwang---->' + JSON.stringify(bodyElement));
    this.printEle(bodyElement._elements);
  }

  printEle(
    elements?: JsObjectElement[],
    item?: UserAvailabilityResponseMessage,
    freeBusyResponse?: FreeBusyResponseMessage,
    calendarEvent?: CalendarEvent,
    workingPeriod?: WorkingPeriod,
    suggestionDayResult?: SuggestionDayResult,
    suggestion?: Suggestion): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_GET_USER_AVAILABILITY_RESPONSE:
          item = new UserAvailabilityResponseMessage();
          this.data.setResponseMessages(item)
          break;
        case XmlElement.NAME_FREE_BUSY_RESPONSE_ARRAY:
          item.freeBusyResponseArray = [];
          break;
        case XmlElement.NAME_FREE_BUSY_RESPONSE:
          freeBusyResponse = new FreeBusyResponseMessage();
          freeBusyResponse.responseClass = element._elements[0]._attributes.ResponseClass;
          freeBusyResponse.responseCode = element._elements[0]._elements[0]._text;
          item.freeBusyResponseArray.push(freeBusyResponse);
          break;
        case XmlElement.NAME_FREE_BUSY_VIEW:
          freeBusyResponse.freeBusyViewType = element._elements[0]._text;
          break;
        case XmlElement.NAME_CALENDAR_EVENT_ARRAY:
          freeBusyResponse.calendarEventArray = [];
          calendarEvent = new CalendarEvent();
          freeBusyResponse.calendarEventArray.push(calendarEvent);
          break;
        case XmlElement.NAME_CALENDAR_EVENT:
          fillObjectFields(element, calendarEvent);
          break;
        case XmlElement.NAME_WORKING_HOURS:
          freeBusyResponse.workingHours = new WorkingHours();
          break;
        case XmlElement.NAME_TIME_ZONE:
          freeBusyResponse.workingHours.timeZone = new TimeZone();
          fillObjectFields(element, freeBusyResponse.workingHours.timeZone);
          break;
        case XmlElement.NAME_STANDARD_TIME:
          freeBusyResponse.workingHours.timeZone.standardTime = new Time();
          fillObjectFields(element, freeBusyResponse.workingHours.timeZone .standardTime);
          break;
        case XmlElement.NAME_DAYLIGHT_TIME:
          freeBusyResponse.workingHours.timeZone.daylightTime = new Time();
          fillObjectFields(element, freeBusyResponse.workingHours.timeZone .standardTime);
          break;
        case XmlElement.NAME_WORKING_PERIOD_ARRAY:
          freeBusyResponse.workingHours.workingPeriodArray = [];
          workingPeriod = new WorkingPeriod();
          freeBusyResponse.workingHours.workingPeriodArray.push(workingPeriod);
          break;
        case XmlElement.NAME_WORKING_PERIOD:
          fillObjectFields(element, workingPeriod);
          break;
        case XmlElement.NAME_SUGGESTIONS_RESPONSE:
          item.suggestionsResponse = new SuggestionsResponseMessage();
          item.suggestionsResponse.responseClass = element._elements[0]._attributes.ResponseClass;
          item.suggestionsResponse.responseCode = element._elements[0]._elements[0]._text;
          break;
        case XmlElement.NAME_SUGGESTION_DAY_RESULT_ARRAY:
          item.suggestionsResponse.suggestionDayResultArray = [];
          suggestionDayResult = new SuggestionDayResult();
          item.suggestionsResponse.suggestionDayResultArray.push(suggestionDayResult);
          break;
        case XmlElement.NAME_SUGGESTION_DAY_RESULT_ARRAY:
          fillObjectFields(element, suggestionDayResult);
          break;
        case XmlElement.NAME_SUGGESTION_ARRAY:
          suggestionDayResult.suggestionArray = [];
          break;
        case XmlElement.NAME_SUGGESTION:
          suggestion = new Suggestion();
          fillObjectFields(element, suggestion);
          suggestionDayResult.suggestionArray.push(suggestion);
          break;
      }
      this.printEle(element._elements, item, freeBusyResponse, calendarEvent, workingPeriod, suggestionDayResult, suggestion);
    });
  }
}