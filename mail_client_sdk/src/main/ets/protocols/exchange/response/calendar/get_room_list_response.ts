/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { GetRoomListsResponseMessage } from '../../model/calendar/get_room_lists_message';
import { Mailbox, RoomListsResponseMessage } from '../../model/calendar/calendar_response_types';
import { fillObjectFields } from '../../utils/calendar_util';
import { JsObjectElement } from '../../xml/soap_xml'
import { XmlElement } from '../../xml/xml_element';
import { ResponseBase } from '../response_base'

export class GetRoomListResponse extends ResponseBase<GetRoomListsResponseMessage> {
  public constructor() {
    super();
    this.data = new GetRoomListsResponseMessage();
  }

  protected readBodyFromXml(bodyElement: JsObjectElement): void {
    console.log('liwang---->' + JSON.stringify(bodyElement));
    this.printEle(bodyElement._elements);
  }

  printEle(elements?: JsObjectElement[], item?: RoomListsResponseMessage): void {
    elements?.forEach(element => {
      switch (element._name) {
        case XmlElement.NAME_GET_ROOM_LISTS_RESPONSE:
          item = new RoomListsResponseMessage();
          item.responseClass = element._attributes.ResponseClass;
          this.data.setResponseMessages(item);
          break;
        case XmlElement.NAME_RESPONSE_CODE:
          item[element._name] = element._elements[0]._text;
          break;
        case XmlElement.NAME_ROOM_LISTS:
          item.roomLists = [];
          break;
        case XmlElement.NAME_ADDRESS:
          let address = new Mailbox();
          fillObjectFields(element, address);
          item.roomLists.push(address);
          break;
      }
      this.printEle(element._elements, item);
    });
  }
}