/* Copyright © 2024 Coremail论客. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BusinessError } from '@ohos.base';
import http from '@ohos.net.http';
import { FolderHierarchy, FolderHierarchyResp } from '../../model/sync/folder_hierarchy';
import { SyncFolderHierarchyRequest } from '../../request/sync/sync_folder_hierarchy_request';
import { SyncFolderHierarchyResponse } from '../../response/sync/sync_folder_hierarchy_response';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { TaskBase } from '../task_base';

/**
 * 同步文件夹任务
 *
 * @since 2024-03-24
 */
export class SyncFolderHierarchyTask extends TaskBase<SyncFolderHierarchyRequest, SyncFolderHierarchyResponse, FolderHierarchy, FolderHierarchyResp> {
  public execute(): Promise<SyncFolderHierarchyResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<SyncFolderHierarchyResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: SyncFolderHierarchyResponse = new SyncFolderHierarchyResponse(new FolderHierarchyResp());
            resp.readFromXml(new SoapXmlReader(data.result as string));
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}