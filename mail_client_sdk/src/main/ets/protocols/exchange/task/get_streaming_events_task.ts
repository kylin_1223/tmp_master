/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { TaskBase } from './task_base';
import { BusinessError } from '@kit.BasicServicesKit';
import { GetStreamingEventsRequest } from '../request/get_streaming_events_request';
import { GetStreamingEventsResponse } from '../response/get_streaming_events_response';
import {
  StreamingSubscription,
  GetStreamingEventsResp,
  Notification,
  EventItem
} from '../model/streaming_subscription';
import { getLogger, Logger } from '../../../utils/log';
import { util } from '@kit.ArkTS';
import { UniteId } from '../model/mode_base';
import { FolderItems } from '../model/sync/folder_items';
import { SyncFolderItemsTask } from './sync/sync_folder_items_task';
import { ExchangeError } from '../error/exchange_error';
import { SyncFolderItemsRequest } from '../request/sync/sync_folder_items_request';
import { PropertySet } from '../utils/common_enum';

const logger: Logger = getLogger('StreamingEventsTask');

/**
 * 流式处理通知任务
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsTask extends TaskBase<GetStreamingEventsRequest, GetStreamingEventsResponse,
StreamingSubscription, GetStreamingEventsResp> {
  private subscription: StreamingSubscription;
  private streamingEventsResp: GetStreamingEventsResp;

  /**
   * 设置订阅数据模型
   *
   * @param sub 订阅数据模型
   */
  public setSubscription(sub: StreamingSubscription): void {
    this.subscription = sub;
  }

  /**
   * 设置流式处理通知响应模型
   */
  public getStreamingEventsResp(): GetStreamingEventsResp {
    return this.streamingEventsResp;
  }

  /**
   * 同步已更改的文件夹的内容
   *
   * @param id 已更改的文件夹id
   * @param changeKey 版本值
   */
  private syncFolderItems(id: string, changeKey: string): void {
    // 模拟用户传递数据
    let uniteId: UniteId = {
      id,
      changeKey,
    };
    let propertySet: PropertySet = PropertySet.AllProperties;
    let ignoredItemIds: UniteId[] = [];
    let maxChangesReturned: number = 10;

    // 构造请求数据
    let folderItems: FolderItems = new FolderItems();
    folderItems.syncFolderId = uniteId;
    folderItems.propertySet = propertySet;
    folderItems.ignoredItemIds = ignoredItemIds;
    folderItems.maxChangesReturned = maxChangesReturned;
    let syncFolderItemsTask: SyncFolderItemsTask = new SyncFolderItemsTask(new SyncFolderItemsRequest(folderItems),
      this.properties);
    syncFolderItemsTask.execute().then(data => {
      logger.info('NewMail Synced', data);
    }).catch((error: ExchangeError) => {
      logger.info('NewMail Sync Error', error);
    });
  }

  /**
   * 获取所有更改事件的列表
   *
   * @param notifications 通知列表
   * @returns allEventsList 所有更改事件列表
   */
  private getAllEventsList(notifications: Notification[]): EventItem[] {
    let allEventsList: EventItem[] = [];
    notifications?.forEach(notification => {
      let eventsMap: Map<string, EventItem[]> = notification.events;
      let notificationEventsList: EventItem[] = this.getNotificationEventsList(eventsMap);
      allEventsList.push.apply(allEventsList, notificationEventsList);
    });
    return allEventsList;
  }

  /**
   * 获取每个通知下的更改事件列表
   *
   * @param eventsMap 每个通知的事件Map
   * @returns notificationEventsList 每个通知下的更改事件列表
   */
  private getNotificationEventsList(eventsMap: Map<string, EventItem[]>): EventItem[] {
    let notificationEventsList: EventItem[] = [];
    for (const eventList of eventsMap.values()) {
      eventList.forEach(eventItem => {
        let hasThatEvent: EventItem | undefined = notificationEventsList.find(c => c.itemId === eventItem.itemId);
        if (!hasThatEvent) {
          notificationEventsList.push(eventItem);
        }
      });
    }
    return notificationEventsList;
  }

  public execute(): Promise<GetStreamingEventsResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<GetStreamingEventsResponse>((resolve: Function, reject: Function) => {
      this.httpClient.requestInStream({ extraData: this.request.xmlToString(writer) },
        {
          onDataReceive:(data: ArrayBuffer) => {
            let resp: GetStreamingEventsResponse = new GetStreamingEventsResponse(new GetStreamingEventsResp());
            const textDecoder: util.TextDecoder = util.TextDecoder.create();
            const bufferXml: string = textDecoder.decodeWithStream(new Uint8Array(data));
            this.subscription.streamingEventsXml += bufferXml.toString();
            let streamingEventsXml: string = this.subscription.streamingEventsXml;
            let isEndingEnvelope: boolean = streamingEventsXml.toString().endsWith('</Envelope>');
            if (!isEndingEnvelope) {
              return;
            } else {
              let reader: SoapXmlReader = new SoapXmlReader(streamingEventsXml as string);
              resp.readFromXml(reader, false);
              this.streamingEventsResp = resp.getData();
              if (this.streamingEventsResp.responseType === 'eventResponse') {
                let notifications: Notification[] = resp.getData().notifications;
                let allEventsList: EventItem[] = this.getAllEventsList(notifications);
                allEventsList.forEach(eventItem => {
                  this.syncFolderItems(eventItem.folderId, eventItem.folderChangeKey);
                });
              }
              this.subscription.streamingEventsXml = '';
            }
          },
        }
      ).then((data: number) => {
        resolve(data);
      }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}
