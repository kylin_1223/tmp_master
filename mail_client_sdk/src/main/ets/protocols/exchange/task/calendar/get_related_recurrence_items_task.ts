/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { ModelBase } from '../../model/mode_base';
import { getLogger } from '../../../../utils/log';
import { GetRelatedRecurrenceItemsRequest } from '../../request/calendar/get_related_recurrence_items_request';
import { GetRelatedRecurrenceItemsResponse } from '../../response/calendar/get_related_recurrence_items_response';
import { GetRelatedRecurrenceItemsMessage } from '../../model/calendar/get_related_recurrence_items_message';
import { getEncodeBase64 } from '../../utils/calendar_util';

const logger = getLogger("CalendarGetRelatedRecurrenceItemsTask");

/**
 * 访问定期系列（使用 EWS 托管 API 获取相关的重复日历项）
 */
export class GetRelatedRecurrenceItemsTask extends TaskBase<GetRelatedRecurrenceItemsRequest, GetRelatedRecurrenceItemsResponse, GetRelatedRecurrenceItemsMessage, ModelBase> {
  public async execute(): Promise<GetRelatedRecurrenceItemsResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    const xmlReqString = this.request.xmlToString(writer);
    logger.debug('execute: xmlReqString = ', xmlReqString);
    return new Promise<GetRelatedRecurrenceItemsResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlReqString,header: {
        'Authorization':`Basic ${getEncodeBase64()}`,
        'Content-Type':'text/xml; charset=utf-8',
        'Accept':'text/xml',
        'Accept-Encoding':'gzip,deflate',
      } })
        .then((data: http.HttpResponse) => {
          console.log('[liwang-mail]', '请求结果=' + JSON.stringify(data))
          if (this.isSuccessful(data.responseCode)) {
            let resp: GetRelatedRecurrenceItemsResponse = new GetRelatedRecurrenceItemsResponse();
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}
