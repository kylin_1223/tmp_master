/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { CreateCalendarRequest } from '../../request/calendar/create_calendar_request';
import { CreateCalendarsMessage, FolderResult } from '../../model/calendar/create_calendar_message';
import { CreateCalendarsResponse, CreateCalendarResponse } from '../../response/calendar/create_calendar_response';
import { Properties } from '../../../../api';
import { ResponseBatchModelBase, ResponseModelBase } from '../../response/response_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml';


export class createCalendarTask<R extends CreateCalendarResponse | CreateCalendarsResponse> extends TaskBase<
  CreateCalendarRequest,
  CreateCalendarResponse | CreateCalendarsResponse,
  CreateCalendarsMessage,
  ResponseBatchModelBase<FolderResult> | ResponseModelBase<FolderResult>
> {
  // 是否批量创建
  public batch = false;

  public async execute(): Promise<R> {
    const xmlWriter = new SoapXmlWriter();
    this.request.writeToXml(xmlWriter);
    const xmlReqString = this.request.xmlToString(xmlWriter);
    return new Promise<R>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlReqString })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            let resp: CreateCalendarResponse | CreateCalendarsResponse
            const xmlReader = new SoapXmlReader(data.result as string)
            if (this.batch) {
              resp = new CreateCalendarsResponse();
            } else {
              resp = new CreateCalendarResponse();
            }
            resp.readFromXml(xmlReader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};

export async function createCalendarItems(folders: string[]): Promise<ResponseBatchModelBase<FolderResult>> {
  const msg = new CreateCalendarsMessage();
  msg.folderDisplayNames = folders;
  const task = new createCalendarTask<CreateCalendarsResponse>(new CreateCalendarRequest(msg), properties);
  task.batch = true;
  try {
    const resp = await task.execute();
    const data = resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}

export async function createCalendarItem(folderName: string): Promise<ResponseModelBase<FolderResult>> {
  const msg = new CreateCalendarsMessage();
  msg.folderDisplayNames = [folderName];
  const task = new createCalendarTask<CreateCalendarResponse>(new CreateCalendarRequest(msg), properties);
  try {
    const resp = await task.execute();
    const data = resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}
