/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { Properties } from '../../../../api';
import { ResponseModelBase } from '../../response/response_base';
import { MessageDisposition } from '../../utils/common_enum';
import { SoapXmlReader, SoapXmlWriter } from '../../xml';
import { CancelMeetingRequest } from '../../request/calendar/cancel_meeting_request';
import {
  CancelMeetingInfo,
  CancelMeetingMessage, CancelMeetingResult } from '../../model/calendar/cancel_meeting_message';
import { CancelMeetingResponse } from '../../response/calendar/cancel_meeting_response';

export class CancelMeetingTask extends TaskBase<
  CancelMeetingRequest,
  CancelMeetingResponse,
  CancelMeetingMessage,
  ResponseModelBase<CancelMeetingResult>
> {
  public async execute(): Promise<CancelMeetingResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.writeToXml(xmlWriter);
    const xmlReqString = this.request.xmlToString(xmlWriter);
    return new Promise<CancelMeetingResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlReqString })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            const xmlReader = new SoapXmlReader(data.result as string)
            let resp = new CancelMeetingResponse();
            resp.readFromXml(xmlReader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};


export async function cancelMeeting(appointMessage: CancelMeetingInfo)
  : Promise<ResponseModelBase<CancelMeetingResult>> {
  const cancelMeetingMsg = new CancelMeetingMessage();
  cancelMeetingMsg.messageDisposition = MessageDisposition.SendAndSaveCopy;
  cancelMeetingMsg.items = [appointMessage];
  const task = new CancelMeetingTask(new CancelMeetingRequest(cancelMeetingMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}
