/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { CreateAppointmentItemRequest } from '../../request/calendar/create_appointment_request';
import {
  CreateAppointmentMessage, CreateItemResult,
  InternalAppointmentMessage,
} from '../../model/calendar/create_appointment_message';
import { CreateAppointmentResponse, CreateBatchAppointmentResponse } from '../../response/calendar/create_appointment_response';
import { Properties } from '../../../../api';
import { ResponseBatchModelBase, ResponseModelBase } from '../../response/response_base';
import { SendMeetingInvitations } from '../../utils/common_enum';
import { FolderName, SoapXmlReader, SoapXmlWriter } from '../../xml';
import { CreateItemResponse, CreateItemsResponse } from '../../response/create_item_response';

export class createAppointmentItemTask extends TaskBase<
  CreateAppointmentItemRequest,
  CreateItemResponse,
  CreateAppointmentMessage,
  ResponseModelBase<CreateItemResult>
> {
  public async execute(): Promise<CreateItemResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.writeToXml(xmlWriter);
    const xmlReqString = this.request.xmlToString(xmlWriter);
    return new Promise<CreateItemResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlReqString })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            const xmlReader = new SoapXmlReader(data.result as string)
            let resp = new CreateItemResponse();
            resp.readFromXml(xmlReader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

export class CreateAppointmentItemsTask extends TaskBase<
  CreateAppointmentItemRequest,
  CreateItemsResponse,
  CreateAppointmentMessage,
  ResponseBatchModelBase<CreateItemResult>
> {
  public async execute(): Promise<CreateItemsResponse> {
    const xmlWriter = new SoapXmlWriter();
    this.request.writeToXml(xmlWriter);
    const xmlReqString = this.request.xmlToString(xmlWriter);
    return new Promise<CreateItemsResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlReqString })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            const xmlReader = new SoapXmlReader(data.result as string)
            let resp = new CreateItemsResponse();
            resp.readFromXml(xmlReader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

let properties: Properties = {
  exchange: {
    url: 'https://outlook.office365.com/EWS/Exchange.asmx',
    secure: false,
  },
  userInfo: {
    username: 'wangml9655@outlook.com',
    password: 'wml840576)(',
  }
};


export async function createAppointmentItem(appointMessage: InternalAppointmentMessage, sendMeetingInvitations: SendMeetingInvitations, folderId ?: string): Promise<ResponseModelBase<CreateItemResult>> {
  const appointmentMsg = new CreateAppointmentMessage();
  appointmentMsg.sendInvitationsMode = sendMeetingInvitations;
  if (folderId) {
    appointmentMsg.folderId = folderId;
  } else {
    appointmentMsg.distinguishedFolderId = FolderName.calendar
  }
  appointmentMsg.items = [appointMessage];
  const task = new createAppointmentItemTask(new CreateAppointmentItemRequest(appointmentMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}

export async function createAppointmentItems(appointMessage: InternalAppointmentMessage[], sendMeetingInvitations: SendMeetingInvitations, folderId ?: string): Promise<ResponseBatchModelBase<CreateItemResult>> {
  const appointmentMsg = new CreateAppointmentMessage();
  appointmentMsg.sendInvitationsMode = sendMeetingInvitations;
  if (folderId) {
    appointmentMsg.folderId = folderId;
  } else {
    appointmentMsg.distinguishedFolderId = FolderName.calendar
  }
  appointmentMsg.items = appointMessage;
  const task = new CreateAppointmentItemsTask(new CreateAppointmentItemRequest(appointmentMsg), properties);
  try {
    const resp = await task.execute();
    const data =  resp.getData();
    return data;
  } catch (err) {
    throw err;
  }
}