/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BusinessError } from '@ohos.base';
import http from '@ohos.net.http';
import url from '@ohos.url';
import { Properties } from '../../../api';
import { ErrorType, ExchangeError } from '../error/exchange_error';
import { ModelBase } from '../model/mode_base';
import { RequestBase } from '../request/request_base';
import { RequestBase2 } from '../request/request_base2';
import { ResponseBase } from '../response/response_base';
import { ResponseBase2 } from '../response/response_base2';
import {HttpRequest, HttpRequestBase, HttpStreamRequest} from '../utils/http_client';
import { getLogger, Logger } from '../../../utils/log';

const logger: Logger = getLogger("taskBase");

/**
 * 业务请求任务
 * 主要实现邮件请求的调度，调用request模块获取请求的协议报文，调用HTTP模块发送请求，再调用Response模块解析协议报文。
 *
 * @param T { T } 请求类型
 * @param R { R } 响应类型
 * @param TD { TD } 请求model类型
 * @param RD { RD } 响应model类型
 * @since 2024-03-19
 */
export abstract class TaskBase<T extends RequestBase<TD> | RequestBase2<TD>, R extends ResponseBase<RD> | ResponseBase2<RD>,
TD extends ModelBase, RD extends ModelBase> {
  protected httpClient: HttpRequestBase;

  /**
   * 构造器
   *
   * @param request 请求对象
   * @param properties 属性
   * @param stream 是否流式请求
   */
  public constructor(protected request: T, protected properties: Properties, stream = false) {
    if (stream) {
      this.httpClient = new HttpStreamRequest();
    } else {
      this.httpClient = new HttpRequest();
    }
    try {
      let requestUrl: url.URL = url.URL.parseURL(properties.exchange.url);
      this.httpClient.setUrl(requestUrl.toString());
    } catch (err) {
      logger.error(`parse url failed, code: ${err.code}`);
    }
  }

  /**
   * 销毁
   */
  public destroy(): void {
    this.httpClient.destroy();
  };

  /**
   * 执行任务。
   *
   * @returns  {R} 描述exchange响应。
   * @throws { ExchangeError } HttpError http请求异常
   * @throws { ExchangeError } EwsError Exchange业务异常
   * @throws { ExchangeError } ClientError sdk client异常
   */
  public abstract execute(bufferSize?: number): Promise<R>;

  /**
   * 返回http请求是否成功
   *
   * @param { http.ResponseCode } code HTTP响应状态码
   * @returns 如果为 { true } 成功，否则失败。
   */
  protected isSuccessful(code: http.ResponseCode): boolean {
    return code >= http.ResponseCode.OK && code < http.ResponseCode.BAD_REQUEST;
  }

  /**
   * 返回http请求是否是Ews服务端内部错误。
   *
   * @param { http.ResponseCode } code HTTP响应状态码
   * @returns 如果为 { true } 为服务端内部错误，否则不是。
   */
  protected isInternalError(code: http.ResponseCode): boolean {
    return code === http.ResponseCode.INTERNAL_ERROR;
  }

  /**
   * 处理HTTP状态错误。
   *
   * @param httpResp http请求响应体。
   * @returns { ExchangeError } exchange异常实例对象。
   */
  protected handleHttpStatusError(httpResp: http.HttpResponse): ExchangeError<http.HttpResponse> {
    return new ExchangeError(ErrorType.HTTP_ERROR, 'Http response status error.', httpResp.responseCode);
  }

  /**
   * 处理HTTP请求异常。
   * HTTP 错误码映射关系：2300000 + curl错误码
   * 通用错误码: 201、202、401、801
   * 正则匹配http错误码
   *
   * @param { BusinessError } rawError 原始异常。
   * @returns { ExchangeError } exchange异常实例对象。
   */
  protected handleHttpError(rawError: BusinessError): ExchangeError {
    let type: ErrorType = `${rawError.code}`.match(/^23\d{5}$/) ? ErrorType.HTTP_ERROR : ErrorType.SYS_COMMON_ERROR;
    return new ExchangeError(type, 'An error occurs in the request.', rawError.code);
  }

  /**
   * 抛出一个Exs服务端异常
   *
   * @param errorCode 异常错误码
   */
  protected handleEwsError(errorCode: string): ExchangeError {
    return new ExchangeError(ErrorType.EWS_ERROR, 'Ews API error occurred.', errorCode);
  }
}