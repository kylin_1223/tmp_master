/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { getLogger, Logger } from '../../../../utils/log';
import { MarkAllItemsAsRead, MarkAllItemsAsReadResp } from '../../model/folder/mark_all_items_as_read';
import { MarkAllItemsAsReadResponse } from '../../response/folder/mark_all_items_ad_read_response';
import { MarkAllItemsAsReadRequest } from '../../request/folder/mark_all_items_as_read_request';

const logger: Logger = getLogger("MarkAllItemsAsReadTask");

export class MarkAllItemsAsReadTask extends TaskBase<MarkAllItemsAsReadRequest,MarkAllItemsAsReadResponse, MarkAllItemsAsRead, MarkAllItemsAsReadResp> {
  public async execute(): Promise<MarkAllItemsAsReadResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    logger.info('sss', this.request.xmlToString(writer))
    return new Promise<MarkAllItemsAsReadResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
         console.log('MarkAllItemsAsReadTask',JSON.stringify(data.result))
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: MarkAllItemsAsReadResponse = new MarkAllItemsAsReadResponse();
            const result: string = data.result as string;
            logger.debug(result);
            resp.readFromXml(new SoapXmlReader(result));
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
      });
    });
  }
}
