/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { getLogger, Logger } from '../../../../utils/log';
import { MoveFolderRequest } from '../../request/folder/move_folder_request';
import { MoveFolder, MoveFolderResp } from '../../model/folder/move_folder';
import { MoveFolderResponse } from '../../response/folder/move_folder_response';

const logger: Logger = getLogger("MoveFolderTask");

export class MoveFolderTask extends TaskBase<MoveFolderRequest, MoveFolderResponse, MoveFolder, MoveFolderResp> {
  public async execute(): Promise<MoveFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    logger.info(this.request.xmlToString(writer))
    return new Promise<MoveFolderResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: MoveFolderResponse = new MoveFolderResponse();
            const result: string = data.result as string;
            logger.debug(result);
            resp.readFromXml(new SoapXmlReader(result));
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
      });
    });
  }
}
