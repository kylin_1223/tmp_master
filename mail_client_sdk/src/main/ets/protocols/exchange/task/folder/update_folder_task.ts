/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { UpdateFolderResponse } from '../../response/folder/update_folder_response';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { UpdateFolder, UpdateFolderResp } from '../../model/folder/update_folder';
import { UpdateFolderRequest } from '../../request/folder/update_folder_request';
import { getLogger, Logger } from '../../../../utils/log';

const logger: Logger = getLogger('UpdateFolderTask');

/**
 * 更新文件夹任务
 *
 * @since 2024-03-19
 */
export class UpdateFolderTask extends TaskBase<UpdateFolderRequest, UpdateFolderResponse, UpdateFolder, UpdateFolderResp> {
  public async execute(): Promise<UpdateFolderResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    logger.info(this.request.xmlToString(writer));
    return new Promise<UpdateFolderResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            let resp: UpdateFolderResponse = new UpdateFolderResponse();
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            if(resp.isSuccessful()){
              resolve(resp);
            }else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}
