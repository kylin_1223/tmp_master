/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from './task_base';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { GetItem } from '../model/get_item';
import { GetItemRequest } from '../request/get_item_request';
import { ResponseBase } from '../response/response_base';
import { ModelBase } from '../model/mode_base';
import { Properties } from '../../../api';

/**
 * 获取item任务，item包括邮件、任务、联系人等
 *
 * @since 2024-04-08
 */
export class GetItemTask<R extends ResponseBase<RD>, RD extends ModelBase>
  extends TaskBase<GetItemRequest, R, GetItem, RD> {
  private response: R; // 响应类

  /**
   * 构造函数
   *
   * @param request 请求
   * @param properties 属性
   * @param getItemResponse 返回
   * @param stream 是否流式请求
   */
  public constructor(protected request: GetItemRequest,
                     protected properties: Properties, getItemResponse: R, stream = false) {
    super(request, properties, stream);
    this.response = getItemResponse;
  }

  /**
   * 执行具体的任务，开始网络请求
   *
   * @returns Promise<R> 请求对应的响应类
   */
  public execute(): Promise<R> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    let xmlStr: string = this.request.xmlToString(writer);
    return new Promise<R>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: xmlStr })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            this.response.readFromXml(reader);
            if (this.response.isSuccessful()) {
              resolve(this.response);
            } else {
              reject(this.handleEwsError(this.response.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

