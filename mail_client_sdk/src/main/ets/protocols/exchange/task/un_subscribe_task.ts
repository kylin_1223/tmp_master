/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { UnSubscribeRequest } from '../request/un_subscribe_request';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { TaskBase } from './task_base';
import { http } from '@kit.NetworkKit';
import { BusinessError } from '@kit.BasicServicesKit';
import { UnSubscribeResponse } from '../response/un_subscribe_response';
import { StreamingSubscription, UnSubscribeResp } from '../model/streaming_subscription';

/**
 * 取消订阅任务
 *
 * @since 2024-03-19
 */
export class UnSubscribeTask extends TaskBase<UnSubscribeRequest, UnSubscribeResponse,
StreamingSubscription, UnSubscribeResp> {
  public execute(): Promise<UnSubscribeResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<UnSubscribeResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: UnSubscribeResponse = new UnSubscribeResponse(new UnSubscribeResp());
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}