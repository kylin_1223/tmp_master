/* Copyright © 2023 - 2024 Coremail论客.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CMError, ErrorCode, Properties } from '../../../api';
import { ModelBase } from '../model/mode_base';
import { RequestBase } from '../request/request_base';
import { ResponseBase } from '../response/response_base';
import { formatString } from '../utils/string_helper';
import { GetUserSettingsRequest } from '../request/get_user_settings_request';
import { GetUserSettingsResponse } from '../response/get_user_settings_response';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { HttpRequest } from '../utils/http_client';
import { AutoDiscoverMessage, UserSettingMessage, UserSettingMessageResp } from '../model/autodiscover_message';
import { getLogger, Logger } from '../../../utils/log';
import { ClientErrorCode, ErrorType, ExchangeError } from '../error/exchange_error';

const logger: Logger = getLogger('AutoDiscover');

/**
 * 自动发现入口函数
 *
 * @param emailAddress 用户邮箱
 * @param callback 处理连接点的状态
 * @returns url字符串数组。
 */
export async function autoDiscoverUrl(prop: Properties, callback?: (progress: number) => void): Promise<string> {
  let autoDiscoverMessage: AutoDiscoverMessage = new AutoDiscoverMessage();
  let userSettingMessage: UserSettingMessage = {
    requestedServerVersion: 'Exchange2010',
    action: 'http://schemas.microsoft.com/exchange/2010/Autodiscover/Autodiscover/GetUserSettings',
    to: 'https://autodiscover-s.outlook.com/autodiscover/autodiscover.svc',
    emailAddress: prop.userInfo.username,
    setting: 'ExternalEwsUrl',
  }
  autoDiscoverMessage.setUserSettingMessage(userSettingMessage);
  let getUserSettingsRequest: GetUserSettingsRequest = new GetUserSettingsRequest(autoDiscoverMessage);
  let getUserSettingsTask: GetUserSettingsTask = new GetUserSettingsTask(getUserSettingsRequest, prop);
  getUserSettingsTask.callback = callback;
  try {
    let resp: GetUserSettingsResponse = await getUserSettingsTask.execute();
    return resp.getData().ewsEndpoint;
  } catch (err) {
    logger.error(`autoDiscoverUrl, auto discover failed, code: ${err.code}`);
  }
  return '';
}

/**
 * 自动发现任务基类。
 */
abstract class AutoDiscoverBaseTask<T extends RequestBase<TD>, R extends ResponseBase<RD>, TD extends ModelBase,
RD extends ModelBase> {
  /**
   * 自动发现标识字符
   */
  private static readonly AUTO_DISCOVER_TAG = 'autodiscover';

  /**
   * 自动发现SOAP端点模板URL
   */
  private static readonly AUTO_DISCOVER_SOAP_HTTPS_URL = 'https://{0}/autodiscover/autodiscover.svc';

  protected domain: string | null = null;
  protected httpClient: HttpRequest | null = null;

  /**
   * 构造器
   *
   * @param request 请求。
   * @param properties 属性。
   */
  public constructor(protected request: T, properties: Properties) {
    this.httpClient = new HttpRequest();
    let mailAddress: string = properties.userInfo.username;
    this.domain = mailAddress.substring(mailAddress.indexOf('@') + 1, mailAddress.length);
  }

  /**
   * 产生自动发现服务端点。
   *
   * @param domain 用户域。
   * @returns url字符串数组。
   */
  protected generateEndpoints(domain: string): string[] {
    return [...this.findEndpointsByScp(), ...this.forkEndpointsFromDomain(domain)];
  }

  /**
   * 使用Scp查找自动发现服务端点。
   *
   * @returns url字符串数组。
   */
  protected findEndpointsByScp(): string[] {
    return [];
  }

  /**
   * 从域派生端点。
   *
   * @param domain 用户域。
   * @returns url字符串数组。
   */
  protected forkEndpointsFromDomain(domain: string): string[] {
    return [
      formatString(AutoDiscoverBaseTask.AUTO_DISCOVER_SOAP_HTTPS_URL, domain),
      formatString(AutoDiscoverBaseTask.AUTO_DISCOVER_SOAP_HTTPS_URL,
        `${AutoDiscoverBaseTask.AUTO_DISCOVER_TAG}.${domain}`),
    ];
  }
}

/**
 * 自动发现获取用户设置任务
 */
class GetUserSettingsTask extends AutoDiscoverBaseTask<GetUserSettingsRequest, GetUserSettingsResponse,
AutoDiscoverMessage, UserSettingMessageResp> {
  public callback: Function;

  /**
   * 执行生成自动发现终结点的列表，并且发现查找连接点
   */
  public async execute(): Promise<GetUserSettingsResponse> {
    if (!this.domain) {
      throw new CMError('The user domain is empty.', ErrorCode.PARAMETER_ERROR);
    }
    let write: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(write);
    let requestData: string = this.request.xmlToString(write);
    let urls: string[] = this.generateEndpoints(this.domain);
    for (let i = 0; i < urls.length; i++) {
      let url = urls[i];
      let result: string = await this.getUrl(url, requestData.trimEnd());
      if (this.callback && result == '') {
        this.callback((i+1) / urls.length);
        continue;
      }
      let resp: GetUserSettingsResponse = new GetUserSettingsResponse(new UserSettingMessageResp());
      let reader: SoapXmlReader = new SoapXmlReader(result);
      resp.readFromXml(reader, false);
      if (resp.isSuccessful()) {
        this.callback(1);
        return resp;
      }
    }
    this.callback(1);
  }

  /**
   * 发送http请求，发现查找连接点
   *
   * @param url 请求链接
   * @param requestData http请求参数
   * @returns Promise resolve对象
   */
  private async getUrl(url: string, requestData: string): Promise<string> {
    this.httpClient.setUrl(url);
    try {
      let resp = await this.httpClient.request({
        extraData: requestData,
      });
      return resp.result?.toString();
    } catch (err) {
      logger.warn(`getUrl, error occurred, code: ${err.code}`);
      return '';
    }
  }
}

