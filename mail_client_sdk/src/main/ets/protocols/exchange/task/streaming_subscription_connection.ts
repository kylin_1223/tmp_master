/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { EventTypes, GetStreamingEventsResp, StreamingSubscription } from '../model/streaming_subscription';
import { StreamingSubscriptionTask } from './streaming_subscription_task';
import { StreamingSubscriptionRequest } from '../request/streaming_subscription_request';
import { UnSubscribeRequest } from '../request/un_subscribe_request';
import { UnSubscribeTask } from './un_subscribe_task';
import { Properties } from '../../../api';
import { getLogger, Logger } from '../../../utils/log';
import { GetStreamingEventsRequest } from '../request/get_streaming_events_request';
import { GetStreamingEventsTask } from './get_streaming_events_task';
import { FolderName } from '../xml';

const logger: Logger = getLogger('SubscriptionConnection');

// 数字常量枚举
enum Constants {
  NumberZero = 0,
  NumberOne  = 1,
  NumberMaxTime = 30,
  NumberMinute = 60,
  NumberSecond = 1000,
}

/**
 * 流式通知操作类
 * 用于流式通知的订阅、流式处理通知请求、取消订阅以及销毁订阅
 *
 * @since 2024-03-19
 */
export class StreamingSubscriptionConnection {
  /**
   * 构造器
   *
   * @param subscription 订阅数据模型
   * @param properties 请求相关属性
   */
  public constructor(private subscription: StreamingSubscription, private properties: Properties) {
  }

  /**
   * 设置文件夹标识符数组
   *
   * @param names 文件夹标识符数组
   */
  public setFolderIds(names: FolderName[]): void {
    this.subscription.folderIds = names;
  }

  /**
   * 设置事件通知的集合
   *
   * @param events 事件通知的集合
   */
  public setEventTypes(events: EventTypes[]): void {
    this.subscription.eventTypes = events;
  }

  /**
   * 创建订阅
   * 创建一个拥有唯一标识的流式订阅
   *
   * @returns Promise<void> 用于返回异常
   */
  public requestSubscription(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      if (!this.subscription.eventTypes.length) {
        logger.info('EventTypes cannot be empty!');
        return;
      }
      let streamingSubscriptionRequest: StreamingSubscriptionRequest = new StreamingSubscriptionRequest(
        this.subscription);
      let task: StreamingSubscriptionTask = new StreamingSubscriptionTask(streamingSubscriptionRequest,
        this.properties);
      task.execute().then((res) => {
        logger.info('Subscribe to StreamingNotification Success', res);
        this.subscription.subscriptionId = res.getData().subscriptionId;
        resolve();
      }).catch((err) => {
        logger.info('Subscribe to StreamingNotification Error');
        reject(err);
      });
    });
  }

  /**
   * 流式处理通知请求
   * 与服务器达成连接，接收事件流通知
   *
   * @param time 打开状态的分钟数(只能设置1-30的整数),默认为30分钟
   * @returns Promise<void> 用于返回异常
   */
  public open(time: number = Constants.NumberMaxTime): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      if (time < Constants.NumberOne || time > Constants.NumberMaxTime ||
        time%Constants.NumberOne !== Constants.NumberZero ) {
        throw new Error('Please enter an integer ranging from 1 to 30!')
      }
      let connectionTime: string = time.toString();
      this.subscription.connectionTime = connectionTime;
      let getStreamingEventsRequest: GetStreamingEventsRequest = new GetStreamingEventsRequest(this.subscription);
      let getStreamingEventsTask: GetStreamingEventsTask = new GetStreamingEventsTask(getStreamingEventsRequest,
        this.properties, true);
      getStreamingEventsTask.setSubscription(this.subscription);
      getStreamingEventsTask.execute().then((res) => {
        let streamingEventsResp: GetStreamingEventsResp = getStreamingEventsTask.getStreamingEventsResp();
        if (streamingEventsResp.connectionStatus === 'close') {
          this.reconnection(time);
        }
        setTimeout(() => {
          this.reconnection(time);
        }, time * Constants.NumberMinute * Constants.NumberSecond);
        resolve();
      }).catch((err) => {
        logger.info('GetStreamingEvents (streamingSubscription) connect Error');
        reject(err);
      });
    });
  }

  /**
   * 取消订阅
   * 中断连接,并销毁订阅数据模型
   *
   * @returns Promise<void> 用于返回异常
   */
  public close(): Promise<void> {
    return new Promise<void>((resolve: Function, reject: Function) => {
      let unSubscribeRequest: UnSubscribeRequest = new UnSubscribeRequest(this.subscription);
      let unSubscribeTask: UnSubscribeTask = new UnSubscribeTask(unSubscribeRequest, this.properties);
      unSubscribeTask.execute().then((res) => {
        logger.info('Unsubscribe streamingNotification Success', res);
        this.dispose();
        resolve();
      }).catch((err) => {
        logger.info('Unsubscribe streamingNotification Error');
        reject(err);
      });
    });
  }

  /**
   * 销毁订阅，用于释放内存
   */
  public dispose(): void {
    this.subscription = null;
  }

  /**
   * 重新连接
   * 连接超时或连接意外中断时重新建立连接
   *
   * @param time 连接时间
   */
  private reconnection(time: number): void {
    this.requestSubscription();
    this.open(time);
  }
}