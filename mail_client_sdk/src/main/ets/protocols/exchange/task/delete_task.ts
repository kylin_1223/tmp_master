/* Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from './task_base';
import { SoapXmlReader, SoapXmlWriter } from '../xml/soap_xml';
import { DeleteItem, DeleteItemResp } from '../model/delete_item';
import { DeleteItemRequest } from '../request/delete_item_request';
import { DeleteItemResponse } from '../response/delete_item_response';
import { getLogger } from '../../../utils/log';

const logger = getLogger("DeleteTask");

export class DeleteItemTask extends TaskBase<DeleteItemRequest, DeleteItemResponse, DeleteItem, DeleteItemResp> {
  public async execute(): Promise<DeleteItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    logger.info(this.request.xmlToString(writer));
    return new Promise<DeleteItemResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: DeleteItemResponse = new DeleteItemResponse(new DeleteItemResp());
            const result: string = data.result as string;
            logger.debug(result);
            resp.readFromXml(new SoapXmlReader(result));
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

