/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { CreateContactRequest } from '../../request/contact/create_contact_request';
import { CreateContact, CreateContactResp } from '../../model/contact/create_contact';
import { CreateContactResponse } from '../../response/contact/create_contact_response';

/**
 * 创建联系人网络请求类
 *
 * @since 2024-04-11
 */
export class CreateContactTask extends TaskBase<CreateContactRequest, CreateContactResponse, CreateContact, CreateContactResp> {
  /**
   * 创建联系人网络请求API
   *
   * @returns 返回附带请求结果的Promise，内部判断为NoError则reject
   */
  public async execute(): Promise<CreateContactResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<CreateContactResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode)) {
            let resp: CreateContactResponse = new CreateContactResponse(new CreateContactResp());
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            if (resp.isSuccessful()) {
              resolve(resp);
            } else {
              reject(this.handleEwsError(resp.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    })
  }
}