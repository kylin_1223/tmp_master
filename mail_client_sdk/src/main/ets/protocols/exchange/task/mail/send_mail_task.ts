/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { SendItemResponse, SendMailResponse } from '../../response/mail/send_mail_response';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { CreateMessage } from '../../model/mail/create_message';
import { SendItemRequest, SendMailRequest } from '../../request/mail/send_mail_request';
import { getLogger, Logger } from '../../../../utils/log';
import { SendItem } from '../../model/mail/send_item';
import { ModelBase } from '../../model/mode_base';
import { CreateItem } from '../../model/create_item';

const logger: Logger = getLogger('SendMailTask');

/**
 * 发送邮件任务
 *
 * @since 2024-03-16
 */
export class SendMailTask extends TaskBase<SendMailRequest, SendMailResponse, CreateMessage, CreateItem> {
  public async execute(): Promise<SendMailResponse> {
    let size = 1024 * 4; // 预留4K
    const attachments = this.request.getRawData().getMessage().attachments ?? [];
    attachments.forEach(attachment => {
      size += attachment.size;
    })
    let writer: SoapXmlWriter = new SoapXmlWriter(size);
    this.request.writeToXml(writer);
    return new Promise<SendMailResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let sendMailResponse: SendMailResponse = new SendMailResponse(new CreateItem());
            const result: string = data.result as string;
            logger.debug(result);
            sendMailResponse.readFromXml(new SoapXmlReader(result));
            if (sendMailResponse.isSuccessful()) {
              resolve(sendMailResponse);
            } else {
              reject(this.handleEwsError(sendMailResponse.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        })
        .catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
        });
    });
  }
}

/**
 * 发送草稿任务
 *
 * @since 2024-03-16
 */
export class SendItemTask extends TaskBase<SendItemRequest, SendItemResponse, SendItem, ModelBase> {
  public execute(): Promise<SendItemResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<SendItemResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let sendItemResponse: SendItemResponse = new SendItemResponse(new CreateItem());
            const result: string = data.result as string;
            logger.debug(result);
            sendItemResponse.readFromXml(new SoapXmlReader(result));
            if (sendItemResponse.isSuccessful()) {
              resolve(sendItemResponse);
            } else {
              reject(this.handleEwsError(sendItemResponse.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        })
        .catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
        });
    });
  }
}
