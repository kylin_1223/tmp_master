/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { CreateAttachmentResponse } from '../../response/mail/attachment_response';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { CreateAttachment, CreateAttachmentResp, FileAttachment } from '../../model/mail/attachments';
import { CreateAttachmentRequest } from '../../request/mail/attachment_request';
import { getLogger } from '../../../../utils/log';

const logger = getLogger('Attachment--')

/**
 * 创建附件类
 */
export class CreateAttachmentTask extends TaskBase<CreateAttachmentRequest, CreateAttachmentResponse,
  CreateAttachment, CreateAttachmentResp> {
  public async execute(): Promise<CreateAttachmentResponse> {
    let attachments = this.request.getRawData().attachment.attachments;
    let contentSize: number = 1024 * 8;
    if (attachments.length) {
      attachments.forEach(item => {
        contentSize += JSON.stringify(item).length;
      })
    }
    let writer: SoapXmlWriter = new SoapXmlWriter(contentSize);
    this.request.writeToXml(writer);
    return new Promise<CreateAttachmentResponse>((resolve: Function, reject: Function) => {
      logger.info('requestXML',this.request.xmlToString(writer));
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: CreateAttachmentResponse = new CreateAttachmentResponse();
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}
