/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { OofSettings } from '../../model/mail/oof_settings';
import { GetOofRequest, SetOofRequest } from '../../request/mail/oof_settings_request';
import { Properties } from '../../../../api';
import { GetOofSettingsTask, SetOofSettingsTask } from './oof_settings_task';
import { ExchangeError } from '../../error/exchange_error';

const ONE_DAY_MILLISECONDS: number = 24 * 3600 * 1000;

/**
 * 自动回复类
 *
 * @since 2024-03-16
 */
export class AutoReply {

  /**
   * 自动回复构造器
   *
   * @param properties 用户配置属性
   */
  public constructor(private properties: Properties) {
  }

  /**
   * 获取自动回复设置
   *
   * @returns 自动回复设置对象
   */
  public getOofSettings(): Promise<OofSettings> {
    const settings: OofSettings = new OofSettings();
    settings.address = this.properties.userInfo.username;
    const oofGetter: GetOofSettingsTask = new GetOofSettingsTask(new GetOofRequest(settings), this.properties);
    return new Promise((resolve, reject) => {
      oofGetter.execute().then((res) => {
        resolve(res.getData() as OofSettings);
      }).catch((err: ExchangeError) => {
        reject(err);
      });
    });
  }

  /**
   * 设置自动回复
   *
   * @param oof 自动回复数据对象
   */
  public setOofSettings(oof: OofSettings): Promise<void> {
    if (!oof.startTime || !oof.endTime) {
      oof.startTime = new Date();
      oof.endTime = new Date(oof.startTime.getTime() + ONE_DAY_MILLISECONDS);
    }
    if (oof.startTime.getTime() > oof.endTime.getTime()) {
      [oof.startTime, oof.endTime] = [oof.endTime, oof.startTime];
    }
    const oofSetter: SetOofSettingsTask = new SetOofSettingsTask(new SetOofRequest(oof), this.properties);
    return new Promise((resolve, reject) => {
      oofSetter.execute().then(() => {
        resolve();
      }).catch((err: ExchangeError) => {
        reject(err);
      });
    });
  }
}