/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fs from '@ohos.file.fs';
import { util } from '@kit.ArkTS';
import { BusinessError } from '@kit.BasicServicesKit';
import {
  DownLoadAttachment,
  DownLoadAttachmentResp,
  FileAttachment,
  ItemAttachment
} from '../../model/mail/attachments';
import { getLogger } from '../../../../utils/log';
import { base64Decode } from '../../../../utils/encodings';
import { getFileSuffixByType } from '../../../../utils/file_stream';
import { SoapXmlReader, SoapXmlWriter } from '../../xml';
import { TaskBase } from '../task_base';
import { DownLoadAttachmentRequest } from '../../request/mail/attachment_request';
import { DownLoadAttachmentResponse, GetAttachmentResponse } from '../../response/mail/attachment_response';

const logger = getLogger('DownLoadAttachmentTask--')

/**
 * 下载附件类
 */
export class DownLoadAttachmentTask extends TaskBase<DownLoadAttachmentRequest, DownLoadAttachmentResponse, DownLoadAttachment, DownLoadAttachmentResp> {
  public async execute(): Promise<DownLoadAttachmentResponse> {
    let path = this.request.getRawData().path;
    let onDataReceive = this.request.getRawData().onDataReceive;
    let onComplete = this.request.getRawData().onComplete;
     await this.downLoad(path, onDataReceive, onComplete);
    const downLoadAttachmentResponse: DownLoadAttachmentResponse = new DownLoadAttachmentResponse();
    return new Promise((resolve: Function) => {
      resolve(downLoadAttachmentResponse);
    });
  }
  /**
   * 下载附件
   *
   * @param path 下载路径
   * @param onDataReceive 进度callback
   * @param onComplete 文件下载路径加文件名称callback
   * @returns 空
   */
  public downLoad(path: string, onDataReceive: Function, onComplete: Function): Promise<string> {
    let streamingEventsXml: string = '';
    return new Promise((resolve: Function, reject: Function) => {
      let writer: SoapXmlWriter = new SoapXmlWriter();
      this.request.writeToXml(writer);
      logger.info('XML', this.request.xmlToString(writer));
      this.httpClient.requestInStream({ extraData: this.request.xmlToString(writer) },
        {
          onDataReceive: (data) => {
            logger.info('XML onDataReceive');
            const textDecoder: util.TextDecoder = util.TextDecoder.create();
            let bufferXml = textDecoder.decodeWithStream(new Uint8Array(data));
            streamingEventsXml += bufferXml.toString();
          },
          onDataReceiveProgress: (receiveSize,totalSize) => {
            logger.info('XML onDataReceiveProgress');
            let progress: string = (receiveSize/totalSize * 100)+'%';
            logger.info("progress",progress)
            onDataReceive(progress);
          },
        }).then((data: number) => {
          logger.info('XML then');
          if (data === 200) {
            const resp: GetAttachmentResponse = new GetAttachmentResponse();
            const reader: SoapXmlReader = new SoapXmlReader(streamingEventsXml as string);
            let name: string;
            let contentType: string;
            const uuid = util.generateRandomUUID();
            resp.readFromXml(reader).getData().responseMessages.forEach(responseMessages => {
              if(responseMessages.responseCode == 'NoError'){
                responseMessages.attachmentList.forEach(att => {
                  if (att instanceof FileAttachment) {
                    const fileAttachment = (att as FileAttachment);
                    name = fileAttachment.name;
                    contentType = fileAttachment.contentType;
                    this.downloadBase64(fileAttachment.content, contentType, name+uuid, path);
                  }
                })
                const finalName: string =`${path}/${name}${uuid}.${getFileSuffixByType(contentType)}`;
                onComplete(finalName);
                resolve();
              } else {
                reject(responseMessages);
              }
            })
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }

  /**
   * 取消下载附件
   */
  public cancelDownLoad(): void {
    logger.info('cancelDownLoad success')
    this.httpClient.destroy();
  }

  /**
   * 下载文件base64到filesDir
   */
  public downloadBase64(base64String: string, contentType: string, fileName: string, filesDir: string) {
    try {
      let file = fs.openSync(`${filesDir}/${fileName}.${getFileSuffixByType(contentType)}` , fs.OpenMode.READ_WRITE | fs.OpenMode.CREATE);
      let writeLen = fs.writeSync(file.fd, base64Decode(base64String).buffer);
      logger.info('base64String', writeLen)
      fs.closeSync(file);
    } catch (e) {
      logger.info('task file', e)
    }
  }

  /**
   * 返回项目附件JS对象
   */
  public async getItemAttachment(itemAttachment: ItemAttachment): Promise<ItemAttachment> {
    return itemAttachment;
  }
}


