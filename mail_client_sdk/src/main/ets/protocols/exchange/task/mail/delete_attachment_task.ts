/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { DeleteAttachmentResponse } from '../../response/mail/attachment_response';
import { TaskBase } from '../task_base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/soap_xml';
import { DeleteAttachment, DeleteAttachmentResp } from '../../model/mail/attachments';
import { DeleteAttachmentRequest } from '../../request/mail/attachment_request';
import { getLogger } from '../../../../utils/log';

const logger = getLogger('Attachment--')

/**
 * 删除附件类
 *
 * @since
 */
export class DeleteAttachmentTask extends TaskBase<DeleteAttachmentRequest, DeleteAttachmentResponse,
  DeleteAttachment, DeleteAttachmentResp> {
  public async execute(): Promise<DeleteAttachmentResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    logger.info('requestXML',this.request.xmlToString(writer));
    return new Promise<DeleteAttachmentResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let resp: DeleteAttachmentResponse = new DeleteAttachmentResponse();
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            resp.readFromXml(reader);
            resolve(resp);
          } else {
            reject(this.handleHttpStatusError(data));
          }
        }).catch((err: BusinessError) => {
        reject(this.handleHttpError(err));
      });
    });
  }
}

