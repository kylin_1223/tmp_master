/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import http from '@ohos.net.http';
import { BusinessError } from '@ohos.base';
import { SoapXmlReader, SoapXmlWriter } from '../../xml/index';
import { TaskBase } from '../task_base';
import { OofSettings } from '../../model/mail/oof_settings';
import { GetOofResponse, SetOofResponse } from '../../response/mail/oof_settings_response';
import { GetOofRequest, SetOofRequest } from '../../request/mail/oof_settings_request';

/**
 * autoReply获取设置
 *
 * @since 2024-03-16
 */
export class GetOofSettingsTask extends TaskBase<GetOofRequest, GetOofResponse, OofSettings, OofSettings> {
  public execute(): Promise<GetOofResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<GetOofResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let getOofResponse: GetOofResponse = new GetOofResponse(new OofSettings());
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            getOofResponse.readFromXml(reader);
            if (getOofResponse.isSuccessful()) {
              resolve(getOofResponse);
            } else {
              reject(this.handleEwsError(getOofResponse.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        })
        .catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
        });
    });
  }
}

/**
 * autoReply设置任务
 *
 * @since 2024-03-16
 */
export class SetOofSettingsTask extends TaskBase<SetOofRequest, SetOofResponse, OofSettings, OofSettings> {
  public execute(): Promise<SetOofResponse> {
    let writer: SoapXmlWriter = new SoapXmlWriter();
    this.request.writeToXml(writer);
    return new Promise<SetOofResponse>((resolve: Function, reject: Function) => {
      this.httpClient.request({ extraData: this.request.xmlToString(writer) })
        .then((data: http.HttpResponse) => {
          if (this.isSuccessful(data.responseCode) || this.isInternalError(data.responseCode)) {
            let setOofResponse: SetOofResponse = new SetOofResponse();
            let reader: SoapXmlReader = new SoapXmlReader(data.result as string);
            setOofResponse.readFromXml(reader);
            if (setOofResponse.isSuccessful()) {
              resolve(setOofResponse);
            } else {
              reject(this.handleEwsError(setOofResponse.getRespCode()));
            }
          } else {
            reject(this.handleHttpStatusError(data));
          }
        })
        .catch((err: BusinessError) => {
          reject(this.handleHttpError(err));
        });
    });
  }
}
