/* Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import xml from '@ohos.xml';
import util from '@ohos.util';
import convertxml from '@ohos.convertxml';
import { XmlElement } from './xml_element';
import { XmlNamespace } from './xml_namespace';
import { ResponseBase } from '../response/response_base';
import { ModelBase } from '../model/mode_base';
import { booleanToString, formatTimeToUtcString } from '../utils/transformToString';

/**
 * xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
 *
 * @since 2024-03-19
 */
export class SoapXmlWriter {
  private static readonly BUFFER_SIZE: number = 1024 * 8;
  private arrayBuffer: ArrayBuffer;
  private xmlInstance: xml.XmlSerializer;
  private depth: number = 0;

  /**
   * xml写入器，调用系统xml生成接口，根据指定参数生成xml文本
   *
   * @param size number
   */
  public constructor(size?: number) {
    this.arrayBuffer = new ArrayBuffer(size || SoapXmlWriter.BUFFER_SIZE);
    this.xmlInstance = new xml.XmlSerializer(this.arrayBuffer);
  }

  /**
   * 写入<?xml version="1.0" encoding="utf-8"?>
   *
   * @returns SoapXmlWriter
   */
  public writeStartDocument(): SoapXmlWriter {
    this.xmlInstance.setDeclaration();
    return this;
  }

  /**
   * 根据属性写入一个闭合元素或非闭合元素，例：<t:RequestServerVersion Version="Exchange2007_SP1" />
   *
   * @param namespace 命名空间，如上例中的 t
   * @param elementName 元素名称，如上例中的 RequestServerVersion
   * @param attr 属性map表，key为上例中的Version，值为Exchange2007_SP1
   * @param closed 写入元素标签是否闭合，默认为闭合 (true)
   * @returns 返回当前写入器对象
   */
  public writeElementWithAttribute(namespace: string, elementName: string, attr: Map<string, string>, closed:boolean = true):
    SoapXmlWriter {
    this.writeStartElement(namespace, elementName);
    attr.forEach((value: string, key: string) => {
      this.writeAttribute(key, value);
    })
    if (closed) {
      this.writeEndElement();
    }
    return this;
  }

  /**
   * 根据元素值写入一个闭合元素，例：<t:Subject>Company Soccer Team</t:Subject>
   *
   * @param namespace 命名空间，如上例中的 t
   * @param elementName 元素名称，如上例中的 Subject
   * @param value 元素值，如上例中的 Company Soccer Team
   * @returns 返回当前写入器对象
   */
  public writeElementWithValue(namespace: string, elementName: string, value: string): SoapXmlWriter {
    return this.writeStartElement(namespace, elementName).writeElementValue(value).writeEndElement();
  }

  /**
   * 根据属性和元素值写入一个完整的闭合元素，例：<t:Body BodyType="HTML">Are you interested in joining?</t:Body>
   *
   * @param namespace 命名空间，如上例中的 t
   * @param elementName 元素名称，如上例中的 Body
   * @param attr 属性map表，key为上例中的 BodyType，值为 HTML
   * @param value 元素值，如上例中的 Are you interested in joining?
   * @returns 返回当前写入器对象
   */
  public writeIntactElement(namespace: string, elementName: string, attr: Map<string, string>, value: string):
    SoapXmlWriter {
    this.writeStartElement(namespace, elementName);
    attr.forEach((value: string, key: string) => {
      this.writeAttribute(key, value);
    })
    this.writeElementValue(value);
    this.writeEndElement();
    return this;
  }

  /**
   * 写入一个不带命名空间的元素
   *
   * @param elementName 元素名称
   * @returns 当前写入器
   */
  public writeStartSingleElement(elementName: string): SoapXmlWriter {
    return this.writeStartElement('', elementName);
  }

  /**
   * 写入开始标签 <name>
   *
   * @param name 标签名称
   * @returns SoapXmlWriter
   */
  public writeStartElement(namespace: string, elementName: string): SoapXmlWriter {
    const realElementName = namespace ? namespace + ':' + elementName : elementName;
    this.xmlInstance.startElement(realElementName);
    this.depth++;
    return this;
  }

  /**
   * 写入开始标签 <name>
   *
   * @param name 标签名称
   * @returns SoapXmlWriter
   */
  public writeStartElementWithoutNamespace(elementName: string): SoapXmlWriter {
    this.xmlInstance.startElement(elementName);
    this.depth++;
    return this;
  }

  /**
   * 写入结束标签 </name>
   *
   * @returns SoapXmlWriter
   */
  public writeEndElement(): SoapXmlWriter {
    this.xmlInstance.endElement();
    this.depth--;
    return this;
  }

  /**
   * 写入一个属性
   *
   * @param attributeName 属性名称
   * @param value 属性值
   * @returns 当前写入器
   */
  public writeAttribute(attributeName: string, value: string): SoapXmlWriter {
    if (value) {
      this.xmlInstance.setAttributes(attributeName, value);
    }
    return this;
  }

  /**
   * 写入一个带命名空间的属性
   *
   * @param namespace 命名空间
   * @param attributeName 属性名称
   * @param value 属性值
   * @returns 当前写入器
   */
  public writeAttrWithNamespace(namespace: XmlNamespace, attributeName: string, value: string): SoapXmlWriter {
    this.xmlInstance.setAttributes(namespace + ':' + attributeName, value);
    return this;
  }

  /**
   * 设置文本 <tag>text</tag>
   *
   * @param value 文本值
   * @returns 当前写入器
   */
  public writeElementValue(value: string): SoapXmlWriter {
    // value 必需有值才能setText，否则写入会报错跳出。
    if (value) {
      this.xmlInstance.setText(value);
    }
    return this;
  }

  /**
   * 设置文本 <tag>true | false</tag>
   *
   * @param value 布尔值
   * @returns SoapXmlWriter
   */
  public writeElementValueFromBoolean(value: boolean): SoapXmlWriter {
    this.xmlInstance.setText(booleanToString(value));
    return this;
  }
  /**
   * 设置文本 <tag>60</tag>
   *
   * @param value 数值
   * @returns SoapXmlWriter
   */
  public writeElementValueFromNumber(value: number): SoapXmlWriter {
    this.xmlInstance.setText(value.toString());
    return this;
  }
  /**
   * 设置文本 <tag>2024-03-28T05:30:00</tag>
   *
   * @param value Date对象
   * @returns SoapXmlWriter
   */
  public writeElementValueFromDate(value: Date): SoapXmlWriter {
    this.xmlInstance.setText(formatTimeToUtcString(value));
    return this;
  }

  /**
   * 获取xml字符串
   *
   * @returns xml字符串
   */
  public getXmlString(): string {
    if (this.depth || !this.arrayBuffer) {
      return '';
    }
    const textDecoder: util.TextDecoder = util.TextDecoder.create();
    return textDecoder.decodeWithStream(new Uint8Array(this.arrayBuffer)).trimEnd();
  }
}

/**
 * xml读取器，调用系统xml生成接口，读取指定元素的xml数据
 *
 * @since 2024-03-19
 */
export class SoapXmlReader {
  // 信封根节点元素名称
  private static readonly SOAP_ENVELOPE = XmlNamespace.SOAP + XmlNamespace.COLON + XmlElement.NAME_ENVELOPE;
  private jsObject: JsObjectElement;

  public constructor(strXml: string) {
    let convert = new convertxml.ConvertXML()
    this.jsObject = convert.convertToJSObject(strXml) as JsObjectElement;
  }

  /**
   * 获取解析的js对象
   *
   * @returns jsObject
   */
  public getJsObject(): JsObjectElement {
    return this.jsObject;
  }

  /**
   * 读取节点文本
   *
   * @param elementName 节点名称
   * @param element js对象
   * @returns 节点文本
   */
  public readElementValue(elementName: string, element: JsObjectElement): string {
    return element._elements?.find(el => el._type === XmlElement.NAME_TEXT && el._text === elementName)?._text ?? '';
  }

  /**
   * 查找节点对象
   *
   * @param elementName 节点名称
   * @param elements js对象数组
   * @returns 节点对象
   */
  public readElement(elementName: string, elements: JsObjectElement[]): JsObjectElement | undefined {
    return elements.find(el => el._type === XmlElement.NAME_ELEMENT && el._name === elementName);
  }

  /**
   * 返回最外层envelope元素
   *
   * @returns Js元素对象
   */
  public readEnvelopeElement(): JsObjectElement | undefined {
    return this.jsObject?._elements
    ?.find(el => el._type === XmlElement.NAME_ELEMENT && el._name === XmlElement.NAME_ENVELOPE);
  }
}

/**
 * Xml协议转换成JS对象的数据结构
 * 此结构为convertxml解析后的固定格式。 !!!请勿修改!!!
 */
export interface JsObjectElement {
  _type?: string;
  _name?: string;
  _text?: string;
  _attributes?: JsObjectAttribute;
  _elements?: JsObjectElement[];
}

export interface JsObjectAttribute {
  [key: string]: string;
}
