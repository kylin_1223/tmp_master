import { SoapXmlTag, TagType } from '../SoapXmlTag';


class ParentFolderIdTag extends SoapXmlTag {
  readonly tagType = TagType.message;
}

export const $ParentFolderId = () => new ParentFolderIdTag('ParentFolderId');