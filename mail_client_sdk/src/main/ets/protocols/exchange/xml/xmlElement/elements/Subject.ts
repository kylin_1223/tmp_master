import { SoapXmlTag, TagType } from '../SoapXmlTag';


class SubjectTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $Subject = () => new SubjectTag('Subject');