import { SoapXmlTag, TagType } from '../SoapXmlTag';


class CreateFolderTag extends SoapXmlTag {
  readonly tagType = TagType.message;
}

export const $CreateFolder = () => new CreateFolderTag('CreateFolder');