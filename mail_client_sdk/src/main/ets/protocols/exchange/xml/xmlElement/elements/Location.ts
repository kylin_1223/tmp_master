import { SoapXmlTag, TagType } from '../SoapXmlTag';


class LocationTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $Location = () => new LocationTag('Location');