import { SoapXmlTag, TagType } from '../SoapXmlTag';


class SavedItemFolderIdTag extends SoapXmlTag {
  readonly tagType = TagType.message
}

export const $SavedItemFolderId = () => new SavedItemFolderIdTag('SavedItemFolderId');