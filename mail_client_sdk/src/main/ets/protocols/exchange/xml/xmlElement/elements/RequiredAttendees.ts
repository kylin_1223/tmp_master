import { XmlElement } from '../../xml_element';
import { XmlNamespace } from '../../xml_namespace';
import { createSoapXmlTag, SoapXmlTag, TagType } from '../SoapXmlTag';
import { $Attendee } from './Attendee';
import { $EmailAddress } from './EmailAddress';
import { $MailBox } from './MailBox';


class RequiredAttendeesTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $RequiredAttendees = () => new RequiredAttendeesTag('RequiredAttendees');

const createAttendees = (emails: string[]) => {
  return emails.map(email =>
    $Attendee().appendChild(
      $MailBox().appendChild(
        $EmailAddress().setText(email)
      )
    )
  )
}

export const $RequiredAttendeesFromEmailArray = (emails: string[]) => {
  return $RequiredAttendees().appendChildren(
    createAttendees(emails)
  )
}

export const $OptionalAttendeesFromEmailArray = (emails: string[]) => {
  return createSoapXmlTag(XmlElement.NAME_OPTIONAL_ATTENDEES).appendChildren(
    createAttendees(emails)
  )
}
