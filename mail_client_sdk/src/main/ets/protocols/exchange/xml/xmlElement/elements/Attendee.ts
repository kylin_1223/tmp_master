import { SoapXmlTag, TagType } from '../SoapXmlTag';


class AttendeeTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $Attendee = () => new AttendeeTag('Attendee');