import { SoapXmlTag } from '../SoapXmlTag';


class ReminderIsSetTag extends SoapXmlTag { }

export const $ReminderIsSet = () => new ReminderIsSetTag('ReminderIsSet');