import { SoapXmlTag, TagType } from '../SoapXmlTag';


class MailBoxTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $MailBox = () => new MailBoxTag('MailBox');