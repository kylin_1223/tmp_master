import { SoapXmlTag, TagType } from '../SoapXmlTag';


class IsOnlineMeetingTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $IsOnlineMeeting = () => new IsOnlineMeetingTag('IsOnlineMeeting');