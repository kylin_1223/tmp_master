import { SoapXmlTag, TagType } from '../SoapXmlTag';


export class ItemsTag extends SoapXmlTag {

  // 标签类型
  readonly tagType = TagType.type

  public setId(s: string) {
    this.setAttribute('Id', s);
    return this;
  }

  public setChangeKey(s: string) {
    this.setAttribute('ChangeKey', s);
    return this;
  }
}


export const $Items = () => new ItemsTag('Items');