import { SoapXmlTag, TagType } from '../SoapXmlTag';


class EndTag extends SoapXmlTag {
  // 标签类型
  readonly tagType = TagType.type
}

export const $End = () => new EndTag('End');