import { SoapXmlTag, TagType } from '../SoapXmlTag';

export enum FreeBusyStatus {
  Free, Tentative, Busy, OOF, WorkingElsewhere, NoData
}

class LegacyFreeBusyStatusTag extends SoapXmlTag {
  readonly tagType = TagType.type

  setTextFromStatus(status: FreeBusyStatus) {
    this.setText(FreeBusyStatus[status]);
    return this;
  }
}

export const $LegacyFreeBusyStatus = () => new LegacyFreeBusyStatusTag('LegacyFreeBusyStatus');