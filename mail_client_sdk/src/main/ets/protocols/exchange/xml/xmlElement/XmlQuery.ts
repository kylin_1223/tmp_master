import { XmlTag } from '.';

export class XmlQuery<T extends XmlTag> {
  private tagInstance: T;
  constructor(tagInstance: T) {
    this.tagInstance = tagInstance;
  }

  /*
   * 传入的元素名，查第一个
   * */
  queryFirst(tagName: string): T | null {
    let result: null | T = null;
    this.tagInstance.traverseElementTree<T>((tag) => {
      if (tag.getTagName() === tagName) {
        result = tag;
        return false;
      }
      return true;
    });
    return result;
  }

  /*
   * 传入的元素名，查询所有
   * */
  queryAll(tagName: string) {
    let queryResult: T[] = [];
    this.tagInstance.traverseElementTree<T>((tagInstance) => {
      if (tagName === tagInstance.getTagName()) {
        queryResult.push(tagInstance);
      };
      return true;
    })
    return queryResult;
  }

  queryAllText(tagName: string) {
    return this.queryAll(tagName).map(tag => tag.getText());
  }

  /*
   * 传入元素名数组， 查询结果集
   * */
  multiQueryAll(tagNameArr: string[]) {
    let queryResult: Record<string, T[]> = {};
    if (!tagNameArr.length) { return queryResult; };
    for (const resultKey of tagNameArr) {
      queryResult[resultKey] = [];
    };
    this.tagInstance.traverseElementTree<T>((tagInstance) => {
      const tagName = tagInstance.getTagName();
      if (tagNameArr.includes(tagName)) {
        queryResult[tagName].push(tagInstance);
      }
      return true;
    })
    return queryResult;
  }
}