import { SoapXmlTag, TagType } from '../SoapXmlTag';


class IsMeetingTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}

export const $IsMeeting = () => new IsMeetingTag('IsMeeting');