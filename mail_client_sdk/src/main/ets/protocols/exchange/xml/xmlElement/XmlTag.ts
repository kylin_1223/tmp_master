import xml from '@ohos.xml';
import util from '@ohos.util';
import { XmlQuery } from '.';
import { booleanToString, formatTimeToUtcString } from '../../utils/transformToString';
import { SoapXmlWriter } from '../soap_xml';


// 计算报文长度的常量
const CRLFLength = 2;
const IndentLength = 2;
const AngleBracketsLengthSingle = getByteLengthFromString('</>');
const AngleBracketsLength = getByteLengthFromString('<></>');
const AttrExtLength = getByteLengthFromString(' =""');

// 返回 true继续遍历元素，false中止
type LoopFunction<T extends XmlTag> = (tagInstance: T, depth: number) => boolean;

const BUFFER_SIZE = 4096;
let tagId: number = 0;

export class XmlTag {

  private static arrayBuffer: ArrayBuffer = new ArrayBuffer(BUFFER_SIZE);
  protected static xmlSerializer: xml.XmlSerializer = new xml.XmlSerializer(XmlTag.arrayBuffer);

  static textDecoder: util.TextDecoder = util.TextDecoder.create();

  private tagName: string;
  private _tagId: number = 0;

  private parent: XmlTag | null = null;
  public children: XmlTag[] | null = null;
  private _attr: Map<string, string> | null = null;
  private text: string | null = null;
  private namespace = '';

  public addNamespace(ns: string) {
    this.namespace = ns;
    return this;
  }

  public getTagName() {
    return this.tagName;
  }

  public getTagNameWithNamespace() {
    if (this.namespace.length) {
      return this.namespace + ':' + this.tagName;
    }
    return this.tagName;
  }

  public getText(): string {
    return this.text || '';
  }

  public setText(val: string) {
    this.text = val;
    return this;
  }

  public setTextFromBoolean(val: boolean) {
    this.text = booleanToString(val);
    return this;
  }

  public setTextFromDate(date: Date) {
    this.text = formatTimeToUtcString(date);
    return this;
  }

  public setTextFromNumber(n: number) {
    this.text = n.toString();
    return this;
  }

  public getAttribute(key: string) {
    if (!this._attr) { return null; }
    if (this._attr.has(key)) {
      return this._attr.get(key);
    } else {
      return null
    }
  }

  public getAllAttribute() : [string, string][] | null {
    if (!this._attr) { return null; };
    return Array.from(this._attr.entries())
  }

  public setAttribute(key: string, value: string) {
    if (key) {
      if (!this._attr) {
        this._attr = new Map();
        this._attr.set(key, value);
      } else {
        this._attr.set(key, value);
      }
    } else {
      new Error('The key cannot be empty.')
    }
    return this;
  }

  constructor(tagName: string) {
    if (tagName.length) {
      this.tagName = tagName;
      this._tagId = tagId++;
    } else {
      throw new Error('The tagName cannot be empty.')
    }
  }

  static create(tagName: string) {
    if (tagName.length) {
      return new XmlTag(tagName);
    } else {
      throw new Error('The tagName cannot be empty.')
    }
  }

  public initQuery() {
    return new XmlQuery(this);
  }

  public hasChildren() {
    return Boolean(this.children !== null && this.children.length);
  }

  /*
   * 遍历元素树
   * loopFunction： 接收元素对象，返回ture继续遍历false终止。
   * */
  public traverseElementTree<T extends XmlTag>(loopFunction: LoopFunction<T>): void {
    let currentLevel: XmlTag[] = [this];
    let saveLevel: T[] = [];
    let depth = 0;

    let i = 0;
    while ( i < currentLevel.length ) {
      const tagInstance = currentLevel[i];
      const findNext = loopFunction(tagInstance as T, depth);
      if (!findNext) { return; };
      if (tagInstance.children && tagInstance.children.length) {
        saveLevel = saveLevel.concat(tagInstance.children as T[]);
      };
      i++;
      if (i === currentLevel.length) {
        currentLevel = saveLevel;
        saveLevel = [];
        i = 0;
        depth += 1;
      };
    };
  }

  /*
   * 计算xml报文长度
   * */
  public getXmlLength() {
    let len = 0;
    this.traverseElementTree((tag, depth) => {
      // 是否一行
      let isOneLine = false;
      const tagNameLen = getByteLengthFromString(tag.getTagNameWithNamespace());
      if (tag.children) {
        // 双标签有子元素
        len += (AngleBracketsLength + tagNameLen * 2);
      } else {
        isOneLine = true;
        if (tag.text) {
          // 双标签 有text
          len += (AngleBracketsLength + tagNameLen * 2);
          len += getByteLengthFromString(tag.text);
        } else {
          // 单标签
          len += (AngleBracketsLengthSingle + tagNameLen);
        }
      };
      const attrs = tag.getAllAttribute();
      if (attrs) {
        for (const attrInfo of attrs) {
          const attrKey = attrInfo[0];
          const attrValue = attrInfo[1];
          len += (getByteLengthFromString(attrKey) + getByteLengthFromString(attrValue) + AttrExtLength);
        }
      };
      // 计算换行符长度和空格缩进
      const CRLFAndIndentLength = CRLFLength + depth * IndentLength;
      len += (isOneLine ? CRLFAndIndentLength : CRLFAndIndentLength * 2)
      return true;
    });
    // 根节点闭合元素没有CRLF
    len -= CRLFLength;
    return len;
  }

  public toString() {
    // 计算xml长度, 调整buffer size大小
    const xmlLen = this.getXmlLength();
    console.log('Xml Length: ', xmlLen);
    if (xmlLen > BUFFER_SIZE) {
      this.resetSerializer(xmlLen + 128);
    };
    this._toString();
    let view: Uint8Array = new Uint8Array(XmlTag.arrayBuffer); // 使用Uint8Array读取arrayBuffer的数据
    let res: string = XmlTag.textDecoder.decodeWithStream(view); // 对view解码
    this.resetSerializer();
    return res.trim();
  }

  public write(serializer: SoapXmlWriter) {
    serializer.writeStartElement(this.namespace, this.tagName);
    if (this._attr) {
      for (const attr of this._attr.entries()) {
        const attrName = attr[0];
        const attrValue = attr[1];
        serializer.writeAttribute(attrName, attrValue);
      }
    }
    if (this.text) {
      serializer.writeElementValue(this.text);
    }
    if (this.children && this.children.length) {
      for (const tagInstance of this.children) {
        tagInstance.write(serializer);
      }
    }
    serializer.writeEndElement();
  }

  private resetSerializer(bufferSize?: number) {
    XmlTag.arrayBuffer = new ArrayBuffer(bufferSize ?? BUFFER_SIZE);
    XmlTag.xmlSerializer = new xml.XmlSerializer(XmlTag.arrayBuffer);
  }

  private _toString() {
    let thatSer: xml.XmlSerializer = XmlTag.xmlSerializer; // 基于Arraybuffer构造XmlSerializer对象
    thatSer.startElement(this.getTagNameWithNamespace());
    if (this._attr) {
      for (const attr of this._attr.entries()) {
        const attrName = attr[0];
        const attrValue = attr[1];
        thatSer.setAttributes(attrName, attrValue);
      }
    }
    if (this.text) {
      thatSer.setText(this.text);
    }
    if (this.children && this.children.length) {
      for (const tagInstance of this.children) {
        tagInstance._toString();
      }
    }
    thatSer.endElement();
  }

  public appendChild(child: XmlTag) {
    if (this.children) {
      this.children.push(child);
    } else {
      this.children = [child]
    }
    child.parent = this;
    return this;
  }

  public appendChildren(children: XmlTag[]) {
    if (children && children.length) {
      if (this.children) {
        for(const child of children) {
          this.children.push(child);
        }
      } else {
        this.children = children;
      }
    }
    return this;
  }
}

export function getByteLengthFromString(s: string) {
  let count = 0;
  const strLen = s.length;
  for(let i = 0; i < strLen; i++) {
    count += s.charCodeAt(i) <= 128 ? 1 : 2;
  }
  return count;
}

export const createXmlTag = (tagName: string): XmlTag => XmlTag.create(tagName);
