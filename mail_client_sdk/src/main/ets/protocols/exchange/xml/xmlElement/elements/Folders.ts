import { SoapXmlTag, TagType } from '../SoapXmlTag';


class FoldersTag extends SoapXmlTag {
  readonly tagType = TagType.message;
}

class FolderTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}
class CalendarFolderTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}
// FolderClass

class FolderClassTag extends SoapXmlTag {
  readonly tagType = TagType.type;
}


export const $Folder = () => new FolderTag('Folder');
export const $Folders = () => new FoldersTag('Folders');
export const $CalendarFolder = () => new CalendarFolderTag('CalendarFolder');
export const $FolderClass = () => new FolderClassTag('FolderClass');
