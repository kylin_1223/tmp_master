/*
 * Copyright 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from './mode_base';

export class DeleteItem extends ModelBase {
  private _deleteType: DeleteType = DeleteType.SoftDelete;
  private _sendMeetingCancellations?: SendCancellationsType;
  private _affectedTaskOccurrence?: AffectedTaskOccurrence;
  private _suppressReadReceipts: boolean = false;
  private _itemIds: UniteId[];

  public get deleteType() {
    return this._deleteType;
  }

  public set deleteType(deleteType) {
    this._deleteType = deleteType;
  }

  public get sendMeetingCancellations() {
    return this._sendMeetingCancellations;
  }

  public set sendMeetingCancellations(sendMeetingCancellations) {
    this._sendMeetingCancellations = sendMeetingCancellations;
  }

  public get affectedTaskOccurrence() {
    return this._affectedTaskOccurrence;
  }

  public set affectedTaskOccurrence(affectedTaskOccurrence) {
    this._affectedTaskOccurrence = affectedTaskOccurrence;
  }

  public get suppressReadReceipts() {
    return this._suppressReadReceipts;
  }

  public set suppressReadReceipts(suppressReadReceipts) {
    this._suppressReadReceipts = suppressReadReceipts;
  }

  public set itemIds(itemIds: UniteId[]) {
    this._itemIds = [...itemIds];
  }

  public get itemIds(): UniteId[] {
    return this._itemIds;
  }
}

export class DeleteItemResp extends ModelBase {
  private _responseMessages: DeleteItemResponseMessage[];

  public set responseMessages(value: DeleteItemResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): DeleteItemResponseMessage[] {
    return this._responseMessages;
  }
}

export class DeleteItemResponseMessage {
  private _responseCode: string;
  private _responseClass: string;
  private _messageText: string;
  private _descriptiveLinkKey: string;

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  public set responseClass(value: string) {
    this._responseClass = value;
  }

  public get responseClass(): string {
    return this._responseClass;
  }

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }

  public set descriptiveLinkKey(value: string) {
    this._descriptiveLinkKey = value;
  }

  public get descriptiveLinkKey(): string {
    return this._descriptiveLinkKey;
  }
}

export enum DeleteType {

  // 从存储区中永久删除项目
  HardDelete = 1,

  // 如果启用了垃圾站，项将移动到垃圾站
  SoftDelete = 2,

  // 将项目移动到"已删除邮件"文件夹中
  MoveToDeletedItems = 3
}


export enum SendCancellationsType {

  // 在不发送取消消息的情况下删除日历项目
  SendToNone = 1,

  // 日历项目将被删除，并且会向所有与会者发送取消邮件
  SendOnlyToAll = 2,

  // 日历项目将被删除，并且会向所有与会者发送取消邮件。 取消邮件的副本保存在"已发送项目"文件夹中
  SendToAllAndSaveCopy = 3
}

export enum AffectedTaskOccurrence {

  // 删除项目请求将删除主任务，因此删除与主任务关联的所有定期任务
  AllOccurrences = 1,

  // 删除项目请求仅删除任务的特定匹配项
  SpecifiedOccurrenceOnly = 2
}
