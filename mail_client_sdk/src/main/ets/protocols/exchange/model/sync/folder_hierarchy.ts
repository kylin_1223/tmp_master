/* Copyright © 2024 Coremail论客. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PropertySet } from '../../utils/common_enum';
import { ModelBase, UniteId } from '../mode_base';

/**
 * 同步文件夹请求数据模型
 * 用于构建请求报文
 *
 * @since 2024-03-22
 */
export class FolderHierarchy extends ModelBase {
  /**
   * 指定目录id，不传则表示查询所有文件夹结构
   */
  private _syncFolderId?: UniteId;
  private _propertySet?: PropertySet = PropertySet.Default;
  private _syncState?: string;

  public set syncFolderId(value: UniteId) {
    this._syncFolderId = value;
  }

  public get syncFolderId(): UniteId {
    return this._syncFolderId;
  }

  public set propertySet(value: PropertySet) {
    this._propertySet = value;
  }

  public get propertySet(): PropertySet {
    return this._propertySet;
  }

  public set syncState(value: string) {
    this._syncState = value;
  }

  public get syncState(): string {
    return this._syncState;
  }
}

/**
 * 用于标识用户或具有文件夹访问权限的用户
 *
 * @since 2024-03-22
 */
export class UserId {
  sID?: string;
  primarySmtpAddress?: string;
  displayName?: string;
  distinguishedUser?: string;
  externalUserIdentity?: string;
}

/**
 * 包含文件夹的权限集合
 *
 * @since 2024-03-22
 */
export class Permission {
  userId?: UserId;
  canCreateItems?: string;
  canCreateSubFolders?: string;
  isFolderOwner?: string;
  isFolderVisible?: string;
  isFolderContact?: string;
  editItems?: string;
  deleteItems?: string;
  readItems?: string;
  permissionLevel?: string;
}

/**
 * 用于处理响应内容中的文件夹项目
 *
 * @since 2024-03-24
 */
export class FolderItem {
  folderId: UniteId;
  parentFolderId?: UniteId;
  folderClass?: string;
  displayName?: string;
  totalCount?: string;
  childFolderCount?: string;
  effectiveRights: EffectiveRights;
  permissionSet: Permission[];
  unreadCount?: string;
  sourceId: string;
}

/**
 * 项目或文件夹的权限设置的客户端权限
 *
 * @since 2024-03-24
 */
export class EffectiveRights {
  createAssociated?: string;
  createContents?: string;
  createHierarchy?: string;
  delete?: string;
  modify?: string;
  read?: string;
  viewPrivateItems?: string;
}

/**
 * 需要同步的有变更的文件夹
 *
 * @since 2024-03-24
 */
export class FolderChange {
  changeType: string;
  folder: FolderItem;
}

/**
 * 同步文件夹响应数据模型
 * 用于解析响应报文
 *
 * @since 2024-03-22
 */
export class FolderHierarchyResp extends ModelBase {
  /**
   * 同步标识
   */
  private _syncState: string;

  /**
   * 是否包含要同步的最后一个项目
   */
  private _includesLastItemInRange: string;

  /**
   * 需要同步的有变更的文件夹
   */
  private _folderChanges: FolderChange[];

  public set syncState(value: string) {
    this._syncState = value;
  }

  public get syncState(): string {
    return this._syncState;
  }

  public set includesLastItemInRange(value: string) {
    this._includesLastItemInRange = value;
  }

  public get includesLastItemInRange(): string {
    return this._includesLastItemInRange;
  }

  public set folders(value: FolderChange[]) {
    this._folderChanges = value;
  }

  public get folders(): FolderChange[] {
    return this._folderChanges;
  }
}