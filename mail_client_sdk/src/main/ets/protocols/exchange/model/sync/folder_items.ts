/* Copyright © 2024 Coremail论客. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from '../mode_base';
import { PropertySet, SyncFolderItemsScope } from '../../utils/common_enum';
import {
  CompleteName,
  ConversationId,
  EmailAddress,
  FileAsMapping,
  Flag,
  ImAddress,
  PhoneNumber,
  PhysicalAddress
} from '../contact/get_contact';

/**
 * 同步项目请求数据模型
 * 用于构建请求报文
 *
 * @since 2024-03-22
 */
export class FolderItems extends ModelBase {
  private _syncFolderId: UniteId;
  private _propertySet?: PropertySet = PropertySet.Default;
  private _syncScope?: SyncFolderItemsScope = SyncFolderItemsScope.NormalItems;
  private _syncState?: string;

  public set syncFolderId(value: UniteId) {
    this._syncFolderId = value;
  }

  public get syncFolderId(): UniteId {
    return this._syncFolderId;
  }

  public set propertySet(value: PropertySet) {
    this._propertySet = value;
  }

  public get propertySet(): PropertySet {
    return this._propertySet;
  }

  private _ignoredItemIds?: UniteId[];

  public set ignoredItemIds(value: UniteId[]) {
    this._ignoredItemIds = value;
  }

  public get ignoredItemIds(): UniteId[] {
    return this._ignoredItemIds;
  }

  private _maxChangesReturned?: number = 10;

  public set maxChangesReturned(value: number) {
    this._maxChangesReturned = value;
  }

  public get maxChangesReturned(): number {
    return this._maxChangesReturned;
  }

  public set syncScope(value: SyncFolderItemsScope) {
    this._syncScope = value;
  }

  public get syncScope(): SyncFolderItemsScope {
    return this._syncScope;
  }

  public set syncState(value: string) {
    this._syncState = value;
  }

  public get syncState(): string {
    return this._syncState;
  }
}

/**
 * 同步文件夹响应数据模型
 * 用于解析响应报文
 *
 * @since 2024-03-22
 */
export class FolderItemsResp extends ModelBase {
  /**
   * 同步标识
   */
  private _syncState: string;

  /**
   * 是否包含要同步的最后一个项目
   */
  private _includesLastItemInRange: string;

  /**
   * 变化数据数组
   */
  private _itemChanges: ItemChange[] = [];

  public set syncState(value: string) {
    this._syncState = value;
  }

  public get syncState(): string {
    return this._syncState;
  }

  public set includesLastItemInRange(value: string) {
    this._includesLastItemInRange = value;
  }

  public get includesLastItemInRange(): string {
    return this._includesLastItemInRange;
  }

  public set itemChanges(value: ItemChange[]) {
    this._itemChanges = value;
  }

  public get itemChanges(): ItemChange[] {
    return this._itemChanges;
  }
}

/**
 * 变更项目公共内容抽象类
 *
 * @since 2024-03-24
 */
export abstract class ItemChange {
  changeType: string;
  itemId: UniteId;
  parentFolderId: UniteId;
  itemClass: string;
  subject: string;
  sensitivity: string;
  dateTimeReceived: string;
  size: string;
  importance: string;
  isSubmitted: string;
  isDraft: string;
  isFromMe: string;
  isResend: string;
  isUnmodified: string;
  dateTimeSent: string;
  dateTimeCreated: string;
  displayCc: string;
  displayTo: string;
  hasAttachments: string;
  culture: string;
  reminderIsSet: string;
  reminderMinutesBeforeStart: string;
  effectiveRights: EffectiveRights;
  lastModifiedName: string;
  lastModifiedTime: string;
  isAssociated: string;
  webClientReadFormQueryString: string;
  conversationId: ConversationId;
  flag: Flag;
  instanceKey: string;
}

/**
 * 联系人变更项目
 *
 * @since 2024-03-24
 */
export class ContactChange extends ItemChange {
  fileAs: string;
  fileAsMapping: FileAsMapping;
  displayName: string;
  displayNamePrefix: string;
  givenName: string;
  initials: string;
  middleName: string;
  nickname: string;
  completeName: CompleteName;

  yomiGivenName: string;
  yomiSurname: string;
  yomiCompanyName: string;
  personalHomePage: string;

  companyName: string;
  emailAddresses: EmailAddress[];
  physicalAddresses: PhysicalAddress[];
  phoneNumbers: PhoneNumber[];
  assistantName: string;
  birthday: string;
  businessHomePage: string;
  children: string;
  department: string;
  generation: string;
  imAddresses: ImAddress[];
  jobTitle: string;
  manager: string;
  mileage: string;
  officeLocation: string;
  postalAddressIndex: string;
  profession: string ;
  spouseName: string ;
  surname: string ;
  accountName: string;
  objectId: string;
  sourceId: string;
  weddingAnniversary: string;
}

/**
 * 邮件变更项目
 *
 * @since 2024-03-24
 */
export class MessageChange extends ItemChange {
  sender: Mailbox;
  isReadReceiptRequested: string;
  conversationIndex: string;
  conversationTopic: string;
  from: Mailbox;
  internetMessageId: string;
  isRead: string;
  references: string;
  receivedBy: Mailbox;
  receivedRepresenting: Mailbox;
}

/**
 * 会议取消变更项目
 *
 * @since 2024-03-24
 */
export class MeetingCancellationChange extends ItemChange {
  sender: Mailbox;
  isReadReceiptRequested: string;
  conversationIndex: string;
  conversationTopic: string;
  from: Mailbox;
  internetMessageId: string;
  isRead: string;
  isResponseRequested: string;
  receivedBy: Mailbox;
  receivedRepresenting: Mailbox;
  hasBeenProcessed: string;
  uID: string;
  dateTimeStamp: string;
  isOrganizer: string;
  start: string;
  end: string;
  location: string;
  calendarItemType: string;
}

/**
 * 会议邀请变更项目
 *
 * @since 2024-03-24
 */
export class MeetingRequestChange extends ItemChange {
  sender: Mailbox;
  isReadReceiptRequested: string;
  conversationIndex: string;
  conversationTopic: string;
  from: Mailbox;
  internetMessageId: string;
  isRead: string;
  isResponseRequested: string;
  receivedBy: Mailbox;
  receivedRepresenting: Mailbox;
  hasBeenProcessed: string;
  responseType: string;
  uID: string;
  dateTimeStamp: string;
  isOrganizer: string;
  meetingRequestType: string;
  intendedFreeBusyStatus: string;
  start: string;
  end: string;
  location: string;
  isMeeting: string;
  isCancelled: string;
  isRecurring: string;
  meetingRequestWasSent: string
  calendarItemType: string;
  organizer: Mailbox;
  duration: string;
  appointmentSequenceNumber: string;
  appointmentState: string;
  allowNewTimeProposal: string;
  isOnlineMeeting: string;
  startWallClock: string;
  endWallClock: string;
  startTimeZoneId: string;
  endTimeZoneId: string;
}

/**
 * 日历变更项目
 *
 * @since 2024-03-24
 */
export class CalendarChange extends ItemChange {
  uID: string;
  dateTimeStamp: string;
  start: string;
  end: string;
  isAllDayEvent: string;
  legacyFreeBusyStatus: string;
  location: string;
  isMeeting: string;
  isCancelled: string;
  isRecurring: string;
  meetingRequestWasSent: string;
  isResponseRequested: string;
  calendarItemType: string;
  myResponseType: string;
  organizer: Mailbox;
  duration: string;
  timeZone: string;
  appointmentSequenceNumber: string;
  appointmentState: string;
  isOnlineMeeting: string;
  allowNewTimeProposal: string;
  startWallClock: string;
  endWallClock: string;
  startTimeZoneId: string;
  endTimeZoneId: string;
  intendedFreeBusyStatus: string;
  joinOnlineMeetingUrl: string;
  isOrganizer: string;
}

/**
 * 标识启用了邮件的 Active Directory 对象。
 *
 * @since 2024-03-24
 */
export class Mailbox {
  name: string;
  emailAddress?: string;
  routingType?: string;
  mailboxType: string;
}

/**
 * 项目或文件夹的权限设置的客户端权限
 *
 * @since 2024-03-24
 */
export class EffectiveRights {
  createAssociated?: string;
  createContents?: string;
  createHierarchy?: string;
  delete?: string;
  modify?: string;
  read?: string;
  viewPrivateItems?: string;
}