/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DistinguishedPropertySetId, PropertyType } from '../utils/common_enum';
import { SoapXmlWriter, XmlAttribute, XmlElement, XmlNamespace, XmlWritable } from '../xml';

/**
 * 扩展字段URI
 */
export class ExtendedFieldURI implements XmlWritable {
  private _propertyType: PropertyType; // 属性标记的属性类型
  private _propertyId?: string; // 按扩展属性的调度 ID 标识扩展属性
  private _propertyName?: string; // 按扩展属性的名称标识扩展属性
  private _propertyTag?: string; // 标识没有标记的类型部分的属性标记。 PropertyTag 可以表示为十六进制整数或短整数
  private _propertySetId?: string; // 通过标识 GUID 来标识 MAPI 扩展属性集或命名空间
  private _distinguishedPropertySetId?: DistinguishedPropertySetId; // 定义扩展 MAPI 属性的已知属性集 ID

  public get propertyType(): PropertyType {
    return this._propertyType;
  }
  public set propertyType(val: PropertyType) {
    this._propertyType = val;
  }

  public get propertyId(): string {
    return this._propertyId;
  }
  public set propertyId(val: string) {
    this._propertyId = val;
  }

  public get propertyName(): string {
    return this._propertyName;
  }
  public set propertyName(val: string) {
    this._propertyName = val;
  }

  public get propertyTag(): string {
    return this._propertyTag;
  }
  public set propertyTag(val: string) {
    this._propertyTag = val;
  }

  public get propertySetId(): string {
    return this._propertySetId;
  }
  public set propertySetId(val: string) {
    this._propertySetId = val;
  }

  public get distinguishedPropertySetId(): DistinguishedPropertySetId {
    return this._distinguishedPropertySetId;
  }
  public set distinguishedPropertySetId(val: DistinguishedPropertySetId) {
    this._distinguishedPropertySetId = val;
  }

  /**
   * 写入ExtendedFieldURI的xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_EXTENDED_FIELD_URL);
    if (this.propertyType) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_PROPERTY_TYPE, PropertyType[this.propertyType]);
    }

    // 如果使用 PropertyTag 属性，则无法使用 DistinguishedPropertySetId、 PropertySetId、 PropertyName 和 PropertyId 属性
    if (this.propertyTag) {
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_PROPERTY_TAG, this.propertyTag);
    } else {
      if (this.propertyId) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_PROPERTY_ID, this.propertyId);
      } else if (this.propertyName) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_PROPERTY_NAME, this.propertyName);
      }

      // PropertyId 和 PropertyName 必须与 DistinguishedPropertySetId 或 PropertySetId 结合使用
      if (this.propertySetId) {
        soapXmlWriter.writeAttribute(XmlAttribute.NAME_PROPERTY_SET_ID, this.propertySetId);
      }
      if (this.distinguishedPropertySetId) {
        soapXmlWriter.writeAttribute(
          XmlAttribute.NAME_DISTINGUISHED_PROPERTY_SET_ID,
          DistinguishedPropertySetId[this._distinguishedPropertySetId]
        );
      }
    }
    soapXmlWriter.writeEndElement();
  }
}

/**
 * 扩展属性
 */
export class ExtendedProperty implements XmlWritable {
  private _extendedFieldURI: ExtendedFieldURI;
  private _values: string | string[] = '';

  public get extendedFieldURI(): ExtendedFieldURI {
    return this._extendedFieldURI;
  }
  public set extendedFieldURI(val: ExtendedFieldURI) {
    this._extendedFieldURI = val;
  }

  public get values(): string | string[] {
    return this._values;
  }
  public set values(val: string | string[]) {
    this._values = val;
  }

  /**
   * 写入扩展属性的xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    this.extendedFieldURI?.writeToXml(soapXmlWriter);
    if (Array.isArray(this.values)) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_VALUES);
      this.values.forEach(item => {
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_VALUE, item);
      })
      soapXmlWriter.writeEndElement();
    } else {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_VALUE, this.values);
    }
  }
}

/**
 * 标识字典的各个成员
 *
 * @since 2024-04-13
 */
export class IndexFiledURL implements XmlWritable{
  private _fieldURI: string; // 标识包含要返回的成员的字典
  private _fieldIndex: string; // 标识要返回的字典的成员

  /**
   * 设置标识包含要返回的成员的字典
   *
   * @param value
   */
  public set fieldURI(value: string) {
    this._fieldURI = value;
  }

  /**
   * 获取标识包含要返回的成员的字典
   *
   * @returns string 标识包含要返回的成员的字典
   */
  public get fieldURI(): string {
    return this._fieldURI;
  }

  /**
   * 设置标识要返回的字典的成员
   *
   * @param value
   */
  public set fieldIndex(value: string) {
    this._fieldIndex = value;
  }

  /**
   * 获取标识要返回的字典的成员
   *
   * @returns string 标识标识要返回的字典的成员
   */
  public get fieldIndex(): string {
    return this._fieldIndex;
  }


  /**
   * 写入扩展属性的xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_INDEXED_FIELD_URL);
    soapXmlWriter.writeAttribute(XmlAttribute.NAME_FIELD_URI, this.fieldURI ?? '');
    soapXmlWriter.writeAttribute(XmlAttribute.NAME_FIELD_INDEX, this.fieldIndex ?? '');
    soapXmlWriter.writeEndElement();
  }
}