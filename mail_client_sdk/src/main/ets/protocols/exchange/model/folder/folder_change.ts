/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UniteId } from '../mode_base';
import { DistinguishedFolderId, Folder } from './folder';
import { FieldURISchema } from '../../xml/xml_attribute';
import { ContactsFolder } from './contacts_folder';
import { SearchFolder } from './search_folder';
import { TasksFolder } from './tasks_folder';
import { CalendarFolder } from './calendar_folder';

/**
 * 文件夹更改model
 *
 * @since 2024-03-19
 */
export class FolderChange {
  private _folderId: UniteId;
  private _distinguishedFolderId: DistinguishedFolderId;
  private _folderFields: FolderField[];

  /**
   * 设置文件夹Id
   *
   * @param folderId
   */
  public set folderId(folderId: UniteId) {
    this._folderId = folderId;
  }

  /**
   * 获取文件夹Id
   *
   * @returns
   */
  public get folderId(): UniteId {
    return this._folderId;
  }

  /**
   * 设置系统默认文件夹Id
   *
   * @param distinguishedFolderId 系统默认文件夹Id
   */
  public set distinguishedFolderId(distinguishedFolderId: DistinguishedFolderId) {
    this._distinguishedFolderId = distinguishedFolderId;
  }

  /**
   * 获取系统默认文件夹Id
   *
   * @returns 系统默认文件夹Id
   */
  public get distinguishedFolderId(): DistinguishedFolderId {
    return this._distinguishedFolderId;
  }

  /**
   * 设置文件夹单个属性值的更新数据集合
   *
   * @param folderFields 文件夹单个属性值的更新数据集合
   */
  public set folderFields(folderFields: FolderField[]) {
    this._folderFields = [...folderFields];
  }

  /**
   * 获取文件夹单个属性值的更新数据集合
   *
   * @returns 文件夹单个属性值的更新数据集合
   */
  public get folderFields(): FolderField[] {
    return this._folderFields;
  }
}

/**
 * 更新文件夹操作类型枚举类
 */
export enum FolderOperateType {
  AppendToFolderField,
  SetFolderField,
  DeleteFolderField,
}

/**
 * 文件夹更新数据类型
 */
export type FolderField = {
  operateType: FolderOperateType;
  fieldURI?: FieldURISchema;
  indexedFieldURI?: IndexedFieldURI;
  extendedFieldURI?: ExtendedFieldURI;
  folder?: Folder;
  contactsFolder?: ContactsFolder;
  tasksFolder?: TasksFolder;
  searchFolder?: SearchFolder;
  calenderFolder?: CalendarFolder;
}

/**
 * IndexedFieldURI成员类型
 */
export type IndexedFieldURI = {
  fieldURI?: string,
  fieldIndex?: string,
}

/**
 * 扩展 MAPI 属性成员类型
 */
export type ExtendedFieldURI = {
  distinguishedPropertySetId?: string,
  propertySetId?: string,
  propertyTag?: string,
  propertyName?: string,
  propertyId?: string,
  propertyType?: string
}

