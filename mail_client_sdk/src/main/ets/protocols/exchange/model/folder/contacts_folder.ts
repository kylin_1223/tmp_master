/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { XmlElement, XmlNamespace } from '../../xml';
import { Folder } from './folder';
import { JsObjectElement, SoapXmlWriter } from '../../xml/soap_xml';

/**
 * 联系人文件夹
 *
 * @since 2024-03-19
 */
export class ContactsFolder extends Folder {
  /**
   * 获取联系人文件夹标签名称
   *
   * @returns 联系人文件夹标签名称
   */
  public get xmlElementName(): string {
    return XmlElement.NAME_CONTACTS_FOLDER;
  }

  /**
   * 生成联系人文件夹xml
   *
   * @param soapXmlWriter xml编写器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CONTACTS_FOLDER);
    // 设置文件名
    if (this._displayName) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_DISPLAY_NAME, this._displayName);
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 解析联系人文件夹xml
   *
   * @param jsObject js元素对象
   */
  public readFromXml(jsObject: JsObjectElement[]): void {
    super.readFromXml(jsObject);
  }
}