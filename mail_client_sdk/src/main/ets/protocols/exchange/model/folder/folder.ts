/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { UniteId } from '../mode_base';
import { FolderName, XmlAttribute } from '../../xml/xml_attribute';
import { SoapXmlWriter, JsObjectElement } from '../../xml/soap_xml';
import { XmlElement, XmlNamespace, XmlReadable, XmlWritable } from '../../xml/index';
import { TasksFolder } from './tasks_folder';
import { CalendarFolder } from './calendar_folder';
import { ContactsFolder } from './contacts_folder';
import { DistinguishedPropertySetId, MailboxType, PropertyType } from '../../utils/common_enum';
import { Mailbox } from '../mail/item_message';

/**
 * 文件夹父类
 *
 * @since 2024-03-19
 */
export class Folder implements XmlWritable, XmlReadable {
  protected _folderId: UniteId;
  protected _parentFolderId: UniteId;
  protected _folderClass: FolderIPFType;
  protected _displayName: string;
  protected _totalCount: string;
  protected _childFolderCount: string;
  protected _extendedProperty: ExtendedProperty;
  protected _managedFolderInformation: ManagedFolderInformation;
  protected _unreadCount: string;
  protected _permissionSet: PermissionSet;
  protected _effectiveRights: EffectiveRights;

  /**
   * 获取文件夹标签名称
   *
   * @returns 文件夹标签名称
   */
  public get xmlElementName(): string {
    return XmlElement.NAME_FOLDER;
  }

  /**
   * 获取文件夹Id
   *
   * @returns
   */
  public get folderId(): UniteId {
    return this._folderId;
  }

  /**
   * 设置文件夹Id
   *
   * @param folderId 文件夹Id
   */
  public set folderId(folderId) {
    this._folderId = folderId;
  }

  /**
   * 获取父级文件夹Id
   *
   * @returns 父级文件夹Id
   */
  public get parentFolderId(): UniteId {
    return this._parentFolderId;
  }

  /**
   * 获取文件夹类型
   *
   * @returns 文件夹类型
   */
  public get folderClass(): FolderIPFType {
    return this._folderClass;
  }

  /**
   * 设置文件夹类型
   *
   * @param folderClass 文件夹类型
   */
  public set folderClass(folderClass: FolderIPFType) {
    this._folderClass = folderClass;
  }

  /**
   * 获取文件夹名称
   *
   * @returns 文件夹名称
   */
  public get displayName(): string {
    return this._displayName;
  }

  /**
   * 设置文件夹名称
   *
   * @param displayName 文件夹名称
   */
  public set displayName(displayName: string) {
    this._displayName = displayName;
  }

  /**
   * 获取文件夹内的项目总数
   *
   * @returns 文件夹内的项目总数
   */
  public get totalCount(): string {
    return this._totalCount;
  }

  /**
   * 获取文件夹中包含的子文件夹的数量
   *
   * @returns 子文件夹的数量
   */
  public get childFolderCount(): string {
    return this._childFolderCount;
  }

  /**
   * 获取文件夹的扩展属性
   *
   * @returns 文件夹的扩展属性
   */
  public get extendedProperty(): ExtendedProperty {
    return this._extendedProperty;
  }

  /**
   * 获取托管文件夹信息
   *
   * @returns 托管文件夹信息
   */
  public get managedFolderInformation(): ManagedFolderInformation {
    return this._managedFolderInformation;
  }

  /**
   * 获取文件夹内未读项目的计数
   *
   * @returns 未读项目的计数
   */
  public get unreadCount(): string {
    return this._unreadCount;
  }

  /**
   * 获取文件夹所有的配置权限
   *
   * @returns 文件夹所有的配置权限
   */
  public get permissionSet(): PermissionSet {
    return this._permissionSet;
  }

  /**
   * 获取文件夹的客户端权限
   *
   * @returns 客户端权限
   */
  public get effectiveRights(): EffectiveRights {
    return this._effectiveRights;
  }

  /**
   * 生成文件夹xml
   *
   * @param soapXmlWriter xml编写器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER);
    // 设置文件名
    if (this._displayName) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_DISPLAY_NAME, this._displayName);
    }
    soapXmlWriter.writeEndElement();
  }

  /**
   * 解析文件夹xml
   *
   * @param jsObject js元素对象
   */
  public readFromXml(jsObject: JsObjectElement[]): void {
    let folderInfo = jsObject[0]?._elements;
    if (!folderInfo.length) {
      throw new Error('element list does not exist.');
    }
    for (let i = 0; i < folderInfo.length; i++) {
      switch (folderInfo[i]?._name) {
        case XmlElement.NAME_FOLDER_ID:
          this.folderId = {
            id: folderInfo[i]?._attributes?.Id,
            changeKey: folderInfo[i]?._attributes?.ChangeKey,
          };
          break;
        case XmlElement.NAME_PARENT_FOLDER_ID:
          this._parentFolderId = {
            id: folderInfo[i]?._attributes?.Id,
            changeKey: folderInfo[i]?._attributes?.ChangeKey,
          };
          break;
        case XmlElement.NAME_FOLDER_CLASS:
          this._folderClass = FolderIPFType[folderInfo[i]?._elements[0]?._text];
          break;
        case XmlElement.NAME_DISPLAY_NAME:
          this._displayName = folderInfo[i]?._elements[0]?._text;
          break;
        case XmlElement.NAME_TOTAL_COUNT:
          this._totalCount = folderInfo[i]?._elements[0]?._text;
          break;
        case XmlElement.NAME_CHILD_FOLDER_COUNT:
          this._unreadCount = folderInfo[i]?._elements[0]?._text;
          break;
        case XmlElement.NAME_UNREAD_COUNT:
          this._childFolderCount = folderInfo[i]?._elements[0]?._text;
          break;
        default:
          break;
      }
    }
  }
}

/**
 * 文件夹Id集合
 *
 * @since 2024-03-19
 */
export class FolderIds {
  private _folderId: UniteId;
  private _distinguishedFolderId: DistinguishedFolderId;

  /**
   * 设置文件夹Id
   *
   * @param folderId 文件夹Id
   */
  public set folderId(folderId: UniteId) {
    this._folderId = folderId;
  }

  /**
   * 获取文件夹Id
   *
   * @returns 文件夹Id
   */
  public get folderId(): UniteId {
    return this._folderId;
  }

  /**
   * 设置系统文件夹Id
   *
   * @param distinguishedFolderId 系统文件夹Id
   */
  public set distinguishedFolderId(distinguishedFolderId: DistinguishedFolderId) {
    this._distinguishedFolderId = distinguishedFolderId;
  }

  /**
   * 获取系统文件夹Id
   *
   * @returns 系统文件夹Id
   */
  public get distinguishedFolderId(): DistinguishedFolderId {
    return this._distinguishedFolderId;
  }
}

/**
 * 文件夹Id
 *
 * @since 2024-03-19
 */
export class FolderId implements XmlWritable {
  private _folderId: UniteId | DistinguishedFolderId;

  /**
   * 设置文件夹Id
   *
   * @param folderId 文件夹Id
   */
  public set folderId(folderId: UniteId | DistinguishedFolderId) {
    this._folderId = folderId;
  }

  /**
   * 获取文件夹Id
   *
   * @returns 文件夹Id
   */
  public get folderId(): UniteId | DistinguishedFolderId {
    return this._folderId;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    if (this.folderId instanceof DistinguishedFolderId) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_DISTINGUISHED_FOLDER_ID);
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_ID, this._folderId.id);
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_CHANGE_KEY, this._folderId.changeKey ?? '');
      if (this.folderId.mailbox) {
        this.folderId.mailbox.writeToXml(soapXmlWriter);
      }
      soapXmlWriter.writeEndElement();
    }
    if ((this.folderId as UniteId).id && (this.folderId as UniteId).changeKey) {
      soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FOLDER_ID);
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_ID, this._folderId.id);
      soapXmlWriter.writeAttribute(XmlAttribute.NAME_CHANGE_KEY, this._folderId.changeKey ?? '');
      soapXmlWriter.writeEndElement();
    }
  }
}

/**
 * 系统默认文件夹
 *
 * @since 2024-03-19
 */
export class DistinguishedFolderId {
  _id: FolderName;
  _changeKey?: string;
  mailbox?: Mailbox;

  /**
   * 设置id
   *
   * @param id 系统folder唯一标识
   */
  public set id(id: FolderName) {
    this._id = id;
  }

  /**
   * 获取id
   *
   * @returns 系统folder唯一标识
   */
  public get id(): FolderName {
    return this._id;
  }

  /**
   * 获取更新版本标识
   *
   * @param changeKey 更新版本标识
   */
  public set changeKey(changeKey: string) {
    this._changeKey = changeKey;
  }

  /**
   * 获取更新版本标识
   *
   * @returns 更新版本标识
   */
  public get changeKey(): string {
    return this._changeKey;
  }
}

/**
 * 响应数据类
 *
 * @since 2024-03-19
 */
export class BaseResponseMessage {
  responseCode: string;
  responseClass: string;
  messageText: string;
}

/**
 * 文件夹响应数据类
 *
 * @since 2024-03-19
 */
export class FolderResponse extends BaseResponseMessage {
  folder: Folder;
  contactsFolder: ContactsFolder;
  tasksFolder: TasksFolder;
  calendarFolder: CalendarFolder;
}

/**
 * 文件夹操作共用解析xml数据方法
 *
 * @param responseMessages 响应数据
 * @returns 解析对象
 */
export function parseResponseMassage(responseMessages: JsObjectElement[]): FolderResponse[] {
  let responses: FolderResponse[] = [];
  if (!responseMessages.length) {
    throw new Error('element list does not exist.');
  }
  responseMessages.forEach(item => {
    let response: FolderResponse = new FolderResponse();
    let responseMessage = item?._elements;
    response.responseClass = item?._attributes?.ResponseClass;
    let length = responseMessage.length;
    for (let i = 0; i < length; i++) {
      let elementsName = responseMessage[i]?._name;
      switch (elementsName) {
        case XmlElement.NAME_RESPONSE_CODE:
          response.responseCode = responseMessage[i]?._elements[0]?._text;
          break;
        case XmlElement.NAME_MESSAGE_TEXT:
          response.messageText = responseMessage[i]?._elements[0]?._text;
          break;
        case XmlElement.NAME_FOLDERS:
          if (responseMessage[i]?._elements && response?.responseClass === 'Success') {
            let changeElements = responseMessage[i]?._elements[0];
            let folder = new Folder();
            folder.folderId = {
              id: changeElements._elements[0]?._attributes?.Id,
              changeKey: changeElements?._elements[0]?._attributes?.ChangeKey,
            };
            response.folder = folder;
          }
          break;
        default:
          break;
      }
    }
    responses.push(response);
  });
  return responses;
}

/**
 * 文件夹的扩展属性
 */
export interface ExtendedProperty {
  extendedFieldURI: ExtendedFieldURI;
  values: string[];
  value: string;
}

/**
 * 扩展MAPI属性
 */
export interface ExtendedFieldURI {
  distinguishedPropertySetId?: DistinguishedPropertySetId;
  propertySetId?: string;
  propertyTag: string;
  propertyName: string;
  propertyId: string;
  propertyType: PropertyType;
}

/**
 * 托管自定义文件夹的信息
 */
export interface ManagedFolderInformation {
  canDelete: string;
  canRenameOrMove: string;
  mustDisplayComment: string;
  hasQuota: string;
  isManagedFoldersRoot: string;
  managedFolderId: string;
  comment: string;
  storageQuota: string;
  folderSize: string;
  homePage: string;
}

/**
 * 文件夹所有的配置权限
 */
export interface PermissionSet {
  permissions: Permissions[];
  unknownEntries: UnknownEntries[];
}

/**
 * 文件夹权限的集合
 */
export interface Permissions {
  permission: string;
}

/**
 * 无法根据 Active Directory 目录服务解析的未知条目数组
 */
export interface UnknownEntries {
  unknownEntry: string;
}

/**
 * 基于项目或文件夹的权限设置的客户端权限
 */
export interface EffectiveRights {
  createAssociated: string;
  createContents: string;
  createHierarchy: string;
  delete: string;
  modify: string;
  read: string;
  viewPrivateItems: string;
}

/**
 * FolderClass文本值
 */
export enum FolderIPFType {
  'IPF.NOTE' = 'IPF.NOTE', // 搜索文件夹和普通文件夹
  'IPF.Appointment' = 'IPF.Appointment', // 约会和会议
  'IPF.Contact' = 'IPF.Contact', // 联系人和通讯组列表
  'IPF.Task' = 'IPF.Task' // 工作项
}