/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase, UniteId } from '../mode_base';
import { Folder, FolderResponse } from './folder';
import { FolderName } from '../../xml/xml_attribute';

/**
 * 新建文件夹model
 *
 * @since 2024-03-19
 */
export class CreateFolder extends ModelBase {
  private _folders: Folder[];
  private _distinguishedFolderId: FolderName;
  private _parentFolderId: UniteId;

  /**
   * 设置新建文件夹集合
   *
   * @param folders 文件夹集合
   */
  public set folders(folders: Folder[]) {
    this._folders = folders;
  }

  /**
   * 获取文件夹集合
   *
   * @returns 文件夹集合
   */
  public get folders(): Folder[] {
    return this._folders;
  }

  /**
   * 设置系统默认文件夹Id
   *
   * @param distinguishedFolderId 系统默认文件夹Id
   */
  public set distinguishedFolderId(distinguishedFolderId: FolderName) {
    this._distinguishedFolderId = distinguishedFolderId;
  }

  /**
   * 获取系统默认文件夹Id
   *
   * @returns 系统默认文件夹Id
   */
  public get distinguishedFolderId(): FolderName {
    return this._distinguishedFolderId;
  }

  /**
   * 设置父级文件夹Id
   *
   * @param folderId 父级文件夹Id
   */
  public set parentFolderId(folderId: UniteId) {
    this._parentFolderId = folderId;
  }

  /**
   * 获取父级文件夹Id
   *
   * @returns 父级文件夹Id
   */
  public get parentFolderId(): UniteId {
    return this._parentFolderId;
  }
}

/**
 * 新建文件夹响应数据model
 *
 * @since 2024-03-19
 */
export class CreateFolderResp extends ModelBase {
  private _responseMessages: FolderResponse[];

  /**
   * 设置响应数据
   *
   * @param value 响应数据
   */
  public set responseMessages(value: FolderResponse[]) {
    this._responseMessages = value;
  }

  /**
   * 获取响应数据
   *
   * @returns 响应数据
   */
  public get responseMessages(): FolderResponse[] {
    return this._responseMessages;
  }
}


