/* Copyright © 2023 - 2024 Coremail论客.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from './mode_base';

/**
 * 自动发现，xml数据模板类
 */
export class AutoDiscoverMessage extends ModelBase {
  private userSettingMessage: UserSettingMessage;

  public setUserSettingMessage(message: UserSettingMessage): void {
    this.userSettingMessage = message;
  }

  public getUserSettingMessage(): UserSettingMessage {
    return this.userSettingMessage;
  }

}

export class UserSettingMessageResp extends ModelBase {
  private _ewsEndpoint: string;

  public set ewsEndpoint(value: string) {
    this._ewsEndpoint = value;
  }

  public get ewsEndpoint(): string {
    return this._ewsEndpoint;
  }
}

/**
 * 自动发现，接口类型
 */
export type UserSettingMessage = {
  emailAddress: string;
  requestedServerVersion: string;
  action: string;
  to: string;
  setting: string;
}