/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ResponseMessage } from '../response/response_base';
import { FieldURISchema } from '../xml/xml_attribute';
import { Message } from './mail/item_message';
import { ModelBase, UniteId } from './mode_base';
import { ExtendedFieldURI, IndexFiledURL } from '../model/extended_property';

/**
 * getItem请求Model类
 *
 * @since 2024-04-08
 */
export class GetItem extends ModelBase {
  private _baseShape: string; // 标识在项目或文件夹响应中返回的属性的基本配置
  private _includeMimeContent: string; //指定是否在响应中返回项目的多用途 Internet 邮件扩展 (MIME) 内容
  private _itemIds: UniteId[]; // id数组
  private _fieldURI: FieldURISchema; // 元素按 URI 标识经常引用的属性
  private _indexFiledURL: IndexFiledURL; // 标识字典的各个成员
  private _extendedFieldURI: ExtendedFieldURI; // 扩展字段URI

  /**
   * 设置id数组
   *
   * @param value id数组
   */
  public set itemIds(value: UniteId[]) {
    this._itemIds = value;
  }

  /**
   * 获取id数组
   *
   * @returns UniteId[] id数组
   */
  public get itemIds(): UniteId[] {
    return this._itemIds;
  }

  /**
   * 设置元素按 URI 标识经常引用的属性
   *
   * @param value 元素按 URI 标识经常引用的属性
   */
  public set fieldURI(value: FieldURISchema) {
    this._fieldURI = value;
  }

  /**
   * 获取FieldURISchema
   *
   * @returns FieldURISchema 元素按 URI 标识经常引用的属性
   */
  public get fieldURI(): FieldURISchema {
    return this._fieldURI;
  }

  /**
   * 设置标识在项目或文件夹响应中返回的属性的基本配置
   *
   * @param value 标识在项目或文件夹响应中返回的属性的基本配置
   */
  public set baseShape(value: string) {
    this._baseShape = value;
  }

  /**
   * 获取标识在项目或文件夹响应中返回的属性的基本配置
   *
   * @returns string 标识在项目或文件夹响应中返回的属性的基本配置
   */
  public get baseShape(): string {
    return this._baseShape;
  }

  /**
   * 设置指定是否在响应中返回项目的多用途 Internet 邮件扩展 (MIME) 内容
   *
   * @param value 指定是否在响应中返回项目的多用途 Internet 邮件扩展 (MIME) 内容
   */
  public set includeMimeContent(value: string) {
    this._includeMimeContent = value;
  }

  /**
   * 获取指定是否在响应中返回项目的多用途 Internet 邮件扩展 (MIME) 内容
   *
   * @returns string 指定是否在响应中返回项目的多用途 Internet 邮件扩展 (MIME) 内容
   */
  public get includeMimeContent(): string {
    return this._includeMimeContent;
  }

  /**
   * 设置
   *
   * @param value
   */
  public set indexFiledURL(value: IndexFiledURL) {
    this._indexFiledURL = value;
  }

  /**
   * 获取标识字典的各个成员
   *
   * @returns IndexFiledURL 标识字典的各个成员
   */
  public get indexFiledURL(): IndexFiledURL {
    return this._indexFiledURL;
  }

  /**
   * 设置标识字典的各个成员
   *
   * @param value 标识字典的各个成员
   */
  public set extendedFieldURI(value: ExtendedFieldURI) {
    this._extendedFieldURI = value;
  }

  /**
   * 获取扩展字段URI
   *
   * @returns ExtendedFieldURI  扩展字段URI
   */
  public get extendedFieldURI(): ExtendedFieldURI {
    return this._extendedFieldURI;
  }
}

/**
 * getItem-message返回Model类
 *
 * @since 2024-04-08
 */
export class GetItemMessageResponseModel extends ModelBase {
  private _responseMessages: ResponseMessage<Message>[]; //item返回结构数组

  /**
   * 设置item返回结构数组
   *
   * @param value item返回结构数组
   */
  public set responseMessages(value: ResponseMessage<Message>[]) {
    this._responseMessages = value;
  }

  /**
   * 获取item返回结构数组
   *
   * @returns GetItemResponseMessage[] item返回结构数组
   */
  public get responseMessages(): ResponseMessage<Message>[] {
    return this._responseMessages;
  }
}