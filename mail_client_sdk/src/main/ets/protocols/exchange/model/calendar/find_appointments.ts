/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderName } from '../../xml/xml_attribute';
import { ModelBase, UniteId } from '../mode_base';

export class AppointmentMessage extends ModelBase {
  public folderId: FolderName | string;
  public calendarView: CalendarView;

  get FolderId(): FolderName | string {
    return this.folderId;
  }

  set FolderId(value: FolderName | string) {
    this.folderId = value;
  }

  get CalendarView(): CalendarView {
    return this.calendarView;
  }

  set CalendarView(value: CalendarView) {
    this.calendarView = value;
  }
}

export class CalendarView {
  private traversal: ItemTraversal = ItemTraversal.Shallow;
  private maxItemsReturned: number = null;
  private startDate: DateTime = null;
  private endDate: DateTime = null;

  constructor(startDate?: DateTime, endDate?: DateTime, maxItemsReturned: number = null) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.maxItemsReturned = maxItemsReturned;
  }

  /**
   * Gets or sets the start date.
   */
  get StartDate(): DateTime {
    return this.startDate;
  }

  set StartDate(value: DateTime) {
    this.startDate = value;
  }

  /**
   * Gets or sets the end date.
   */
  get EndDate(): DateTime {
    return this.endDate;
  }

  set EndDate(value: DateTime) {
    this.endDate = value;
  }

  public getMaxItemsReturned(): number {
    return this.maxItemsReturned;
  }

  public setMaxItemsReturned(value: number) {
    if (value !== null && value <= 0) {
      throw new Error('value is less than 0');
    }
    this.maxItemsReturned = value;
  }

  get Traversal(): ItemTraversal {
    return this.traversal;
  }

  set Traversal(value: ItemTraversal) {
    this.traversal = value;
  }
}

export class DateTime {
  private readonly date: Date;

  constructor(date: Date) {
    this.date = date;
  }

  /**
   * 将日期格式化为 'YYYY-MM-DDTHH:mm:ssZ' 样式的字符串
   */
  public format(): string {
    const year = this.date.getUTCFullYear();
    const month = this.pad(this.date.getUTCMonth() + 1);
    const day = this.pad(this.date.getUTCDate());
    const hours = this.pad(this.date.getUTCHours());
    const minutes = this.pad(this.date.getUTCMinutes());
    const seconds = this.pad(this.date.getUTCSeconds());

    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}Z`;
  }

  /**
   * 辅助函数，用于确保日期和时间的单个部分始终具有两位数字
   */
  private pad(number: number): string {
    return number < 10 ? `0${number}` : number.toString();
  }
}

export enum ItemTraversal {

  Shallow = 0,

  SoftDeleted = 1,

  Associated = 2
}

export class FindItemsResults extends ModelBase {
  private totalCount: number = 0;
  private items: AppointmentItem[] = [];

  get TotalCount(): number {
    return this.totalCount;
  }

  set TotalCount(value: number) {
    this.totalCount = value;
  }

  get Items(): AppointmentItem[] {
    return this.items;
  }

  // 添加一个新的 AppointmentItem 到 items 数组
  public addItem(item: AppointmentItem): void {
    this.items.push(item);
    // 在这里自动更新 totalCount
    this.totalCount = this.items.length;
  }

  // 添加多个 AppointmentItem 到 items 数组
  public addItems(itemsToAdd: AppointmentItem[]): void {
    this.items.push(...itemsToAdd);
    // 在这里自动更新 totalCount
    this.totalCount = this.items.length;
  }
}


export class AppointmentItem extends ModelBase {
  private appointmentItemId: UniteId;
  private appointmentSubject: string;

  get AppointmentItemId(): UniteId {
    return this.appointmentItemId;
  }

  set AppointmentItemId(value: UniteId) {
    this.appointmentItemId = value;
  }

  get AppointmentSubject(): string {
    return this.appointmentSubject;
  }

  set AppointmentSubject(value: string) {
    this.appointmentSubject = value;
  }
}