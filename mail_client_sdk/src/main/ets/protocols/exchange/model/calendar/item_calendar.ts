/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { XmlAttribute, XmlElement, XmlNamespace, XmlWritable } from '../../xml/index';
import { SoapXmlWriter } from '../../xml/soap_xml';
import { FieldURISchema } from '../../xml/xml_attribute';
import { UniteId } from '../mode_base';
import {
  Attachments,
  Attendee,
  Body,
  EffectiveRights,
  ExtendedProperty,
  FlagStatus,
  InternetMessageHeader,
  Mailbox,
  Occurrence,
  Organizer,
  Recurrence,
  ResponseObjects
} from './calendar_response_types';

/**
 * 日历项详细数据
 */
export class Calendar implements XmlWritable {
  fieldUri: FieldURISchema;
  itemId: UniteId;
  parentFolderId: UniteId;
  itemClass: string;
  subject: string;
  sensitivity: string;
  body: Body;
  dateTimeReceived: string;
  size: number;
  categories: string[];
  importance: string;
  isSubmitted: boolean;
  isDraft: boolean;
  isFromMe: boolean;
  isResend: boolean;
  isUnmodified: boolean;
  dateTimeSent: string;
  dateTimeCreated: string;
  reminderDueBy: string;
  reminderIsSet: boolean;
  reminderNextTime: string;
  reminderMinutesBeforeStart: number;
  hasAttachments: boolean;
  culture: string;
  effectiveRights: EffectiveRights;
  lastModifiedName: string;
  lastModifiedTime: string;
  isAssociated: boolean;
  webClientReadFormQueryString: string;
  conversationId: UniteId;
  flag: FlagStatus;
  uid: string;
  dateTimeStamp: string;
  start: string;
  end: string;
  isAllDayEvent: boolean;
  legacyFreeBusyStatus: string;
  location: string;
  isMeeting: boolean;
  isCancelled: boolean;
  isRecurring: boolean;
  meetingRequestWasSent: boolean;
  isResponseRequested: boolean;
  calendarItemType: string;
  myResponseType: string;
  organizer: Organizer;
  duration: string;
  timeZone: string;
  appointmentSequenceNumber: number;
  appointmentState: number;
  isOnlineMeeting: boolean;
  startWallClock: string;
  endWallClock: string;
  startTimeZoneId: string;
  endTimeZoneId: string;
  intendedFreeBusyStatus: string;
  isOrganizer: boolean;
  mimeContent?: string;
  recurrence?: Recurrence;
  timeZoneName?: string;
  requiredAttendees?: Attendee[];
  optionalAttendees?: Attendee[];
  resources?: Attendee[];
  attachments?: Attachments;
  inReplyTo?: string;
  internetMessageHeaders?: InternetMessageHeader[];
  responseObjects?: ResponseObjects;
  displayCc?: string;
  displayTo?: string;
  extendedProperty?: ExtendedProperty;
  originalStart?: string;
  when?: string;
  conflictingMeetingCount?: number;
  adjacentMeetingCount?: number;
  conflictingMeetings?: Calendar;
  adjacentMeetings?: Calendar;
  appointmentReplyTime?: string;
  firstOccurrence?: Occurrence;
  lastOccurrence?: Occurrence;
  modifiedOccurrences?: Occurrence[];
  deletedOccurrences?: Occurrence[];
  meetingTimeZone?: string;
  startTimeZone?: string;
  endTimeZone?: string;
  conferenceType?: number;
  allowNewTimeProposal?: boolean;
  meetingWorkspaceUrl?: string;
  netShowUrl?: string;
  webClientEditFormQueryString?: string;
  uniqueBody?: string;

  writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_CALENDAR_ITEM);
    switch (this.fieldUri) {
      case FieldURISchema.Subject:
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_SUBJECT, this.subject);
        break;
      case FieldURISchema.Location:
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_LOCATION, this.location)
        break;
      case FieldURISchema.RequiredAttendees:
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_REQUIRED_ATTENDEES);
        this.requiredAttendees.forEach(mailElement => {
          soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ATTENDEE);
          this.writeMailbox(mailElement.mailbox, soapXmlWriter);
          soapXmlWriter.writeEndElement();
        });
        soapXmlWriter.writeEndElement();
        break;
      case FieldURISchema.Start:
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_START, this.start);
        break;
      case FieldURISchema.End:
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_END, this.end);
        break;
      case FieldURISchema.Resources:
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_RESOURCES);
        this.resources.forEach(mailElement => {
          soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ATTENDEE);
          this.writeMailbox(mailElement.mailbox, soapXmlWriter);
          soapXmlWriter.writeEndElement();
        });
        soapXmlWriter.writeEndElement();
        break;
      case FieldURISchema.Recurrence:
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_RECURRENCE);
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_WEEKLY_RECURRENCE);
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_INTERVAL, String(this.recurrence.weeklyRecurrence.interval));
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_DAYS_OF_WEEK, this.recurrence.weeklyRecurrence.daysOfWeek);
        soapXmlWriter.writeEndElement();
        soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_END_DATE_RECURRENCE);
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_START_DATE, String(this.recurrence.endDateRecurrence.startDate));
        soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_END_DATE, this.recurrence.endDateRecurrence.endDate);
        soapXmlWriter.writeEndElement();
        soapXmlWriter.writeEndElement();
        break;
      case FieldURISchema.MeetingTimeZone:
        soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_MEETING_TIME_ZONE, new Map([
          [XmlAttribute.NAME_TIME_ZONE_NAME, this.timeZoneName]
        ]));
        break;
    }
    soapXmlWriter.writeEndElement();
  }

  private writeMailbox(mailbox: Mailbox, soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_MAILBOX);
    if (mailbox.name) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_NAME, mailbox.name);
    }
    if (mailbox.emailAddress) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_EMAIL_ADDRESS, mailbox.emailAddress);
    }
    if (mailbox.routingType) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_ROUTING_TYPE, mailbox.routingType);
    }
    if (mailbox.mailboxType) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_ROUTING_TYPE, mailbox.mailboxType);
    }
    soapXmlWriter.writeEndElement();
  }
}