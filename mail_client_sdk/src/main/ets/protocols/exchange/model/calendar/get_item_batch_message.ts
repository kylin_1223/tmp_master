/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldURISchema } from '../../xml/xml_attribute';
import { ItemResponseMessage } from './calendar_response_types';
import { ModelBase, UniteId } from '../mode_base';
import { PropertySet } from '../../utils/common_enum';

/**
 * 批量获取日历项报文数据
 */
export class GetItemBatchMessage extends ModelBase {
  private _baseShape: PropertySet;
  private _fieldURIs?: FieldURISchema[];
  private _itemIds: UniteId[];

  public set baseShape(value: PropertySet) {
    this._baseShape = value;
  }

  public get baseShape(): PropertySet {
    return this._baseShape;
  }

  public set fieldURIs(value: FieldURISchema[]) {
    this._fieldURIs = value;
  }

  public get fieldURIs(): FieldURISchema[] {
    return this._fieldURIs;
  }

  public set itemIds(value: UniteId[]) {
    this._itemIds = value;
  }

  public get itemIds(): UniteId[] {
    return this._itemIds;
  }
}

/**
 * 批量获取日历项返回数据
 */
export class GetItemBatchResponseMessage extends ModelBase {
  private _responseMessages: ItemResponseMessage[];

  public set responseMessages(value: ItemResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): ItemResponseMessage[] {
    return this._responseMessages;
  }
}
