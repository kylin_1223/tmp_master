/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldURISchema } from '../../xml/xml_attribute';
import { ModelBase } from '../mode_base';
import { ItemResponseMessage } from './calendar_response_types';
import { PropertySet } from '../../utils/common_enum';

export class GetRelatedRecurrenceItemsMessage extends ModelBase {
  private baseShape: PropertySet;
  private fieldUris: FieldURISchema[];
  private occurrenceItemId : OccurrenceItemId ;

  public setOccurrenceItemId(value: OccurrenceItemId): void {
    this.occurrenceItemId = value;
  }

  public getOccurrenceItemId(): OccurrenceItemId {
    return this.occurrenceItemId;
  }

  public setFieldURIs(value: FieldURISchema[]): void {
    this.fieldUris = value;
  }

  public getFieldURIs(): FieldURISchema[] {
    return this.fieldUris;
  }

  public setBaseShape(value: PropertySet): void {
    this.baseShape = value;
  }

  public getBaseShape(): PropertySet {
    return this.baseShape;
  }
}

export type OccurrenceItemId = {
  recurringMasterId: string;
  instanceIndex: number;
}


export class GetRelatedRecurrenceItemsResponseMessage extends ModelBase {
  private responseMessages: ItemResponseMessage[];

  public setResponseMessages(value: ItemResponseMessage[]): void {
    this.responseMessages = value;
  }

  public getResponseMessages(): ItemResponseMessage[] {
    return this.responseMessages;
  }
}