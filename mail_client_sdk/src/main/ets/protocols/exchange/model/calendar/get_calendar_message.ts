/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FolderName } from '../../xml/xml_attribute';
import { FolderResponseMessage } from './calendar_response_types';
import { ModelBase } from '../mode_base';
import { PropertySet } from '../../utils/common_enum';

/**
 * 获取日历文件夹请求报文数据
 */
export class GetCalendarMessage extends ModelBase {
  private _baseShape: PropertySet;
  private _folderId: FolderName;

  public set baseShape(value: PropertySet) {
    this._baseShape = value;
  }

  public get baseShape(): PropertySet {
    return this._baseShape;
  }

  public set folderId(value: FolderName) {
    this._folderId = value;
  }

  public get folderId(): FolderName {
    return this._folderId;
  }
}

/**
 * 获取日历文件夹返回数据
 */
export class GetAppointmentResponseMessage extends ModelBase {
  private _responseMessages: FolderResponseMessage[];

  public set responseMessages(value: FolderResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): FolderResponseMessage[] {
    return this._responseMessages;
  }
}

