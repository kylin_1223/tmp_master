/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from '../mode_base';

/**
 * 查找联系人返回数据类
 *
 * @since 2024-04-09
 */
export class GetContact {
  private _messageText: string;

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }
  private _responseClass: string;

  public set responseClass(value: string) {
    this._responseClass = value;
  }

  public get responseClass(): string {
    return this._responseClass;
  }

  private _responseCode: string;

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  private _fileAs: string; //表示如何在“联系人”文件夹中提交联系人。

  public set fileAs(value: string) {
    this._fileAs = value;
  }

  public get fileAs(): string {
    return this._fileAs;
  }

  private _fileAsMapping: FileAsMapping; //定义如何构造联系人显示的内容。

  public set fileAsMapping(value: FileAsMapping) {
    this._fileAsMapping = value;
  }

  public get fileAsMapping(): FileAsMapping {
    return this._fileAsMapping;
  }

  private _displayName: string; //定义联系人的显示名称。

  public set displayName(value: string) {
    this._displayName = value;
  }

  public get displayName(): string {
    return this._displayName;
  }

  private _displayNamePrefix: string; //定义联系人的职务显示名称。

  public set displayNamePrefix(value: string) {
    this._displayNamePrefix = value;
  }

  public get displayNamePrefix(): string {
    return this._displayNamePrefix;
  }

  private _givenName: string; //包含联系人的给定姓名。

  public set givenName(value: string) {
    this._givenName = value;
  }

  public get givenName(): string {
    return this._givenName;
  }

  private _initials: string; //表示联系人的首字母缩写。

  public set initials(value: string) {
    this._initials = value;
  }

  public get initials(): string {
    return this._initials;
  }

  private _middleName: string; //表示联系人的中间名。

  public set middleName(value: string) {
    this._middleName = value;
  }

  public get middleName(): string {
    return this._middleName;
  }

  private _nickname: string; //表示联系人的昵称。

  public set nickname(value: string) {
    this._nickname = value;
  }

  public get nickname(): string {
    return this._nickname;
  }

  private _yomiGivenName: string; //表示联系人的名字拼音。

  public set yomiGivenName(value: string) {
    this._yomiGivenName = value;
  }

  public get yomiGivenName(): string {
    return this._yomiGivenName;
  }

  private _yomiSurname: string; //表示联系人的姓氏拼音。

  public set yomiSurname(value: string) {
    this._yomiSurname = value;
  }

  public get yomiSurname(): string {
    return this._yomiSurname;
  }

  private _yomiCompanyName: string; //表示联系人公司拼音。

  public set yomiCompanyName(value: string) {
    this._yomiCompanyName = value;
  }

  public get yomiCompanyName(): string {
    return this._yomiCompanyName;
  }

  private _personalHomePage: string; //表示联系人个人网页。

  public set personalHomePage(value: string) {
    this._personalHomePage = value;
  }

  public get personalHomePage(): string {
    return this._personalHomePage;
  }

  private _companyName: string; //表示与联系人关联的公司名称。

  public set companyName(value: string) {
    this._companyName = value;
  }

  public get companyName(): string {
    return this._companyName;
  }

  private _emailAddresses: EmailAddress[]; //表示联系人的电子邮件地址的集合。

  public set emailAddresses(value: EmailAddress[]) {
    this._emailAddresses = value;
  }

  public get emailAddresses(): EmailAddress[] {
    return this._emailAddresses;
  }

  private _physicalAddresses: PhysicalAddress[]; //包含与联系人关联的物理地址的集合。

  public set physicalAddresses(value: PhysicalAddress[]) {
    this._physicalAddresses = value;
  }

  public get physicalAddresses(): PhysicalAddress[] {
    return this._physicalAddresses;
  }

  private _phoneNumbers: PhoneNumber[]; //表示联系人的电话号码集合。

  public set phoneNumbers(value: PhoneNumber[]) {
    this._phoneNumbers = value;
  }

  public get phoneNumbers(): PhoneNumber[] {
    return this._phoneNumbers;
  }

  private _assistantName: string; //表示联系人的助手。

  public set assistantName(value: string) {
    this._assistantName = value;
  }

  public get assistantName(): string {
    return this._assistantName;
  }

  private _birthday: string; //表示联系人的出生日期。

  public set birthday(value: string) {
    this._birthday = value;
  }

  public get birthday(): string {
    return this._birthday;
  }

  private _businessHomePage: string; //表示联系人的主页 (Web 地址) 。

  public set businessHomePage(value: string) {
    this._businessHomePage = value;
  }

  public get businessHomePage(): string {
    return this._businessHomePage;
  }

  private _children: string; //包含联系人的子女的姓名。

  public set children(value: string) {
    this._children = value;
  }

  public get children(): string {
    return this._children;
  }

  private _department: string; //表示联系人的工作部门。

  public set department(value: string) {
    this._department = value;
  }

  public get department(): string {
    return this._department;
  }

  private _generation: string; //表示在联系人全名之后的代系缩写。

  public set generation(value: string) {
    this._generation = value;
  }

  public get generation(): string {
    return this._generation;
  }

  private _imAddresses: ImAddress[]; //表示联系人的即时消息地址的集合。

  public set imAddresses(value: ImAddress[]) {
    this._imAddresses = value;
  }

  public get imAddresses(): ImAddress[] {
    return this._imAddresses;
  }

  private _jobTitle: string; //表示联系人的职务。

  public set jobTitle(value: string) {
    this._jobTitle = value;
  }

  public get jobTitle(): string {
    return this._jobTitle;
  }

  private _manager: string; //表示联系人的经理。

  public set manager(value: string) {
    this._manager = value;
  }

  public get manager(): string {
    return this._manager;
  }

  private _mileage: string; //表示联系人项的里程。

  public set mileage(value: string) {
    this._mileage = value;
  }

  public get mileage(): string {
    return this._mileage;
  }

  private _officeLocation: string; //表示联系人的办公室位置。

  public set officeLocation(value: string) {
    this._officeLocation = value;
  }

  public get officeLocation(): string {
    return this._officeLocation;
  }

  private _postalAddressIndex: string; //表示物理地址的显示类型。

  public set postalAddressIndex(value: string) {
    this._postalAddressIndex = value;
  }

  public get postalAddressIndex(): string {
    return this._postalAddressIndex;
  }

  private _profession: string; //表示联系人的职业。

  public set profession(value: string) {
    this._profession = value;
  }

  public get profession(): string {
    return this._profession;
  }

  private _spouseName: string; //表示联系人的配偶/伴侣的姓名。

  public set spouseName(value: string) {
    this._spouseName = value;
  }

  public get spouseName(): string {
    return this._spouseName;
  }

  private _surname: string; //表示联系人的姓氏。

  public set surname(value: string) {
    this._surname = value;
  }

  public get surname(): string {
    return this._surname;
  }

  private _weddingAnniversary: string; //包含联系人的结婚纪念日。

  public set weddingAnniversary(value: string) {
    this._weddingAnniversary = value;
  }

  public get weddingAnniversary(): string {
    return this._weddingAnniversary;
  }

  private _completeName: CompleteName; //表示联系人名字的集合。

  public set completeName(value: CompleteName) {
    this._completeName = value;
  }

  public get completeName(): CompleteName {
    return this._completeName;
  }

  private _itemId: ItemId; //联系人Id与ChangeKey

  public set itemId(value: ItemId) {
    this._itemId = value;
  }

  public get itemId(): ItemId {
    return this._itemId;
  }

  private _parentFolderId: ItemId; //父文件夹Id与ChangeKey

  public set parentFolderId(value: ItemId) {
    this._parentFolderId = value;
  }

  public get parentFolderId(): ItemId {
    return this._parentFolderId;
  }

  private _itemClass: string; //联系人类型。

  public set itemClass(value: string) {
    this._itemClass = value;
  }

  public get itemClass(): string {
    return this._itemClass;
  }

  private _sensitivity: string;

  public set sensitivity(value: string) {
    this._sensitivity = value;
  }

  public get sensitivity(): string {
    return this._sensitivity;
  }

  private _body: Body;

  public set body(value: Body) {
    this._body = value;
  }

  public get body(): Body {
    return this._body;
  }

  private _dateTimeReceived: string;

  public set dateTimeReceived(value: string) {
    this._dateTimeReceived = value;
  }

  public get dateTimeReceived(): string {
    return this._dateTimeReceived;
  }

  private _size: string;

  public set size(value: string) {
    this._size = value;
  }

  public get size(): string {
    return this._size;
  }

  private _importance: string;

  public set importance(value: string) {
    this._importance = value;
  }

  public get importance(): string {
    return this._importance;
  }

  private _isSubmitted: string;

  public set isSubmitted(value: string) {
    this._isSubmitted = value;
  }

  public get isSubmitted(): string {
    return this._isSubmitted;
  }

  private _isDraft: string;

  public set isDraft(value: string) {
    this._isDraft = value;
  }

  public get isDraft(): string {
    return this._isDraft;
  }

  private _isFromMe: string;

  public set isFromMe(value: string) {
    this._isFromMe = value;
  }

  public get isFromMe(): string {
    return this._isFromMe;
  }

  private _isResend: string;

  public set isResend(value: string) {
    this._isResend = value;
  }

  public get isResend(): string {
    return this._isResend;
  }

  private _isUnmodified: string;

  public set isUnmodified(value: string) {
    this._isUnmodified = value;
  }

  public get isUnmodified(): string {
    return this._isUnmodified;
  }

  private _dateTimeSent: string;

  public set dateTimeSent(value: string) {
    this._dateTimeSent = value;
  }

  public get dateTimeSent(): string {
    return this._dateTimeSent;
  }

  private _dateTimeCreated: string;

  public set dateTimeCreated(value: string) {
    this._dateTimeCreated = value;
  }

  public get dateTimeCreated(): string {
    return this._dateTimeCreated;
  }

  private _hasAttachments: string;

  public set hasAttachments(value: string) {
    this._hasAttachments = value;
  }

  public get hasAttachments(): string {
    return this._hasAttachments;
  }

  private _culture: string;

  public set culture(value: string) {
    this._culture = value;
  }

  public get culture(): string {
    return this._culture;
  }

  private _effectiveRights: EffectiveRights;

  public set effectiveRights(value: EffectiveRights) {
    this._effectiveRights = value;
  }

  public get effectiveRights(): EffectiveRights {
    return this._effectiveRights;
  }

  private _lastModifiedName: string;

  public set lastModifiedName(value: string) {
    this._lastModifiedName = value;
  }

  public get lastModifiedName(): string {
    return this._lastModifiedName;
  }

  private _lastModifiedTime: string;

  public set lastModifiedTime(value: string) {
    this._lastModifiedTime = value;
  }

  public get lastModifiedTime(): string {
    return this._lastModifiedTime;
  }

  private _isAssociated: string;

  public set isAssociated(value: string) {
    this._isAssociated = value;
  }

  public get isAssociated(): string {
    return this._isAssociated;
  }

  private _webClientReadFormQueryString: string;

  public set webClientReadFormQueryString(value: string) {
    this._webClientReadFormQueryString = value;
  }

  public get webClientReadFormQueryString(): string {
    return this._webClientReadFormQueryString;
  }

  private _conversationId: ConversationId;

  public set conversationId(value: ConversationId) {
    this._conversationId = value;
  }

  public get conversationId(): ConversationId {
    return this._conversationId;
  }

  private _flag: Flag;

  public set flag(value: Flag) {
    this._flag = value;
  }

  public get flag(): Flag {
    return this._flag;
  }

  private _instanceKey: string;

  public set instanceKey(value: string) {
    this._instanceKey = value;
  }

  public get instanceKey(): string {
    return this._instanceKey;
  }

  private _accountName: string;

  public set accountName(value: string) {
    this._accountName = value;
  }

  public get accountName(): string {
    return this._accountName;
  }

  private _objectId: string;

  public set objectId(value: string) {
    this._objectId = value;
  }

  public get objectId(): string {
    return this._objectId;
  }

  private _sourceId: string;

  public set sourceId(value: string) {
    this._sourceId = value;
  }

  public get sourceId(): string {
    return this._sourceId;
  }
}

/**
 * 获取联系人返回数据类
 *
 * @since 2024-04-09
 */
export class GetItemContactResponseModel extends ModelBase {
  private _contacts: GetContact[];

  public set contacts(value: GetContact[]) {
    this._contacts = value;
  }

  public get contacts(): GetContact[] {
    return this._contacts;
  }
}

/**
 * 联系人有效权限类型
 *
 * @since 2024-04-09
 */
export interface EffectiveRights {
  createAssociated: string
  createContents: string
  createHierarchy: string
  delete: string
  modify: string
  read: string
  viewPrivateItems: string
}

/**
 * 联系人对话Id类型
 *
 * @since 2024-04-09
 */
export interface ConversationId {
  id: string
}

/**
 * 联系人主体信息类型
 *
 * @since 2024-04-09
 */
export interface Body {
  bodyType: string
  isTruncated: string
}

/**
 * 联系人Id类型
 *
 * @since 2024-04-09
 */
export interface ItemId {
  id: string
  changeKey: string
}

/**
 * 联系人Flag类型
 *
 * @since 2024-04-09
 */
export interface Flag {
  flagStatus?: string
}

/**
 * 联系人名称组合类型
 *
 * @since 2024-04-09
 */
export interface CompleteName {
  title?: string
  firstName?: string
  middleName?: string
  lastname?: string
  suffix?: string
  initials?: string
  fullName?: string
  nickname?: string
  yomiFirstName?: string
  yomiLastName?: string
}

/**
 * 联系人邮箱信息组合类型
 *
 * @since 2024-04-09
 */
export interface EmailAddress {
  key: string
  name?: string
  routingType?: string
  mailboxType?: string
  elementContent: string
}

/**
 * 联系人物理地址类型
 *
 * @since 2024-04-09
 */
export interface PhysicalAddress {
  key: string
  elementContent: PhysicalAddressElementContent
}

/**
 * 联系人物理地址组合类型
 *
 * @since 2024-04-09
 */
export interface PhysicalAddressElementContent {
  street?: string
  city?: string
  state?: string
  countryOrRegion?: string
  postalCode?: string
}

/**
 * 联系人联系电话类型
 *
 * @since 2024-04-09
 */
export interface PhoneNumber {
  key: PhoneNumbersKey
  elementContent: string
}

/**
 * 联系人联系电话联合类型
 *
 * @since 2024-04-09
 */
export type PhoneNumbersKey =
'AssistantPhone'
  | 'BusinessFax'
  | 'BusinessPhone'
  | 'BusinessPhone2'
  | 'Callback'
  | 'CarPhone'
  | 'CompanyMainPhone'
  | 'HomeFax'
  | 'HomePhone'
  | 'HomePhone2'
  | 'Isdn'
  | 'MobilePhone'
  | 'OtherFax'
  | 'OtherTelephone'
  | 'Pager'
  | 'PrimaryPhone'
  | 'RadioPhone'
  | 'Telex'
  | 'TtyTddPhone'
  | 'Other'

/**
 * 联系人通讯地址类型
 *
 * @since 2024-04-09
 */
export interface ImAddress {
  key: ImAddressesKey
  elementContent: string
}

/**
 * 联系人通讯地址联合类型
 *
 * @since 2024-04-09
 */
export type ImAddressesKey =
'ImAddress1'
  | 'ImAddress2'
  | 'ImAddress3'

/**
 * 联系人名称展示方式联合类型
 *
 * @since 2024-04-09
 */
export type FileAsMapping =
| 'None'
  | 'LastCommaFirst'
  | 'FirstSpaceLast'
  | 'Company'
  | 'LastCommaFirstCompany'
  | 'CompanyLastFirst'
  | 'LastFirst'
  | 'LastFirstCompany'
  | 'CompanyLastCommaFirst'
  | 'LastFirstSuffix'
  | 'LastSpaceFirstCompany'
  | 'CompanyLastSpaceFirst'
  | 'LastSpaceFirst'
  | 'DisplayName'
  | 'FirstName'
  | 'LastFirstMiddleSuffix'
  | 'LastName'
  | 'Empty'
