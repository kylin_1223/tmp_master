/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldURISchema } from '../../xml/xml_attribute'

/**
 * 属性定义类
 * 用于定义属性和扩展属性
 * @since 2024-03-23
 */
export class PropertyDefinition {
  private _fieldURIArr: (IPropertySet | FieldURISchema)[];
  private _propertySet: IPropertySet;
  private _fieldURIString: FieldURISchema;
  private _value: string | FieldURISchema;

  public set fieldURIArr(value: (IPropertySet | FieldURISchema)[]) {
    this._fieldURIArr = value;
  }

  public get fieldURIArr(): (IPropertySet | FieldURISchema)[] {
    return this._fieldURIArr;
  }

  public set propertySet(value: IPropertySet) {
    this._propertySet = value;
  }

  public get propertySet(): IPropertySet {
    return this._propertySet;
  }

  public set fieldURIString(value: FieldURISchema) {
    this._fieldURIString = value;
  }

  public get fieldURIString(): FieldURISchema {
    return this._fieldURIString;
  }

  public set value(value: string | FieldURISchema) {
    this._value = value;
  }

  public get value(): string | FieldURISchema {
    return this._value;
  }

  constructor(propertyDefinition?: IPropertySet | FieldURISchema, value?: string | FieldURISchema) {
    if (typeof propertyDefinition === 'string') {
      this.fieldURIString = propertyDefinition;
    } else {
      this.propertySet = propertyDefinition;
    }
    this._value = value;
  }
}

/**
 * 搜索视图类
 * 搜索视图的项视图、排序顺序、项遍历和基本形状
 * @since 2024-03-23
 */
export class SearchView extends PropertyDefinition {
  private _itemView: IItemView;
  private _orderBy: ISortOrder;
  private _itemTraversal: ItemTraversal;
  private _baseShape: BaseShape;

  public set itemView(value: IItemView) {
    this._itemView = value;
  }

  public get itemView(): IItemView {
    return this._itemView;
  }

  public set orderBy(value: ISortOrder) {
    this._orderBy = value;
  }

  public get orderBy(): ISortOrder {
    return this._orderBy;
  }

  public set itemTraversal(value: ItemTraversal) {
    this._itemTraversal = value;
  }

  public get itemTraversal(): ItemTraversal {
    return this._itemTraversal;
  }

  public set baseShape(value: BaseShape) {
    this._baseShape = value;
  }

  public get baseShape(): BaseShape {
    return this._baseShape;
  }
}

export interface IPropertySet {
  propertySetId: string;
  propertySet?: string;
  DistinguishedPropertySetId?: string;
  propertyName: string;
  propertyType: PropertyType;
  PropertyTag?: string;
}


export interface ISortOrder {
  orderBy: SortOrder;
  FieldURI: FieldURISchema;
}

export interface IItemView {
  offset: string;
  basePoint: BasePoint;
  maxEntriesReturned?: string;
}

export enum ItemTraversal {
  Shallow,
  SoftDeleted,
  Associated,
}

export enum BasePoint {
  Beginning,
  End,
}

export enum SortOrder {
  Ascending,
  Descending,
}

export enum BaseShape {
  IdOnly,
  Default,
  AllProperties,
}

export enum PropertyType {
  ApplicationTime,
  ApplicationTimeArray,
  Binary,
  BinaryArray,
  Boolean,
  CLSID,
  CLSIDArray,
  Currency,
  CurrencyArray,
  Double,
  DoubleArray,
  Error,
  Float,
  FloatArray,
  Integer,
  IntegerArray,
  Long,
  LongArray,
  Null,
  Object,
  ObjectArray,
  Short,
  ShortArray,
  SystemTime,
  SystemTimeArray,
  String,
  StringArray,
}