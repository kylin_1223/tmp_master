/* Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 搜索邮件指定文件夹类
 * 搜索指定文件夹下的邮件 id和chengKey
 * @since 2024-04-08
 */
export class Folder {
  private _id: string;
  private _changeKey: string;

  public set id(value: string) {
    this._id = value;
  }

  public get id(): string {
    return this._id;
  }

  public set changeKey(value: string) {
    this._changeKey = value;
  }

  public get changeKey(): string {
    return this._changeKey;
  }

  constructor(id: string, changeKey: string) {
    this._id = id;
    this._changeKey = changeKey;
  }
}