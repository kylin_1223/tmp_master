/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from '../mode_base';
import { SoapXmlWriter, XmlElement, XmlNamespace, XmlWritable } from '../../xml';
import { JsObjectElement } from '../../xml/soap_xml';
import { Message } from './item_message';

/**
 * 附件基本模型类
 */
export class BaseAttachments implements XmlWritable {
  private _attachmentId: string; // 附件ID
  private _name: string; // 附件名称
  private _contentType: string; // 附件类型
  private _contentId: string; // 表示附件内容的标识符
  private _contentLocation: string; // 包含与附件内容的位置相对应的统一资源标识符
  private _lastModifiedTime: string; // 表示文件附件的最后修改时间
  private _isInline: boolean; // 表示附件是否内联显示在项目中
  private _size: number; // 附件大小
  private _isContactPhoto: boolean; // 指示文件附件是否是联系人图片

  /**
   * 设置附件ID
   *
   * @param attachmentId 附件ID
   */
  public set attachmentId(value: string) {
    this._attachmentId = value;
  }

  /**
   * 获取附件ID
   *
   * @returns 获取附件attachmentId
   */
  public get attachmentId(): string {
    return this._attachmentId;
  }

  /**
   * 设置附件名称
   *
   * @param name 附件名称
   */
  public set name(value: string) {
    this._name = value;
  }

  /**
   * 获取附件名称
   *
   * @returns 获取附件名称name
   */
  public get name(): string {
    return this._name;
  }

  /**
   * 设置附件类型
   *
   * @param contentType 附件类型
   */
  public set contentType(value: string) {
    this._contentType = value;
  }

  /**
   * 获取附件类型
   *
   * @returns 获取附件类型contentType
   */
  public get contentType(): string {
    return this._contentType;
  }

  public set contentId(value: string) {
    this._contentId = value;
  }

  public get contentId(): string {
    return this._contentId;
  }

  public set contentLocation(value: string) {
    this._contentLocation = value;
  }

  public get contentLocation(): string {
    return this._contentLocation;
  }

  /**
   * 设置附件大小
   *
   * @param size 附件大小
   */
  public set size(value: number) {
    this._size = value;
  }

  /**
   * 获取附件大小
   *
   * @returns 附件大小size
   */
  public get size(): number {
    return this._size;
  }

  public set lastModifiedTime(value: string) {
    this._lastModifiedTime = value;
  }

  public get lastModifiedTime(): string {
    return this._lastModifiedTime;
  }

  /**
   * 设置附件是否内联显示
   *
   * @param isInline 附件是否内联显示
   */
  public set isInline(value: boolean) {
    this._isInline = value;
  }

  /**
   * 获取附件是否内联显示
   *
   * @returns 附件是否内联显示isInline
   */
  public get isInline(): boolean {
    return this._isInline;
  }

  public set isContactPhoto(value: boolean) {
    this._isContactPhoto = value;
  }

  public get isContactPhoto(): boolean {
    return this._isContactPhoto;
  }

  /**
   * 根据当前属性写出对应xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_NAME, this.name);
  }

  /**
   * 根据当前属性写出对应xml
   *
   * @param soapXmlWriter xml写入器
   */
  public readFromXml(itemAttrs: JsObjectElement[]): void {
    let baseAttachment = itemAttrs;
    baseAttachment.forEach(item => {
      if (item._name === 'AttachmentId') {
        this.attachmentId = item._attributes.Id;
      } else if (item._name === 'Name') {
        this._name = item._elements[0]._text;
      } else if (item._name === 'ContentType') {
        this.contentType = item._elements[0]._text;
      } else if (item._name === 'ContentId') {
        this.contentId = item._elements[0]._text;
      } else if (item._name === 'Size') {
        this.size = Number(item._elements[0]._text);
      } else if (item._name === 'LastModifiedTime') {
        this._lastModifiedTime = item._elements[0]._text;
      } else if (item._name === 'IsInline') {
        this.isInline = Boolean(item._elements[0]._text);
      } else if (item._name === 'IsContactPhoto') {
        this.isContactPhoto = Boolean(item._elements[0]._text);
      }
    })
  }
}

/**
 * 文件附件类
 */
export class FileAttachment extends BaseAttachments {
  private _content?: string;

  /**
   * 设置附件base64字符串
   *
   * @param _content 附件base64字符串
   */
  public set content(value: string) {
    this._content = value;
  }

  /**
   * 获取文件附件Base64
   *
   * @returns 获取文件附件Base64 _content
   */
  public get content(): string {
    return this._content;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_FILE_ATTACHMENT);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_CONTENT, this.content);
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    super.readFromXml(itemAttrs);
    itemAttrs.forEach(fileItem => {
      if (fileItem._name === 'Content') {
        this._content = fileItem._elements[0]._text;
      }
    });
  }
}

/**
 * 项目附件类
 */
export class ItemAttachment extends BaseAttachments {
  private _item?: XmlWritable;

  /**
   * 设置项目附件
   *
   * @param _item 项目附件
   */
  public set item(value: XmlWritable) {
    this._item = value;
  }

  /**
   * 获取项目附件
   *
   * @returns 获取文件附件_item
   */
  public get item(): XmlWritable {
    return this._item;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_ITEM_ATTACHMENT);
    super.writeToXml(soapXmlWriter);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_IS_INLINE, String(this.isInline));
    this._item.writeToXml(soapXmlWriter)
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    super.readFromXml(itemAttrs);
    // 修改为switch case
    itemAttrs.forEach(item => {
      if (item._name === 'Message') {
        const message = new Message();
        message.readFromXml(item._elements);
        this.item = message
      } else if (item._name === 'Item') {
      } else if (item._name === 'CalendarItem') {
      } else if (item._name === 'Contact') {
      } else if (item._name === 'Task') {
      } else if (item._name === 'MeetingMessage') {
      } else if (item._name === 'MeetingRequest') {
      } else if (item._name === 'MeetingResponse') {
      } else if (item._name === 'MeetingCancellation') {
      }
    });
  }
}

/**
 * 附件类
 */
export class Attachment implements XmlWritable {
  private _attachments: BaseAttachments[] = [];

  /**
   * 添加附件
   *
   * @param attachments 附件
   */
  public addAttachments(attachments: BaseAttachments): void {
    this._attachments.push(attachments);
  }

  /**
   * 获取项目
   *
   * @returns 获取附件attachments
   */
  public get attachments(): BaseAttachments[] {
    return this._attachments;
  }

  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.MESSAGE, XmlElement.NAME_ATTACHMENTS);
    this._attachments.forEach((data) => {
      data.writeToXml(soapXmlWriter)
    });
    soapXmlWriter.writeEndElement();
  }

  public readFromXml(itemAttrs: JsObjectElement[]): void {
    this._attachments.forEach((data) => {
      data.readFromXml(itemAttrs)
    });
  }
}

/**
 * 创建附件类
 */
export class CreateAttachment extends ModelBase {
  private _parentItemId: string;
  private _attachment: Attachment = new Attachment();

  public set parentItemId(value: string) {
    this._parentItemId = value;
  }

  public get parentItemId(): string {
    return this._parentItemId;
  }

  public addAttachments(attachment: BaseAttachments): void {
    this._attachment.addAttachments(attachment);
  }

  public get attachment(): Attachment {
    return this._attachment;
  }
}

/**
 * 创建附件响应类
 */
export class CreateAttachmentResp extends ModelBase {
  private _responseMessages: CreateAttachmentResponseMessage[];

  public set responseMessages(value: CreateAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): CreateAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

export class CreateAttachmentResponseMessage {
  private _responseCode: string;
  private _messageText: string;
  private _attachmentList: Array<FileAttachment | ItemAttachment>;

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }

  public set attachmentList(value: Array<FileAttachment | ItemAttachment>) {
    this._attachmentList = value;
  }

  public get attachmentList(): Array<FileAttachment | ItemAttachment> {
    return this._attachmentList;
  }
}

/**
 * 删除附件类
 */
export class DeleteAttachment extends ModelBase {
  private _attachmentIds: Array<string>;

  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }
}

/**
 * 删除附件响应类
 */
export class DeleteAttachmentResp extends ModelBase {
  private _responseMessages: DeleteAttachmentResponseMessage[];

  public set responseMessages(value: DeleteAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): DeleteAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

export class DeleteAttachmentResponseMessage {
  private _responseCode: string;

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  private _messageText: string;

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }
}

/**
 * 获取附件类
 *
 */
export class GetAttachment extends ModelBase {
  private _attachmentIds: Array<string>;

  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }
}

/**
 * 获取附件响应类
 */
export class GetAttachmentResp extends ModelBase {
  private _responseMessages: GetAttachmentResponseMessage[];

  public set responseMessages(value: GetAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): GetAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}

/**
 * 获取附件响应ResponseMessage类
 */
export class GetAttachmentResponseMessage {
  private _responseCode: string;
  private _messageText: string;
  private _attachmentList: Array<FileAttachment | ItemAttachment>;

  public set responseCode(value: string) {
    this._responseCode = value;
  }

  public get responseCode(): string {
    return this._responseCode;
  }

  public set messageText(value: string) {
    this._messageText = value;
  }

  public get messageText(): string {
    return this._messageText;
  }

  public set attachmentList(value: Array<FileAttachment | ItemAttachment>) {
    this._attachmentList = value;
  }

  public get attachmentList(): Array<FileAttachment | ItemAttachment> {
    return this._attachmentList;
  }
}

/**
 * 删除附件类
 */
export class DownLoadAttachment extends ModelBase {
  private _path: string;
  private _attachmentIds: Array<string>;
  private _onDataReceive: Function;
  private _onComplete: Function;

  public set path(value: string) {
    this._path = value;
  }

  public get path(): string {
    return this._path;
  }

  public set attachmentIds(value: Array<string>) {
    this._attachmentIds = value;
  }

  public get attachmentIds(): Array<string> {
    return this._attachmentIds;
  }

  public set onDataReceive(value: Function) {
    this._onDataReceive = value;
  }

  public get onDataReceive(): Function {
    return this._onDataReceive;
  }

  public set onComplete(value: Function) {
    this._onComplete = value;
  }

  public get onComplete(): Function {
    return this._onComplete;
  }
}

/**
 * 删除附件响应类
 */
export class DownLoadAttachmentResp extends ModelBase {
  private _responseMessages: GetAttachmentResponseMessage[];

  public set responseMessages(value: GetAttachmentResponseMessage[]) {
    this._responseMessages = value;
  }

  public get responseMessages(): GetAttachmentResponseMessage[] {
    return this._responseMessages;
  }
}





