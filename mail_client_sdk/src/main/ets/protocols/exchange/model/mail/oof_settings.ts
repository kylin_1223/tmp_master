/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ModelBase } from '../mode_base';

/**
 * 确定向谁发送外部外出 (OOF) 消息
 */
export enum OofExternalAudience {
  None = 0,
  Known = 1,
  All = 2,
}

/**
 * OOF 启用状态
 */
export enum OofState {
  /**
   * 不启用
   */
  Disabled = 0,

  /**
   * 启用，不使用时间范围
   */
  Enabled = 1,

  /**
   * 启用，使用时间范围
   */
  Scheduled = 2,
}

/**
 * 邮箱路由类型
 */
export enum MailBoxRouteType {
  SMTP = 'SMTP',
  EX = 'EX',
};

/**
 * SetUserOofSettings 或 GetUserOofSettings 请求的邮箱用户
 *
 * @since 2024-03-16
 */
export class OofMailbox extends ModelBase {
  private _address: string = ''; // 邮箱地址
  private _name?: string = ''; // 用户名
  private _routeType?: MailBoxRouteType = MailBoxRouteType.SMTP; // 收件人的路由协议，默认SMTP

  /**
   * 获取邮箱地址
   *
   * @returns 邮箱地址
   */
  public get address(): string {
    return this._address;
  }

  /**
   * 设置邮箱地址
   * @param value 邮箱地址
   */
  public set address(value: string) {
    this._address = value;
  }

  /**
   * 获取用户名
   *
   * @returns 用户名
   */
  public get name(): string {
    return this._name;
  }

  /**
   * 设置用户名
   * @param value 用户名
   */
  public set name(value: string) {
    this._name = value;
  }

  /**
   * 获取路由类型
   *
   * @returns 路由类型
   */
  public get routeType(): MailBoxRouteType {
    return this._routeType;
  }

  /**
   * 设置路由类型
   *
   * @param value 路由类型
   */
  public set routeType(value: MailBoxRouteType) {
    this._routeType = value;
  }
}

/**
 * 自动回复设置
 *
 * @since 2024-03-16
 */
export class OofSettings extends OofMailbox {
  private _state: OofState = OofState.Disabled; // 启用状态
  private _startTime: Date | null = null; // 开始时间
  private _endTime: Date | null = null; // 结束时间
  private _externalAudience: OofExternalAudience = OofExternalAudience.None; // 组织外受众
  private _externalReply: string = ''; // 组织外模板
  private _internalReply: string = ''; // 组织内模板

  /**
   * 获取启用状态
   *
   * @returns 启用状态
   */
  public get state(): OofState {
    return this._state;
  }

  /**
   * 设置启用状态
   *
   * @param value 启用状态
   */
  public set state(value: OofState) {
    this._state = value;
  }

  /**
   * 获取开始时间
   *
   * @returns 开始时间
   */
  public get startTime(): Date | null {
    return this._startTime;
  }

  /**
   * 设置开始时间
   *
   * @param value 开始时间
   */
  public set startTime(value: Date | null) {
    this._startTime = value;
  }

  /**
   * 获取结束时间
   *
   * @returns 结束时间
   */
  public get endTime(): Date | null {
    return this._endTime;
  }

  /**
   * 设置结束时间
   *
   * @param value 结束时间
   */
  public set endTime(value: Date | null) {
    this._endTime = value;
  }

  /**
   * 获取组织外受众
   *
   * @returns 组织外受众
   */
  public get externalAudience(): OofExternalAudience {
    return this._externalAudience;
  }

  /**
   * 设置组织外受众
   *
   * @param value 组织外受众
   */
  public set externalAudience(value: OofExternalAudience) {
    this._externalAudience = value;
  }

  /**
   * 获取组织外模板
   *
   * @returns 组织外模板
   */
  public get externalReply(): string {
    return this._externalReply;
  }

  /**
   * 设置组织外模板
   *
   * @param value 组织外模板
   */
  public set externalReply(value: string) {
    this._externalReply = value;
  }

  /**
   * 获取组织内模板
   *
   * @returns 组织内模板
   */
  public get internalReply(): string {
    return this._internalReply;
  }

  /**
   * 设置组织内模板
   *
   * @param value 组织内模板
   */
  public set internalReply(value: string) {
    this._internalReply = value;
  }
}
