/*
 * Copyright © 2023-2024 Coremail论客.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {
  XmlAttribute,
  XmlElement,
  XmlNamespace,
  XmlWritable,
  JsObjectElement,
  SoapXmlWriter,
  XmlReadable
} from '../../xml';
import { getLogger, Logger } from '../../../../utils/log';
import { MailboxType } from '../../utils/common_enum';
import { UniteId } from '../mode_base';
import { ExtendedProperty } from '../extended_property';
import { JsObjectAttribute } from '../../xml/soap_xml';
import { BaseAttachments, FileAttachment, ItemAttachment } from './attachments';
import { ExtendedProperty } from '../extended_property';
import { lowercaseFirstLetter } from '../../utils/sync_util';

const logger: Logger = getLogger('MessageModel');

/**
 * 邮件实体类
 *
 * @since 2024-04-08
 */
export class Message implements XmlWritable, XmlReadable {
  private _subject: string; // 存储项的主题属性,主题限制为 255 个字符
  private _sensitivity: Sensitivity; // 敏感度级别
  private _importance: Importance; // 描述项目的重要性
  private _isDraft: string; // 是否尚未发送项目
  private _reminderIsSet: string; // 是否已为项目设置Exchange提醒
  private _toRecipients: Mailbox[]; // 收件人集合
  private _ccRecipients?: Mailbox[] = []; // 抄送人集合
  private _bccRecipients?: Mailbox[] = []; // 密送人集合
  private _isRead: string; // 是否已读
  private _body: Body; // 邮件正文
  private _size: string; // 邮件大小
  private _dateTimeSent: string; // 发送时间
  private _dateTimeCreated: string; // 创建时间
  private _from: Mailbox; // 收件人
  private _hasAttachments: string; // 是否包含附件
  private _attachments: BaseAttachments[]; // 附件
  private _extendedProperty: ExtendedProperty[] = []; // 扩展属性

  public childWritable: XmlWritable[] = []; // 子元素写方法集合

  // 此集合用于转换xml中属性至对象；键表示属性名，值表示对应的属性，应该如何设置进对象里
  private readAttributeStrategyMap: Map<string, Function> = new Map<string, Function>([
    [XmlElement.NAME_SUBJECT, (tagElements: JsObjectElement[]) => {
      this.subject = tagElements[0]?._text;
    }],
    [XmlElement.NAME_SENSITIVITY, (tagElements: JsObjectElement[]) => {
      this.sensitivity = Sensitivity[tagElements[0]?._text];
    }],
    [XmlElement.NAME_IMPORTANCE, (tagElements: JsObjectElement[]) => {
      this.importance = Importance[tagElements[0]?._text];
    }],
    [XmlElement.NAME_IS_DRAFT, (tagElements: JsObjectElement[]) => {
      this.isDraft = tagElements[0]?._text === 'true' ? 'true' : 'false';
    }],
    [XmlElement.NAME_REMINDER_IS_SET, (tagElements: JsObjectElement[]) => {
      this.reminderIsSet = tagElements[0]?._text;
    }],
    [XmlElement.NAME_TO_RECIPIENTS, (tagElements: JsObjectElement[]) => {
      this._toRecipients = this.getMailBoxes(tagElements);
    }],
    [XmlElement.NAME_CC_RECIPIENTS, (tagElements: JsObjectElement[]) => {
      this.ccRecipients = this.getMailBoxes(tagElements);
    }],
    [XmlElement.NAME_BCC_RECIPIENTS, (tagElements: JsObjectElement[]) => {
      this.bccRecipients = this.getMailBoxes(tagElements);
    }],
    [XmlElement.NAME_IS_READ, (tagElements: JsObjectElement[]) => {
      this.isRead = tagElements[0]?._text === 'true' ? 'true' : 'false';
    }],
    [XmlElement.NAME_BODY, (tagElements: JsObjectElement[], tagAttributes: JsObjectAttribute) => {
      let body: Body = {
        value: tagElements && tagElements[0]?._text,
        bodyType: BodyType[tagAttributes.BodyType],
        isTruncated: Boolean(tagAttributes.IsTruncated)
      }
      this.body = body;
    }],
    [XmlElement.NAME_SIZE, (tagElements: JsObjectElement[]) => {
      this.size = tagElements[0]?._text;
    }],
    [XmlElement.NAME_DATE_TIME_SENT, (tagElements: JsObjectElement[]) => {
      this.dateTimeSent = tagElements[0]?._text;
    }],
    [XmlElement.NAME_DATE_TIME_CREATED, (tagElements: JsObjectElement[]) => {
      this.dateTimeCreated = tagElements[0]?._text;
    }],
    [XmlElement.NAME_FROM, (tagElements: JsObjectElement[]) => {
      let mailBox: Mailbox = new Mailbox();
      for (let i = 0; i < tagElements[0]?._elements?.length; i++) {
        let mailBoxAttrTagName: string = tagElements[0]?._elements[i]?._name;
        mailBox[lowercaseFirstLetter(mailBoxAttrTagName)] = tagElements[0]?._elements[i]?._elements[0]?._text;
      }
      this.from = mailBox;
    }],
    [XmlElement.NAME_HAS_ATTACHMENTS, (tagElements: JsObjectElement[]) => {
      this.hasAttachments = tagElements[0]?._text === 'true' ? 'true' : 'false';
    }],
    [XmlElement.NAME_ATTACHMENTS, (tagElements: JsObjectElement[]) => {
      let attachmentArr: BaseAttachments[] = [];
      tagElements?.forEach(attachmentElement => {
        if (attachmentElement._name === XmlElement.NAME_FILE_ATTACHMENT) {
          let fileAttachment = new FileAttachment();
          fileAttachment.readFromXml(attachmentElement._elements);
          attachmentArr.push(fileAttachment);
        } else if (attachmentElement._name === XmlElement.NAME_ITEM_ATTACHMENT) {
          let itemAttachment = new ItemAttachment();
          itemAttachment.readFromXml(attachmentElement._elements);
          attachmentArr.push(itemAttachment);
        }
      });
      this.attachments = attachmentArr;
    }],
  ]);

  /**
   * 添加附件
   *
   * @param attachments 附件集合
   */
  public addAttachment(attachments: BaseAttachments[]): Message {
    this.childWritable.push({
      nameSpace: XmlNamespace.TYPE,
      elementName: XmlElement.NAME_ATTACHMENTS,
      childWritable: attachments,
    });
    return this;
  }

  /**
   * 获取项目主题
   *
   * @returns 项目主题
   */
  public get subject(): string {
    return this._subject;
  }

  /**
   * 设置项目主题
   *
   * @param subject 主题
   */
  public set subject(subject: string) {
    this._subject = subject;
    this.childWritable.push(getWritableElement(XmlNamespace.TYPE, XmlElement.NAME_SUBJECT, subject));
  }

  /**
   * 获取项目正文
   *
   * @returns 项目正文
   */
  public get body(): Body {
    return this._body;
  }

  /**
   * 设置项目正文
   *
   * @param body 项目正文
   */
  public set body(body: Body) {
    this._body = body;
    const bodyAttr: Map<string, string> = new Map();
    if (body?.bodyType) {
      bodyAttr.set(XmlAttribute.NAME_BODY_TYPE, BodyType[body.bodyType]);
    }
    if (body?.isTruncated) {
      bodyAttr.set(XmlAttribute.NAME_IS_TRUNCATED, String(Boolean(body.isTruncated)));
    }
    this.childWritable.push(getWritableElement(XmlNamespace.TYPE, XmlElement.NAME_BODY, body.value, bodyAttr));
  }

  /**
   * 获取项目敏感度级别
   *
   * @returns 项目敏感度级别
   */
  public get sensitivity(): Sensitivity {
    return this._sensitivity;
  }

  /**
   * 设置项目敏感度级别
   *
   * @param sensitivity 敏感度
   */
  public set sensitivity(sensitivity: Sensitivity) {
    this._sensitivity = sensitivity;
    this.childWritable.push(getWritableElement('', XmlElement.NAME_SENSITIVITY, this._sensitivity));
  }

  /**
   * 获取项目重要性
   *
   * @returns 项目重要性
   */
  public get importance(): Importance {
    return this._importance;
  }

  /**
   * 设置项目重要性
   *
   * @param importance 重要性
   */
  public set importance(importance: Importance) {
    this._importance = importance;
    this.childWritable.push(getWritableElement('', XmlElement.NAME_IMPORTANCE, Importance[this._importance]));
  }

  /**
   * 获取项目是否为草稿
   *
   * @returns 是否为草稿
   */
  public get isDraft(): string {
    return this._isDraft;
  }

  /**
   * 修改项目未发送状态
   *
   * @param isDraft 是否为草稿
   */
  public set isDraft(isDraft: string) {
    this._isDraft = isDraft;
    this.childWritable.push(getWritableElement('', XmlElement.NAME_IS_DRAFT, this._isDraft));
  }

  /**
   * 获取项目是否设置提醒
   */
  public get reminderIsSet(): string {
    return this._reminderIsSet;
  }

  /**
   * 修改项目是否设置提醒
   *
   * @param reminderIsSet 是否设置提醒
   */
  public set reminderIsSet(reminderIsSet: string) {
    this._reminderIsSet = reminderIsSet;
    this.childWritable.push(getWritableElement('', XmlElement.NAME_REMINDER_IS_SET, this._reminderIsSet));
  }

  /**
   * 获取收件人数组
   *
   * @returns 收件人数组
   */
  public get toRecipients(): Mailbox[] {
    return this._toRecipients;
  }

  /**
   * 设置收件人
   *
   * @param toRecipients 收件人集合
   */
  public set toRecipients(toRecipients: Mailbox[]) {
    this._toRecipients = [...toRecipients];
    let recipients: XmlWritable = {
      nameSpace: XmlNamespace.TYPE,
      elementName: XmlElement.NAME_TO_RECIPIENTS,
      childWritable: toRecipients,
    }
    this.childWritable.push(recipients);
  }

  /**
   * 获取抄送人集合
   *
   * @returns 抄送人集合
   */
  public get ccRecipients(): Mailbox[] {
    return this._ccRecipients;
  }

  /**
   * 设置抄送人
   *
   * @param ccRecipients 抄送人集合
   */
  public set ccRecipients(ccRecipients: Mailbox[]) {
    this._ccRecipients = [...ccRecipients];
    let recipients: XmlWritable = {
      nameSpace: XmlNamespace.TYPE,
      elementName: XmlElement.NAME_CC_RECIPIENTS,
      childWritable: ccRecipients,
    }
    this.childWritable.push(recipients);
  }

  /**
   * 获取密送人
   */
  public get bccRecipients(): Mailbox[] {
    return this._bccRecipients;
  }

  /**
   * 设置密送人
   *
   * @param bccRecipients 密送人集合
   */
  public set bccRecipients(bccRecipients: Mailbox[]) {
    this._bccRecipients = [...bccRecipients];
    let recipients: XmlWritable = {
      nameSpace: XmlNamespace.TYPE,
      elementName: XmlElement.NAME_BCC_RECIPIENTS,
      childWritable: bccRecipients,
    }
    this.childWritable.push(recipients);
  }

  /**
   * 获取已读状态
   *
   * @returns 是否已读
   */
  public get isRead(): string {
    return this._isRead;
  }

  /**
   * 设置已读属性
   *
   * @param isRead 已读状态
   */
  public set isRead(isRead: string) {
    this._isRead = isRead;
    this.childWritable.push(getWritableElement(XmlNamespace.TYPE, XmlElement.NAME_IS_READ, this._isRead));
  }

  /**
   * 获取邮件大小
   *
   * @returns string 邮件大小
   */
  public get size(): string {
    return this._size;
  }

  /**
   * 设置邮件大小
   *
   * @param size 邮件大小
   */
  public set size(size: string) {
    this._size = size;
  }

  /**
   * 获取发送时间
   *
   * @returns string 发送时间
   */
  public get dateTimeSent(): string {
    return this._dateTimeSent;
  }

  /**
   * 设置发送时间
   *
   * @param dateTimeSent 发送时间
   */
  public set dateTimeSent(dateTimeSent: string) {
    this._dateTimeSent = dateTimeSent;
  }

  /**
   * 创建时间
   *
   * @returns string 创建时间
   */
  public get dateTimeCreated(): string {
    return this._dateTimeCreated;
  }

  /**
   * 创建时间
   *
   * @param dateTimeCreated 创建时间
   */
  public set dateTimeCreated(dateTimeCreated: string) {
    this._dateTimeCreated = dateTimeCreated;
  }

  /**
   * 发件人
   *
   * @returns 发件人
   */
  public get from(): Mailbox {
    return this._from;
  }

  /**
   * 设置发件人
   *
   * @param from 发件人
   */
  public set from(from: Mailbox) {
    this._from = from;
  }

  /**
   * 获取发件人
   *
   * @returns string 发件人
   */
  public get hasAttachments(): string {
    return this._hasAttachments;
  }

  /**
   * 设置是否包含附件
   *
   * @param hasAttachments
   */
  public set hasAttachments(hasAttachments: string) {
    this._hasAttachments = hasAttachments;
  }

  /**
   * 获取附件列表
   *
   * @returns BaseAttachments[] 附件列表
   */
  public get attachments(): BaseAttachments[] {
    return this._attachments;
  }

  /**
   * 设置附件列表
   *
   * @param attachments 附件列表
   */
  public set attachments(attachments: BaseAttachments[]) {
    this._attachments = attachments;
  }

  /**
   * 获取扩展属性
   *
   * @returns 扩展属性
   */
  public get extendedProperty(): ExtendedProperty[] {
    return this._extendedProperty;
  }

  /**
   * 设置扩展属性
   *
   * @param property 扩展属性
   */
  public set extendedProperty(property: ExtendedProperty[]) {
    this._extendedProperty = property;
    property.forEach((item) => {
      this.childWritable.push({
        nameSpace: XmlNamespace.TYPE,
        elementName: XmlElement.NAME_EXTENDED_PROPERTY,
        childWritable: [item],
      });
    });
  }

  /**
   * xml Message写入方法
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_MESSAGE);
    this.childWritable?.forEach((child: XmlWritable) => {
      this.writeElementToXml(soapXmlWriter, child);
    });
    soapXmlWriter.writeEndElement();
  }

  /**
   * 带有子标签的xml写入方法
   *
   * @param soapXmlWriter xml写入器
   * @param namespace 命名空间
   * @param elementName 标签名
   * @param value 标签值
   * @param attrMap 属性和属性值的Map集合
   * @param writeChild 子标签写入xml的方法
   */
  protected writeElementToXml(soapXmlWriter: SoapXmlWriter, childWritable: XmlWritable): void {
    if (!childWritable.elementName) {
      logger.error('ChildWritable elementName is Empty')
      return;
    }
    if (childWritable.value && childWritable.attrMap?.size > 0) {
      soapXmlWriter.writeIntactElement(childWritable.nameSpace, childWritable.elementName, childWritable.attrMap,
        childWritable.value);
      return;
    }
    if (childWritable.value) {
      soapXmlWriter.writeElementWithValue(childWritable.nameSpace, childWritable.elementName, childWritable.value);
      return;
    }
    soapXmlWriter.writeStartElement(childWritable.nameSpace, childWritable.elementName);
    childWritable?.childWritable?.forEach((child: XmlWritable) => {
      child.writeToXml(soapXmlWriter);
    });
    soapXmlWriter.writeEndElement();
  }

  /**
   * xml读取方法
   *
   * @param jsObjects xml解析后的Json对象
   */
  public readFromXml(itemAttrs: JsObjectElement[]): void {
    for (let j = 0; j < itemAttrs.length; j++) {
      let tagName = itemAttrs[j]._name;
      let tagElements = itemAttrs[j]._elements;
      let tagAttributes = itemAttrs[j]._attributes;
      if (!tagElements && !tagAttributes) {
        continue;
      }
      try {
        if(tagName === XmlElement.NAME_BODY) {
          this.readAttributeStrategyMap.get(tagName)?.(tagElements, tagAttributes);
        } else {
          this.readAttributeStrategyMap.get(tagName)?.(tagElements);
        }
      } catch (err) {
        // XML 读取异常；　加自己的消息　ExchangeError
        throw new Error(`read ${tagName} from xml faild, err is ${err}`);
      }
    }
  }

  /**
   * 获取邮箱
   *
   * @param tagElements js元素对象
   * @returns 邮箱数组
   */
  public getMailBoxes(tagElements: JsObjectElement[]): Mailbox[] {
    const mailBoxesJsonArr: JsObjectElement[] = tagElements;
    let mailBoxesArr: Mailbox[] = new Array<Mailbox>();
    mailBoxesJsonArr.forEach((item: JsObjectElement) => {
      let mailBox: Mailbox = new Mailbox();
      let mailBoxAttrs: JsObjectElement[] = item._elements;
      mailBoxAttrs.forEach((mailBoxAttr: JsObjectElement) => {
        let mailBoxAttrTagName: string = mailBoxAttr._name;
        mailBox[lowercaseFirstLetter(mailBoxAttrTagName)] = mailBoxAttr._elements[0]?._text;
      });
      mailBoxesArr.push(mailBox);
    });
    return mailBoxesArr;
  }
}

/**
 * 正文类型
 */
export interface Body {
  bodyType: BodyType;
  isTruncated?: boolean;
  value: string;
}

/**
 * 正文文本类型
 */
export enum BodyType {
  HTML = 'HTML',
  Text = 'Text'
}

/**
 * 敏感度级别类型
 */
export enum Sensitivity {
  Normal = 'Normal',
  Personal = 'Personal',
  Private = 'Private',
  Confidential = 'Confidential'
}

/**
 * 重要性类型
 */
export enum Importance {
  Low = 'Low',
  Normal = 'Normal',
  High = 'High'
}

/**
 * 将xml标签的元素封装成需要xml写入接口格式
 *
 * @param nameSpace 命名空间
 * @param elementName 标签名
 * @param value 值
 * @param attrMap 属性和属性值得Map集合
 * @param childWritable 子标签的xml写入接口
 * @returns XmlWritable xml写入接口
 */
export function getWritableElement(nameSpace?: string, elementName?: string, value?: string, attrMap?: Map<string, string>,
                                   childWritable?: XmlWritable[]): XmlWritable {
  let writable: XmlWritable = {
    nameSpace,
    elementName,
    value,
    attrMap,
    childWritable,
  };
  return writable;
}

/**
 * 邮箱类，包含邮箱地址、姓名及相关属性
 *
 * @since 2024-04-07
 */
export class Mailbox implements XmlWritable {
  private _emailAddress: string;
  private _name?: string;
  private _routingType?: string;
  private _mailboxType?: MailboxType;
  private _itemId?: UniteId;

  /**
   * 邮箱构造器
   *
   * @param emailAddress 邮箱地址
   * @param routingType 路由类型
   */
  public constructor(emailAddress?: string, routingType?: string) {
    this._emailAddress = emailAddress;
    this._routingType = routingType;
  }

  /**
   * 获取邮箱地址
   *
   * @returns 邮箱地址
   */
  public get emailAddress(): string {
    return this._emailAddress;
  }

  /**
   * 设置邮箱地址
   *
   * @param emailAddress 邮箱地址
   */
  public set emailAddress(emailAddress: string) {
    this._emailAddress = emailAddress;
  }

  /**
   * 获取路由类型
   *
   * @returns 路由类型
   */
  public get routingType(): string {
    return this._routingType;
  }

  /**
   * 设置路由类型
   *
   * @param routingType 路由类型
   */
  public set routingType(routingType: string) {
    this._routingType = routingType;
  }

  /**
   * 获取用户名
   *
   * @returns 用户名
   */
  public get name(): string {
    return this._name;
  }

  /**
   * 设置用户名
   *
   * @param name 用户名
   */
  public set name(name: string) {
    this._name = name;
  }


  /**
   * 获取邮箱用户的邮箱类型
   *
   * @returns 邮箱用户的邮箱类型
   */
  public get mailboxType(): MailboxType {
    return this._mailboxType;
  }

  /**
   * 设置邮箱用户的邮箱类型
   *
   * @param 邮箱用户的邮箱类型
   */
  public set mailboxType(mailboxType: MailboxType) {
    this._mailboxType = mailboxType;
  }

  /**
   * 获取邮箱Id
   *
   * @returns 邮箱Id
   */
  public get itemId(): UniteId {
    return this._itemId;
  }

  /**
   * 设置邮箱Id
   *
   * @param 邮箱Id
   */
  public set itemId(itemId: UniteId) {
    this._itemId = itemId;
  }

  /**
   * 根据当前属性写出对应xml
   *
   * @param soapXmlWriter xml写入器
   */
  public writeToXml(soapXmlWriter: SoapXmlWriter): void {
    soapXmlWriter.writeStartElement(XmlNamespace.TYPE, XmlElement.NAME_MAILBOX);
    soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_EMAIL_ADDRESS, this.emailAddress);
    if (this.routingType) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_ROUTING_TYPE, this.routingType);
    }
    if (this.name) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_NAME, this.name);
    }
    if (this.routingType) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_ROUTING_TYPE, this.routingType);
    }
    if (this.mailboxType) {
      soapXmlWriter.writeElementWithValue(XmlNamespace.TYPE, XmlElement.NAME_MAILBOX_TYPE, MailboxType[this.mailboxType]);
    }
    if (this.itemId) {
      let idChangeKeyArr = new Map();
      idChangeKeyArr.set(XmlElement.NAME_ID, this.itemId.id ?? '');
      idChangeKeyArr.set(XmlElement.NAME_CHANGE_KEY, this.itemId.changeKey ?? '');
      soapXmlWriter.writeElementWithAttribute(XmlNamespace.TYPE, XmlElement.NAME_ITEM_ID, idChangeKeyArr);
    }
    soapXmlWriter.writeEndElement();
  }
}
