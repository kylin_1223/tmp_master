/*
 * Copyright © 2023 - 2024 Coremail论客
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { ModelBase } from './mode_base';
import { FolderName } from '../xml/xml_attribute';
import { JsObjectElement, XmlReadable } from '../xml';

/**
 * 订阅数据模型
 * 流通知数据模型主要用于存储 流通知-订阅 相关数据
 *
 * @since 2024-03-19
 */
export class StreamingSubscription extends ModelBase {
  private _folderIds: FolderName[];
  private _eventTypes: EventTypes[];
  private _subscriptionId: string;
  private _connectionTime: string;
  private _streamingEventsXml: string = '';

  /**
   * 设置文件夹标识符数组
   *
   * @param value 文件夹标识符数组
   */
  public set folderIds(value: FolderName[]) {
    this._folderIds = value;
  }

  /**
   * 获取文件夹标识符数组
   *
   * @returns 文件夹标识符数组
   */
  public get folderIds(): FolderName[] {
    return this._folderIds;
  }

  /**
   * 设置订阅的事件通知集合
   *
   * @param eventTypes 事件通知集合
   */
  public set eventTypes(eventTypes: EventTypes[]) {
    this._eventTypes = eventTypes;
  }

  /**
   * 获取订阅的事件通知集合
   *
   * @returns eventTypes 事件通知集合
   */
  public get eventTypes(): EventTypes[] {
    return this._eventTypes;
  }

  /**
   * 设置订阅id
   *
   * @param id 订阅id
   */
  public set subscriptionId(id: string) {
    this._subscriptionId = id;
  }

  /**
   * 获取订阅id
   *
   * @returns 订阅id
   */
  public get subscriptionId(): string {
    return this._subscriptionId;
  }

  /**
   * 设置打开状态的分钟数
   *
   * @param time 打开状态的分钟数
   */
  public set connectionTime(time: string) {
    this._connectionTime = time;
  }

  /**
   * 获取打开状态的分钟数
   *
   * @returns 打开状态的分钟数
   */
  public get connectionTime(): string {
    return this._connectionTime;
  }

  /**
   * 设置响应xml
   *
   * @param eventsXml 响应xml
   */
  public set streamingEventsXml(eventsXml: string) {
    this._streamingEventsXml = eventsXml;
  }

  /**
   * 获取响应xml
   *
   * @returns 响应xml
   */
  public get streamingEventsXml(): string {
    return this._streamingEventsXml;
  }
}

/**
 * 订阅响应数据模型
 * 流通知数据模型主要用于存储订阅响应相关数据
 *
 * @since 2024-03-19
 */
export class StreamingSubscriptionResp extends ModelBase {
  private _subscriptionId: string;

  /**
   * 设置订阅id
   *
   * @param id 订阅id
   */
  public set subscriptionId(value: string) {
    this._subscriptionId = value;
  }

  /**
   * 获取订阅id
   *
   * @returns 订阅id
   */
  public get subscriptionId(): string {
    return this._subscriptionId;
  }
}

/**
 * 取消订阅响应数据模型
 * 主要用于存储取消订阅响应的相关数据
 *
 * @since 2024-03-19
 */
export class UnSubscribeResp extends ModelBase {
}

/**
 * 请求流式处理通知响应数据模型
 * 主要用于存储请求流式处理通知响应的相关数据
 *
 * @since 2024-03-19
 */
export class GetStreamingEventsResp extends ModelBase implements XmlReadable {
  private _notifications: Notification[] = [];
  private _connectionStatus: string;

  // 响应类型 包括: connectionResponse(请求响应)以及eventResponse(事件响应)
  private _responseType: string;

  /**
   * 设置订阅信息列表
   *
   * @param value 订阅信息列表
   */
  public set notifications(value: Notification[]) {
    this._notifications = value;
  }

  /**
   * 获取订阅信息列表
   *
   * @returns 订阅信息列表
   */
  public get notifications(): Notification[] {
    return this._notifications;
  }

  /**
   * 设置流式处理订阅状态
   *
   * @param value 流式处理订阅状态
   */
  public set connectionStatus(value: string) {
    this._connectionStatus = value;
  }

  /**
   * 获取订阅信息列表
   *
   * @returns 流式处理订阅状态
   */
  public get connectionStatus(): string {
    return this._connectionStatus;
  }

  /**
   * 设置响应类型
   *
   * @param value 响应类型
   */
  public set responseType(value: string) {
    this._responseType = value;
  }

  /**
   * 获取响应类型
   *
   * @returns 响应类型
   */
  public get responseType(): string {
    return this._responseType;
  }

  /**
   * 解析订阅信息列表并赋给响应数据模型
   *
   * @param jsObject 订阅信息列表
   */
  public readFromXml(jsObject: JsObjectElement[]): void {
    let notificationList: Notification[] = [];
    for (const notificationItem of jsObject) {
      if (notificationItem._name === 'Notification') {
        let eventsList: JsObjectElement[] | undefined = notificationItem._elements;
        let notificationSimple: Notification = {
          subscriptionId: '',
          events: new Map(),
        };
        eventsList?.forEach(eventItem => {
          let name: string | undefined = eventItem._name;
          if (name === 'SubscriptionId') {
            notificationSimple.subscriptionId = eventItem._elements[0]._text;
          } else {
            let eventInfoList: JsObjectElement[] | undefined = eventItem._elements;
            let eventSimple: EventItem = this.setEventItemInfo(eventInfoList);
            if (notificationSimple.events.has(name)) {
              let eventList: EventItem[] | undefined = notificationSimple.events.get(name);
              eventList?.push.apply(eventSimple);
              notificationSimple.events.set(name, eventList);
            } else {
              notificationSimple.events.set(name, [eventSimple]);
            }
          }
        });
        notificationList.push(notificationSimple);
      }
    }
    this._notifications = notificationList;
  }

  /**
   * 将传入的事件列表解析为简单事件对象集合
   *
   * @param eventInfoList 事件列表
   * @returns eventSimpleObj 每个事件的简单对象
   */
  private setEventItemInfo(eventInfoList: JsObjectElement[]): EventItem {
    let eventSimpleObj: EventItem = {
      timeStamp: '',
      itemChangeKey: '',
      itemId: '',
      folderChangeKey: '',
      folderId: '',
    };
    eventSimpleObj.timeStamp = eventInfoList[0]._elements[0]._text;
    eventSimpleObj.itemChangeKey = eventInfoList[1]._attributes.ChangeKey;
    eventSimpleObj.itemId = eventInfoList[1]._attributes.Id;
    eventSimpleObj.folderChangeKey = eventInfoList[2]._attributes.ChangeKey;
    eventSimpleObj.folderId = eventInfoList[2]._attributes.Id;
    return eventSimpleObj;
  }
}

/**
 * 用于每个订阅信息项的校验
 */
export interface Notification {
  subscriptionId: string;
  events: Map<string, EventItem[]>;
}

/**
 * 用于每个事件项的校验
 */
export interface EventItem {
  timeStamp: string;
  itemChangeKey: string;
  itemId: string;
  folderChangeKey: string;
  folderId: string;
}

/**
 * 订阅事件类型
 */
export enum EventTypes {
  NewMailEvent = 0,
  CreatedEvent = 1,
  DeletedEvent = 2,
  ModifiedEvent = 3,
  MovedEvent = 4,
  CopiedEvent = 5,
  FreeBusyChangedEvent = 6,
}