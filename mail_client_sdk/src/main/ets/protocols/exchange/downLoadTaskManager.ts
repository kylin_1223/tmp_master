/* Copyright 2023 - 2024 Coremail
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Properties } from '../../api';
import { DownLoadAttachment, GetAttachment, ItemAttachment } from './model/mail/attachments';
import { DownLoadAttachmentRequest, GetAttachmentRequest } from './request/mail/attachment_request';
import { DownLoadAttachmentTask } from './task/mail/downLoad_attachment_task';
import { GetAttachmentTask } from './task/mail/get_attachment_task';

/**
 * 下载管理类
 */
export class DownLoadManager {
  private downloadMap: Map<string, DownLoadAttachmentTask> = new Map<string, DownLoadAttachmentTask>();

  /**
   * 下载项目附件
   *
   * @param attachmentId 附件ID
   * @returns 项目对象ItemAttachment
   */
  public downLoad(attachmentId: string): Promise<ItemAttachment>;

  /**
   * 下载文件附件
   *
   * @param attachmentId 附件ID
   * @param onDataReceive 进度callback
   * @param onComplete 文件下载路径加文件名称callback
   * @returns 空
   */
  public downLoad(attachmentId: string, path: string, onDataReceive: Function, onComplete: Function): Promise<string>;

  /**
   * 下载实现
   */
  public downLoad(attachmentId: string , path?: string,onDataReceive?: Function, onComplete?: Function){
    if (path) {
      return new Promise<void>((resolve,reject) => {
        let downLoadAttachment = new DownLoadAttachment();
        downLoadAttachment.attachmentIds = [attachmentId];
        downLoadAttachment.path = path;
        downLoadAttachment.onDataReceive = onDataReceive;
        downLoadAttachment.onComplete = onComplete;
        let properties: Properties = {
          exchange: {
            url: 'https://outlook.office365.com/EWS/Exchange.asmx',
            secure: false,
          },
          userInfo: {
            username: 'xlleng110@hotmail.com',
            password: 'xlleng@1234'
          }
        }
        const task = new DownLoadAttachmentTask(new DownLoadAttachmentRequest(downLoadAttachment), properties, true)
        task.execute().then();
        this.downloadMap.set(attachmentId, task);
        resolve();
      })
    } else {
      return new Promise((resolve,reject) => {
        let getAttachment = new GetAttachment();
        getAttachment.attachmentIds = [attachmentId];
        let properties: Properties = {
          exchange: {
            url: 'https://outlook.office365.com/EWS/Exchange.asmx',
            secure: false,
          },
          userInfo: {
            username: 'xlleng110@hotmail.com',
            password: 'xlleng@1234',
          }
        }
        let itemAttachment: ItemAttachment;
        let getAttachmentTask: GetAttachmentTask = new GetAttachmentTask(new GetAttachmentRequest(getAttachment), properties, false);
        getAttachmentTask.execute().then(data => {
          data.getData().responseMessages.forEach(responseMessages => {
            if(responseMessages.responseCode == 'NoError'){
              responseMessages.attachmentList.forEach(att => {
                if (att instanceof ItemAttachment) {
                  itemAttachment = (att as ItemAttachment);
                  resolve(itemAttachment)
                }
              })
            } else {
              reject(responseMessages)
            }
          })
        });
      })
    }
  }

  /**
   * 取消下载
   */
  cancelDownLoad(attachmentId: string): void {
    console.log('cancelDownLoad--------' , JSON.stringify(this.downloadMap.get(attachmentId)))
    this.downloadMap.get(attachmentId)?.cancelDownLoad();
    this.downloadMap.delete(attachmentId);
  }
}
